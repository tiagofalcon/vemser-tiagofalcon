SELECT BD.ID_BANCO_DADOS,
        CPF, 
        BD.NOME,
        ANIVERSARIO,
        ID_BANCO_DADOS_2,
        BD2.NOME AS NOMEBD2,
        ENDERECO
FROM BANCO_DADOS BD 
INNER JOIN BANCO_DADOS_DOIS BD2 
ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS
WHERE BD.NOME  = 'TESTE 1';
--ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS AND BD.ID_BANCO_DADOS > 8;
--ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS OR BD.ID_BANCO_DADOS > 8;

SELECT BD.ID_BANCO_DADOS,
        CPF, 
        BD.NOME,
        ANIVERSARIO,
        ID_BANCO_DADOS_2,
        BD2.NOME AS NOMEBD2,
        ENDERECO
FROM BANCO_DADOS BD LEFT JOIN BANCO_DADOS_DOIS BD2 
ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS;

SELECT BD.ID_BANCO_DADOS,
        CPF, 
        BD.NOME,
        ANIVERSARIO,
        ID_BANCO_DADOS_2,
        BD2.NOME AS NOMEBD2,
        ENDERECO
FROM BANCO_DADOS BD FULL JOIN BANCO_DADOS_DOIS BD2 
ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS;

SELECT BD.ID_BANCO_DADOS,
        CPF, 
        BD.NOME,
        ANIVERSARIO,
        ID_BANCO_DADOS_2,
        BD2.NOME AS NOMEBD2,
        ENDERECO
FROM BANCO_DADOS BD CROSS JOIN BANCO_DADOS_DOIS BD2;

SELECT DISTINCT BD.NOME FROM BANCO_DADOS BD CROSS JOIN BANCO_DADOS_DOIS BD2;

SELECT BD.ID_BANCO_DADOS,
        CPF,
        BD.NOME
FROM BANCO_DADOS BD
UNION
SELECT ID_BANCO_DADOS_2,
        BD2.NOME AS NOMEBD2,
        ENDERECO
FROM BANCO_DADOS_DOIS BD2;

SELECT *
FROM BANCO_DADOS
WHERE NOME LIKE '%STE';

SELECT *
FROM BANCO_DADOS
WHERE NOME IS NOT NULL;

-- = IGUAL
-- < MENOS QUE
-- > MAIOR QUE
-- <> DIFERENTE

/*ALTER TABLE BANCO_DADOS_DOIS
DROP CONSTRAINT SYS_C006993;*/

/*INSERT INTO BANCO_DADOS_DOIS(ID_BANCO_DADOS_2, ID_BANCO_DADOS, NOME, ENDERECO) 
VALUES (BANCO_DADOS_DOIS_SEQ.NEXTVAL, null, 'TESTE BANCO DOIS 2', 'QUALQUER 2');*/

