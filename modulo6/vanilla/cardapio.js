function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ];

    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' );
    }
  
    cardapio = [...cardapio,
      'pastel de carne',
      'empada de legumes marabijosa'
    ];
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      cardapio.splice( cardapio.indexOf("enroladinho de salsicha"), 1 );
      cardapio.splice( cardapio.indexOf("pastel de carne"), 1 );
    }

    const resultadoFinal = cardapio.map(name => name.toUpperCase());

    return resultadoFinal;
  }
  
  console.log(cardapioIFood()) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]  