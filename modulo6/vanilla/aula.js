//console.log("Para mostrar que o professor não está mentindo!!!");
//console.log(nome);
//console.log(nome1);


/** --------------------VAR E LET---------------------------- */
/*var nome = "tal coisa";
var nome = "atel coisa";
console.log(nome);*/

let nome1 = "tal coisa 1";
nome1 = "tal coisa 1222";

{
    let nome1 = "Outra coisa";
    console.log(nome1);
}

console.log(nome1);

/** --------------------CONSTANTE--------------------------- */

const cpf = "00000000000";
const doubleV = 1.3;

//console.log(cpf);
//console.log(doubleV);

const pessoa = {
    nome: "Marcos"
};

Object.freeze(pessoa);

const pessoaModificada = pessoa;
pessoaModificada.nome = "Marcos1";
pessoaModificada.idade = 30;
pessoaModificada.status = "Um guri!";

console.log(pessoaModificada);

/** --------------------FUNCTION---------------------------- */

let nome = "Tiago";
let idade = 35;
let semestre = 5;
let notas = [10, 9, 9.5, 9.7];

let nome2 = "Tiago F";
let idade2 = 35;
let semestre2 = 7;
let notas2 = [8, 7, 7.1, 7.6];


//Factory
function funcaoCriacaoObjetoAluno(nomeExt, idadeExt, semestreExt, notas = [], teste = []) {
    const aluno = {
        nome: nomeExt,
        idade: idadeExt,
        semestre: semestreExt,
        avaliacoes: notas
    }

    function verificaAprovacao(notas) {
        if(notas.length == 0) {
            return;
        }

        let somatoria = 0;
        for (let i = 0; i < notas.length; i++) {
            somatoria += notas[i];
        }
        return somatoria / notas.length > 7 ? "Aprovado" : "Reprovado";
    }

    aluno.status = verificaAprovacao(notas);

    return aluno;
}

const aluno = funcaoCriacaoObjetoAluno(nome, idade, semestre, notas);
//const aluno1 = funcaoCriacaoObjetoAluno(nome2, idade2, semestre2, notas2);
//const aluno2 = funcaoCriacaoObjetoAluno(nome2, idade2, semestre2);

/** --------------------Template String----------------------- */

function funcaoTesteConcatETemplate(teste = [], testeComObjeto = "") {
    //[1, "2", 3]
    /*function testandoTipagens(teste) {
        let valor1 = teste[0];
        let valor2 = teste[1];
        let valor3 = teste[2];
        console.log(valor1 + valor2 + valor3);
        console.log(valor2 + valor1 + valor3);
        console.log(valor1 + valor3 + valor2);
    }*/

    //[30,5]
    function testandoTemplate(teste) {
        let valor1 = teste[0];
        let valor2 = teste[1];
        console.log(`A quantidade de dia é ${valor1 + valor2}`);

        let texto = "Varias frutas:" +
                    "\n" +
                    "- Banana" +
                    "\n" +
                    "- Ameixa" +
                    "\n" +
                    "- Uva" +
                    "\n" +
                    "- Caqui";

        let texto1 = `Varias frutas:
            - Banana
            - Ameixa
            - Uva
            - Caqui`;

        console.log(texto1);
    }

    function testandoTemplateComObjeto(teste) {
        if(teste === "") {
            return;
        }

        console.log(`Meu nome é ${teste.nome} tenho ${teste.idade} e sou ${teste.nacionalidade}`);
    }

    testandoTemplate(teste);
    testandoTemplateComObjeto(testeComObjeto);
}




//const teste = [1, "2", 3];
const teste = [30, 5];
const testeObj = {
    nome: "Tiago",
    idade: 35,
    nacionalidade: "brasileiro"
}

// funcaoTesteConcatETemplate(teste, testeObj);


/**----------------Desconstruction--------------------- */

function funcaoTesteDestruction(testeComObjetos) {
    function testando({nome, idade, nacionalidade}) {
        console.log(`Meu nome é ${teste.nome} tenho ${teste.idade} e sou ${teste.nacionalidade}`);
    }

    testando(testeComObjetos);

    /*let array = [0, 77, 42, 25];
    const [, n2,, n4] = array;
    console.log(n4);*/
}

const testeObj1 = {
    nome: "Tiago F",
    idade: 35,
    nacionalidade: "brasileiro"
}

//funcaoTesteDestruction(testeObj1);


/**----------------Spread Operator--------------------- */

function funcaoTesteSpread(...obj) {
    /*
        let array = [0, 77, 42, 25];
        console.log({...array});
        console.log([...array]);
        console.log(..."MEUNOME");
    */

    for (let i = 0; i < obj.length; i++) {
        console.log(obj[i]);
    }
}

//funcaoTesteSpread("Tiago", "R$50,00", "Qualquer1");

//inverte valores com deconstruction
/* let a = 15;
let b = 20;
[a , b] = [b, a];

console.log(a, b) */

let timesCampeaoDoMundo = ["Internacional", "SP", "Corinthians"];
let timesCampeaoIntercontinental = ["Gremio", "Flamengo"];
let timeSemNada = "Palmeiras";

const [Internacional, SP] = timesCampeaoDoMundo;
const [Gremio] = timesCampeaoIntercontinental;
const todosTimes = [ Internacional, timeSemNada, SP, Gremio]
//const todosTimes = [..timesCampeaoDoMundo, timeSemNada, ...timesCampeaoIntercontinental];

//console.log(todosTimes);

/* function times([...mundial], [...intercontinental], semNada) {
    console.log(`Esses times ganharam o mundial de verdade ${mundial.map(e => e)}, esses jogaram 1 jogo só ${intercontinental.map(e => e)} e o ${semNada} não tem mundial`);
} */

//times(timesCampeaoDoMundo, timesCampeaoIntercontinental, timeSemNada);