import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { PrivateRoute } from '../components/rotaPrivada';

import Home from './home';
import ListaAvaliacoes from './avaliacoes';
import DetalhesEpisodios from './detalhesEpisodios';
import TodosEpisodios from './episodios';
import Formulario from '../components/formularioUi';

export default class App extends Component {
  render(){
    return (
      <div className="App">
        <Router>
          <Route path="/" exact component={ TodosEpisodios } />
          <Route path="/formulario" exact component={ Formulario } />
          <PrivateRoute path="/aleatorio" exact component={ Home } />
          <PrivateRoute path="/avaliacoes" exact component={ ListaAvaliacoes } />
          <PrivateRoute path="/episodio/:id" exact component={ DetalhesEpisodios } />
        </Router>
      </div>
    )
  };
}