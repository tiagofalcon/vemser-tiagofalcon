import React from 'react';
import { Link } from 'react-router-dom';


const ListaEpisodiosUi = ( { listaEpisodios } ) => 

  <React.Fragment>
    {
      listaEpisodios && listaEpisodios.map( e => 
        <li key={ e.id }>
          <Link to={ { pathname: `/episodio/${ e.id }`, state: { episodio: e } } }>
            { `${ e.nome } - ${ e.nota || 'Sem nota' } - ${ e.dataEstreia.toLocaleDateString( 'pt-br' ) || 'N/D' }` }
          </Link>
        </li>
      )
    }
  </React.Fragment>

export default ListaEpisodiosUi;