import React from 'react';

const CampoBusca = ( { placeholder, atualizarValor } ) =>
  <React.Fragment>
    <input type='text' placeholder={ placeholder } onBlur={ atualizarValor } />
  </React.Fragment>

export default CampoBusca;