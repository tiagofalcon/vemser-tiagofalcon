import React from 'react';

const Titulo = () => <h1>React Mirror</h1>;

const Familia = props => {
  return (
          <React.Fragment>
            { props.nome }
            { props.sobrenome }
          </React.Fragment>
        )
}

export default Titulo;
export { Familia };