import React, { Component } from 'react';

import { BotaoUi, ListaEpisodiosUi } from '../../components';
import { EpisodiosApi, ListaEpisodios } from '../../models';

export default class ListaAvaliacoes extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      avaliados: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
    .then( respostas => {
      let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
      console.log(listaEpisodios)
      this.setState( state => {
        return {
          ...state,
          avaliados: listaEpisodios.avaliados
        }
      } )
    })
  }

  render() {
    const { avaliados } = this.state;
    return (
      <React.Fragment>
        <header className='App-header'>
          <BotaoUi link='/' nome='Página Inicial' />
          <ListaEpisodiosUi listaEpisodios={ avaliados } />
        </header>
      </React.Fragment>
    )
  }
} 
