//exercicio 01

const temCampoNullOuUndefined = serie => {
  const valores = Object.values( serie );
  for ( let index = 0; index < valores.length; index++ ) {
    if( !valores[index] ){
      return serie;
    }
  }
}

const anoInvalido = serie => {
  const data = new Date();
  const ano = data.getFullYear();
  if( serie.anoEstreia > ano ) {
    return serie;
  }
}


Array.prototype.invalidas = function() {
  let seriesInvalidas = [];
  let stringRetorno ='Séries Inválidas:';

  for ( let index = 0; index < this.length; index++ ) {
    if( temCampoNullOuUndefined( this[index] ) || anoInvalido( this[index] ) ) {
      seriesInvalidas.push( this[index] )
    }
  }

  if( seriesInvalidas.length > 0) {
    for ( let index = 0; index < seriesInvalidas.length; index++ ) {
      if(seriesInvalidas.length - 1 > index) {
        stringRetorno = `${ stringRetorno } ${ seriesInvalidas[index].titulo } -`; 
      } else {
        stringRetorno = `${ stringRetorno } ${ seriesInvalidas[index].titulo }`
      }
    }
  } else {
    stringRetorno = '';
  }

  return stringRetorno;
}

console.log(`Exercicio 1 - ${ series.invalidas() }`);

//exercicio 02

Array.prototype.filtrarPorAno = function( ano ) {

  let arraysFiltrados = [];

  for (let index = 0; index < this.length; index++) {
    if( this[index].anoEstreia >= ano ) {
      arraysFiltrados.push( this[index] );
    }
  }

  return arraysFiltrados;
}

console.log(`Exercicio 2 - ${ series.filtrarPorAno( 2017 ).map( element => element.titulo ) }` );

//exercicio 03

Array.prototype.procurarPorNome = function( nome ) {

  const novoArray = this.filter( serie => serie.elenco.includes(nome));

  return novoArray.length > 0 ? true : false;
}

console.log(`Exercicio 3 - ${ series.procurarPorNome( 'Tiago Falcon Lopes' ) }` );

//exercicio 04

Array.prototype.mediaDeEpisodios = function() {

  let totalEpisodios = 0;

  for (let index = 0; index < this.length; index++) {
    totalEpisodios += this[index].numeroEpisodios;
  }

  return totalEpisodios / this.length;
}

console.log(`Exercicio 4 - ${ series.mediaDeEpisodios() }` );

//exercicio 05

Array.prototype.totalSalarios = function( indice ) {

  let totalDosSalarios = this[indice].diretor.length * 100000 + this[indice].elenco.length * 40000;

  return totalDosSalarios;
}

console.log(`Exercicio 5 - ${ series.totalSalarios(0) }` );

//exercicio 06-A

Array.prototype.queroGenero = function(genero) {

  const generoArray = this.filter( serie => serie.genero.includes( genero ));
  const titulosDasSeries = generoArray.map( element => element.titulo )

  return titulosDasSeries;
}

console.log(`Exercicio 6 A - ${ series.queroGenero( 'Caos' ) }` );

//exercicio 06-B

Array.prototype.queroTitulo = function( titulo ) {

  const tituloArray = this.filter( serie => serie.titulo.includes( titulo ));
  const titulosDasSeries = tituloArray.map( element => element.titulo )

  return titulosDasSeries;
}

console.log(`Exercicio 6 B - ${ series.queroTitulo( 'The' ) }` );

//exercicio 07

const ordenarPeloUltimo = arrayNomes => {
  let ultimoNome = [];
  let primeiroNome = [];
  let nomesCompletos = [];
  
  for (let index = 0; index < arrayNomes.length; index++) {
    let element = arrayNomes[index].split( ' ' );
    ultimoNome.push( element[ element.length - 1 ] );
    primeiroNome.push( element[0]);
  }

  ultimoNome.sort( ( a, b ) => a > b ? 1 : -1 ) ;
  primeiroNome.sort( ( a, b ) => a > b ? 1 : -1 ) ;

  for (let i = 0; i < ultimoNome.length; i++) {
    for (let j = 0; j < arrayNomes.length; j++) {
      if( arrayNomes[j].includes( ultimoNome[i] ) && arrayNomes[j].includes( primeiroNome[i] ) ) {
        nomesCompletos.push( arrayNomes[j] );
      }
      
    }
  }

  return nomesCompletos;
}

Array.prototype.creditos = function(serie) {
  let titulo = `${ serie.titulo }\n`;
  let diretores = `Diretores:`;
  let atores = `\nElenco:`;

  let direcao = ordenarPeloUltimo(serie.diretor);
  let artistas = ordenarPeloUltimo(serie.elenco);

  for (let index = 0; index < direcao.length; index++) {
    diretores = `${ diretores }\n ${ direcao[index] }`
  }

  for (let index = 0; index < artistas.length; index++) {
    atores = `${ atores } \n ${ artistas[index] }`
  }

  const creditoSeries = titulo + diretores + atores

  return creditoSeries;
}

console.log(`Exercicio 7 - ${ series.creditos(series[0]) }` );

//exercicio 09

const nomesAbreviados = elenco => {
  let contador = 0;
  for (let index = 0; index < elenco.length; index++) {
    if( elenco[index].match(/[A-Z]{1}\./g) ) {
      contador++;
    }
  }
  return contador > 0 && contador === elenco.length;
}

const criaHashTag = elenco => {
  let stringHash = '#';
  for (let index = 0; index < elenco.length; index++) {
    stringHash = stringHash + elenco[index].match( /[A-Z]{1}\./g )[0];
  }
  const palavra = stringHash.replace(/\./g, '');
  return palavra;
}

Array.prototype.identificaElencoNomesAbreviados = function() {
  const serie = this.filter( serie => nomesAbreviados ( serie.elenco ) );
  const palavraHash = criaHashTag(serie[0].elenco);
  return palavraHash;
}

console.log(`Exercicio 8 - ${ series.identificaElencoNomesAbreviados() }` );