//Exercicio 01

function calcularCirculo({raio, tipoCalculo:tipo}) {
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
}

const objetoCirculo1 = {
    raio: 4,
    tipoCalculo: "A"
}

const objetoCirculo2 = {
    raio: 4,
    tipoCalculo: "C"
}

const circ1 = calcularCirculo(objetoCirculo1);
const circ2 = calcularCirculo(objetoCirculo2);

console.log("Exercicio 01");
console.log(circ1);
console.log(circ2);
console.log("\n\n");

//Exercicio 02

function naoBissexto(ano) {
    return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;
}

const naoBissextoTrue = naoBissexto(2017);
const bissextoFalse = naoBissexto(2016);

console.log("Exercicio 02");
console.log(`Nao bissexto 2017: ${naoBissextoTrue}`);
console.log(`Nao bissexto 2016: ${bissextoFalse}`);
console.log("\n\n");

//Exercicio 03

function somarPares(array) {
    let somatoria = 0;
    for (let i = 0; i < array.length; i += 2) {
        somatoria += array[i];
    }
    return somatoria;
}

const somados1 = somarPares([0,1,2,3,4,5]);
const somados2 = somarPares([ 1, 56, 4.34, 6, -2 ]);

console.log("Exercicio 03");
console.log(somados1);
console.log(somados2);
console.log("\n\n");

//Exercicio 04
console.log("Exemplo de encapsular em constante")
const aula = {
    turma: 2020,
    qtdAlunos: 12,
    qtdAulasNoModulo(modulo) {
        switch(modulo) {
            case 1:
                console.log(10);
                break;
            case 2:
                console.log(5);
                break;
            default:
                break;
        }
    }
}

aula.qtdAulasNoModulo(1);
console.log("\n\n");

console.log("Exercicio 04");
console.log("Exercicio 04 forma dificil")
function adicionarComMaisCodigo(op1) {
    return function (op2) {
        return op1 + op2;
    }
}
console.log(adicionarComMaisCodigo(3)(4));
console.log("\n\n");

console.log("Exercicio 04 forma simplificada")
const adicionar = a => b => a + b;

console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));
console.log("\n\n");

//Exemplo arrow function
console.log("Exemplo arrow function com base no exercicio 04")
/* let is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 5));
console.log(is_divisivel(divisor, 8));
console.log(is_divisivel(divisor, 16));
console.log(is_divisivel(divisor, 9)); */

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(5));
console.log(is_divisivel(8));
console.log(is_divisivel(16));
console.log(is_divisivel(9));
console.log("\n\n");

//Exercicio 05

function arredondar(value) {
    const duasCasas = Math.ceil(value * 100) / 100;
    return duasCasas.toFixed(2);
}

function imprimirBRL(quantia) {
    let retorno = "";
    const numeroString = arredondar(quantia);
    let parte1 = numeroString.split(".")[0];
    const parte2 = numeroString.split(".")[1];

    if(quantia < 0) {
        retorno = "-";
        parte1 = parte1.substring(1);
    }

    if(parte1.length > 3) {
        parte1 = parte1.split(/(?=(?:...)*$)/).join('.');
    }
    
    retorno = `${retorno}R$ ${parte1},${parte2}`;

    return retorno;
}

console.log("Exercicio 05");
console.log(imprimirBRL(0));
console.log(imprimirBRL(349));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));
