class Partida {
    constructor (time1, time2, resultado) {
        this._time1 = time1;
        this._time2 = time2;
        this._resultado = resultado;
    }

    get time1() {
        return this._time1;
    }

    set time1(time) {
        this._time1 = time;
    }

    get time2() {
        return this._time2;
    }

    set time2(time) {
        this._time2 = time;
    }

    get resultado() {
        return this._resultado;
    }

    set setResultado(resultado) {
        this._resultado = resultado;
    }

}