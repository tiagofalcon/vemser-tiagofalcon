class Jogador {

    constructor(nome, numero) {
        this._nome = nome;
        this._numero = numero;
    }

    get nome() {
        return this._nome;
    }

    set nome(nome) {
        this._nome = nome; 
    }

    get numero() {
        return this._numero;
    }

    set numero(numero) {
        this._numero = numero;
    }
}