class Historico {
    constructor() {
        this._historico = [];
    }

    get historico() {
        return this._historico;
    }

    set historico(historico) {
        this._historico = historico;
    }

    adicionarPartida(partida) {
        this._historico.push(partida);
    }

    retornarPartidasDeTime(time) {
        return this._historico.filter( partida => partida.time1.nome === time.nome || partida.time2.nome === time.nome);
    }
}