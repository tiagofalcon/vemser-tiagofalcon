class Time {

    constructor(nome, tipoEsporte, status, liga) {
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = status;
        this._liga = liga;
        this._jogadores = [];
    }

    get nome() {
        return this._nome;
    }

    set nome(nome) {
        this._nome = nome; 
    }

    get tipoEsporte() {
        return this._tipoEsporte;
    }

    set tipoEsporte(tipoEsporte) {
        this._tipoEsporte = tipoEsporte;
    }

    get status() {
        return this._status;
    }

    set status(status) {
        this._status = status;
    }

    get liga() {
        return this._liga;
    }

    set liga(liga) {
        this._liga = liga;
    }

    get jogadores() {
        return this._jogadores;
    }

    set jogadores(jogadores) {
        this._jogadores;
    }

    adicionarJogador(jogador) {
        this._jogadores.push(jogador);
    }

    buscarJogadorPorNome(nomeJogador) {
        return this._jogadores.filter( jogador => jogador.nome === nomeJogador);
    }

    buscarJogadorPorNumero(numeroJogador) {
        return this._jogadores.filter( jogador => jogador.numero === numeroJogador);
    }

}