let time1 = new Time("T1", "Fut", "Classificado", "RS");

let jogador1 = new Jogador("J1", 7);
let jogador2 = new Jogador("J2", 5);

time1.adicionarJogador(jogador1);
time1.adicionarJogador(jogador2);
console.log(time1.jogadores);
console.log(time1.buscarJogadorPorNumero(7));
console.log(time1.buscarJogadorPorNome(jogador2.nome));


let time2 = new Time("T2", "Fut", "Campeao", "RS");
let jogador3 = new Jogador("J3", 6);
time2.adicionarJogador(jogador3);

console.log(time2.jogadores);

let time3 = new Time("T3", "Fut", "Vice-Campeao", "RS");
let jogador4 = new Jogador("J4", 6);
time3.adicionarJogador(jogador4);

let historico = new Historico();

let partida1 = new Partida(time1, time2, "time1 0x0 time2");
partida1.setResultado = "time1 2x0 time2";
historico.adicionarPartida(partida1);

let partida2 = new Partida(time1, time3, "time1 0x0 time3");
partida2.setResultado = "time1 0x3 time3";
historico.adicionarPartida(partida2);

console.log(historico.retornarPartidasDeTime(time1));
console.log("\n\n")
console.log(historico.retornarPartidasDeTime(time2));