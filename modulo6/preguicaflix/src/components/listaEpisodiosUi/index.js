import React from 'react';
import { Link } from 'react-router-dom';
import './listaEpisodiosUi.css';

import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const ListaEpisodiosUi = ( { listaEpisodios } ) => 

  <div className='listagem'>
    {
      listaEpisodios && listaEpisodios.map( e => 
        <React.Fragment key={ e.id }>
          <div className='card'>
            <Link to={ `episodio/${ e.id }` } className='link'>
              <li className='lista-episodios'>
                <img className='imagem-episodio' src={ e.url } alt={ `Imagem do episódio ${ e.nome }` }/>
              </li>
              <li className='lista-episodios lista-episodios-nome'>{ e.nome }</li>
              <li className='lista-episodios'>Duração: { e.duracao }</li>
              <li className='lista-episodios'>Temporada/Episódio: { e.temporada }/{ e.ordem }</li>
              <li className='lista-episodios'>Data de estreia: { e.dataEstreia.toLocaleDateString('pt-br') }</li>
              <li className='lista-episodios lista-episodios-sinopse'>Sinopse: { e.sinopse }</li>
              <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Nota:</Typography>
                <Rating name="read-only" value={ e.nota } readOnly precision={0.5}/>
              </Box>
            </Link>
          </div>
        </React.Fragment>
      )
    }
  </div>

export default ListaEpisodiosUi;