import './App.css';
import { BrowserRouter as Router, Route } from  'react-router-dom';

import Home from './containers/home';
import DetalhesEpisodios from './containers/detalhesEpisodios';
import Ranking from './containers/ranking';
import { PrivateRoute } from './components/rotaPrivada';
import Formulario from './containers/formularioUi';
import Login from './containers/loginUi';

function App() {
  return (
    <div className="App">
      <Router>
        <Route path='/' exact component={ Home } />
        <Route path='/login' exact component={ Login } />
        <Route path='/formulario' exact component={ Formulario } />
        <PrivateRoute path='/episodio/:id' exact component={ DetalhesEpisodios } />
        <PrivateRoute path='/ranking' exact component={ Ranking } />
      </Router>
    </div>
  );
}

export default App;
