import React, { Component } from "react";
import ListaEpisodiosUi from "../../components/listaEpisodiosUi";
import NavigationUi from "../../components/navigationUi";

import { EpisodiosApi, ListaEpisodios } from "../../models";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: [],
    };
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes(),
    ];

    Promise.all(requisicoes).then((respostas) => {
      const episodiosDoServidor = respostas[0];
      const notasServidor = respostas[1];
      const detalhesDoSeridor = respostas[2];
      const listaEpisodiosNova = new ListaEpisodios(
        episodiosDoServidor,
        notasServidor,
        detalhesDoSeridor
      );

      listaEpisodiosNova._todos.forEach((element) => {
        let tot = 0;
        let tam = 0;
        notasServidor.forEach((nota) => {
          if (nota.episodioId === element.id) {
            tot += nota.nota;
            tam++;
          }
        });
        element.nota = tot / tam;
      });

      this.setState((state) => {
        return {
          ...state,
          listaEpisodios: listaEpisodiosNova._todos,
          notasServidor,
        };
      });
    });
  }

  myKeyPress = (e) => {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.buscarTodosDetalhes(),
    ];

    Promise.all(requisicoes).then((respostas) => {
      const episodiosDoServidor = respostas[0];
      const notasServidor = respostas[1];
      const detalhesDoSeridor = respostas[2];
      const listaEpisodiosNova = new ListaEpisodios(
        episodiosDoServidor,
        notasServidor,
        detalhesDoSeridor
      );
      const episodio = listaEpisodiosNova.episodiosAleatorios;
      const id = episodio.id;
      this.setState((state) => {
        return { ...state, id, listaEpisodios: listaEpisodiosNova._todos };
      });
    });

    if (e.key === "Enter") {
      const termo = e.target.value;
      this.episodiosApi.filtrarPorTermo(termo).then((resultados) => {
        let lista = [];
        resultados.forEach((element) => {
          this.state.listaEpisodios.forEach((ep) => {
            if (element.id === ep.id) {
              lista.push(ep);
            }
          });
        });
        this.setState((state) => {
          return { ...state, listaEpisodios: lista };
        });
      });
    }
  };

  render() {
    const { listaEpisodios } = this.state;
    return (
      <div>
        <NavigationUi metodo={this.myKeyPress.bind(this)} />
        <ListaEpisodiosUi listaEpisodios={listaEpisodios} />
      </div>
    );
  }
}
