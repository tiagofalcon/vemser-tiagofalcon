import React, { useState } from 'react';
import NavigationUi from '../../components/navigationUi';

import './formulario.css';

function Formulario(props) {
  const [ user, setUser ] = useState('');
  const [ login, setLogin ] = useState('');
  const [ senha, setSenha ] = useState('');

  const handler = (e) => {
    e.preventDefault();
    localStorage.setItem( user, JSON.stringify( { login: login, senha: senha } ) );
  }

  return (
    <React.Fragment>
      <NavigationUi />
      <form className='formulario' onSubmit={ handler }>
        <div className='formulario-container'>
          <div className='itens'>
            <label>Nome</label>
            <input type='text' name='nome' onChange={ ev => setUser(ev.target.value) } />
          </div>
          <div className='itens'>
            <label>Login</label>
            <input type='text' name='login' onChange={ ev => setLogin(ev.target.value) } />
          </div>
          <div className='itens'>
            <label>Senha</label>
            <input type='password' name='senha' onChange={ ev => setSenha(ev.target.value) } />
          </div>
          <button type='submit'  >Salvar</button>
        </div>
      </form>
    </React.Fragment>
  )
}

export default Formulario;