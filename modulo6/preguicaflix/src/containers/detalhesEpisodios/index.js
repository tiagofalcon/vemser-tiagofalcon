import React, { Component } from 'react';

import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import { EpisodiosApi, Episodio } from '../../models';
import EpisodioUi from '../../components/episodioUi';
import NavigationUi from '../../components/navigationUi';

import './detalhesEpisodios.css';

export default class DetalheEpisodios extends Component {

  constructor( props ) {
    super( props );
    this.episodioApi = new EpisodiosApi();
    this.state = {
      detalhes: null,
    }
  }

 componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarEpisodio( episodioId ),
      this.episodioApi.buscarDetalhes( episodioId ),
      this.episodioApi.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        let qtdVezesAssistido = 0;
        let assistido = 'Não';
        if(respostas[2].length > 0){
          qtdVezesAssistido = respostas[2].length;
          assistido = 'Sim';
        }
        const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota } = respostas[0];
        let episodio = new Episodio( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota )
        episodio.qtdVezesAssistido = qtdVezesAssistido;
        episodio.assistido = assistido;
        this.setState( {
          episodio,
          detalhes: respostas[1],
          objNota: respostas[2],
        } )
      })
  } 

  registrarNota = ( nota ) => {
    if(nota === null) {
      nota = 1
    }
    const { episodio } = this.state;
    episodio.marcarComoAssistido();
    if ( episodio.validarNota( nota ) ) {
      console.log(episodio)
      episodio.avaliar( nota ).then( () => {
        console.log(nota)
      })
    }
    this.setState((state) => {
      return { ...state, episodio };
    });
    this.episodioApi.buscarNota( episodio.id )
      .then( resposta => {
        this.setState( {
          objNota: resposta
        } )
      } )
  }

  mediaNotas = ( objNotas ) => {
    const tamanho = objNotas.length;
    let total = 0;
    if( tamanho === 0 ) {
      return total;
    }
    objNotas.forEach(element => {
      total += element.nota;
    });
    return total / tamanho;
  }

  render() {
    const { episodio, detalhes, objNota } = this.state;
    return (
      <React.Fragment>
        <div className='detalhes'>
          <NavigationUi />
          { episodio && ( <EpisodioUi episodio={ episodio } /> ) }
          {
            detalhes ? 
            <React.Fragment>
              <p>{ detalhes.sinopse }</p>
              <p>{ new Date( detalhes.dataEstreia ).toLocaleDateString('pt-br') }</p>
              <p>IMDb: { detalhes.notaImdb * 0.5 }</p>
              <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Sua nota:</Typography>
                <Rating name="read-only" 
                        value={ objNota ? this.mediaNotas( objNota ) : 'N/D' } 
                        readOnly 
                        precision={0.5} 
                />
              </Box>
              <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Avaliar:</Typography>
                <Rating name="pristine" 
                        value={1} 
                        max={5}  
                        onChange={ (event, newValue) => this.registrarNota( newValue ) } 
                        precision={0.5} 
                />
              </Box>
            </React.Fragment> : null
          }
        </div>
      </React.Fragment>
    )
  }
}
