import React, { useState } from 'react';
import { Link } from  'react-router-dom';

import NavigationUi from '../../components/navigationUi';

import './login.css';

function Login(props) {
  const [ login, setLogin ] = useState('');
  const [ senha, setSenha ] = useState('');

  const handler = ( e ) => {
    e.preventDefault();
    let keys = Object.keys(localStorage);
    keys.forEach( element => {
      if(JSON.stringify( { login: login, senha: senha } ) === localStorage.getItem(element)) {
        localStorage.setItem('user', 'logado')
        props.history.push('/');
      }
    })
  }

  return (
    <React.Fragment>
      <NavigationUi />
      <form className='formulario' onSubmit={ handler }>
        <div className='formulario-container'>
          <div className='itens'>
            <label>Login</label>
            <input type='text' name='login' onChange={ ev => setLogin(ev.target.value) } />
          </div>
          <div className='itens'>
            <label>Senha</label>
            <input type='password' name='senha' onBlur={ ev => setSenha(ev.target.value) } />
          </div>
          <button type='submit'  >Entrar</button>
          <div>
            <Link className='link-login' to='/formulario'>Ainda não sou cadastrado</Link>
          </div>
        </div>

        {/* console.log(JSON.parse(localStorage.getItem(user))) */}
      </form>
    </React.Fragment>
  )
}

export default Login;