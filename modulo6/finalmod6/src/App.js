import { BrowserRouter as Router, Route } from  'react-router-dom';

import { Home, Interna, Login, Formulario} from './containers';

import { PrivateRoute } from './components/rotaPrivada'

function App() {
  return (
    <div className="App">
      <Router>
        <Route path='/' exact component={ Home } />
        <Route path='/login' exact component={ Login } />
        <Route path='/formulario' exact component={ Formulario } />
        <PrivateRoute path='/produto/:id' exact component={ Interna } />
      </Router>
    </div>
  );
}

export default App;
