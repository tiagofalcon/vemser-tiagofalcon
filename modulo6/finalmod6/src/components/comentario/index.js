import React, { useState } from 'react';
import LojaApi from '../../models/lojaApi';


function Comentario(props) {
  const [ idProduto, setIdProduto ] = useState();
  const [ nomeProduto, setNomeProduto ] = useState();
  const [ codigo, setCodigo] = useState();
  const [ usuario, setUsuario ] = useState('');
  const [ vendedor, setVendedor ] = useState('');
  const [ comentario, setComentario ] = useState('');

  const lojaApi = new LojaApi();

  const handler = (e) => {
    e.preventDefault();
    lojaApi.registrarComentario( { idProduto, nomeProduto, codigo, usuario, vendedor, comentario } );
    window.location.reload(false);
  }

  const buscarUsuarioLogado = () => {
    for( let i=0; i < localStorage.length; i++) {
      let key = localStorage.key(i);
      let value = localStorage[key];
      if( value === 'logado' ){
        return key;
      }
    }
  }

  const perguntar = ( idProduto, nomeProduto, codigo, usuario, vendedor, comentario ) => {
    setIdProduto( idProduto );
    setNomeProduto( nomeProduto );
    setCodigo( codigo );
    setUsuario( usuario );
    setVendedor( vendedor );
    setComentario( comentario );
  }

  const userLog = buscarUsuarioLogado();

  const { produto, detalhes } = props;

  return (
    <React.Fragment>
      <div className='input-button' >
        <form onSubmit={ handler }>
          <div className='left'>
            <input 
              className='input-gray' 
              type='text' 
              placeholder='pergunte ao vendedor' 
              onChange={ ev => perguntar( produto.id, produto.nome, detalhes.codigo, userLog, produto.idVendedor, ev.target.value ) }/>
          </div>
          <div className='right'>
            <button type='submit' className='button-sm button-white'>perguntar</button>
          </div>
        </form>       
      </div>
    </React.Fragment>
  )
}


export default Comentario;