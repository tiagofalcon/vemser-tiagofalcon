import React, { Component } from 'react';

import LojaApi from '../../models/lojaApi';


export default class CardRound1111 extends Component {
  constructor(props) {
    super(props);
    this.lojaApi = new LojaApi();
    this.state = {
      vendedores: []
    } 
  }

  componentDidMount() {

    this.lojaApi.buscarTodosVendedores()
      .then( resposta => {
      this.setState( {
        vendedores: resposta
      } )
    })
  }

  render() {
    
    const { vendedores } = this.state;

    return(
      vendedores.length > 0 && (
        <React.Fragment>
          <div className='card'>
            <p className='escuro'>muita maravilhosidade</p>
            <p className='claro'>nessa famosidade</p>
            <div className='card-4-col'>
              <div className='card-round'>
                <img className='img-card-r' src='https://media.istockphoto.com/photos/nice-beautiful-lady-with-blonde-curly-hair-work-at-the-notebook-sit-picture-id1087959426?b=1&k=6&m=1087959426&s=170x170&h=2W3PfqTV5w53WMi6REF9sDm6Q_1JwH9Sk-UjTOvvOkg=' alt='foto estatica' />
                <p className='nome-vendedores-home'>{vendedores[0].nome}</p>
              </div>
              <div className='card-round'>
                <img className='img-card-r' src='https://media.istockphoto.com/photos/woman-using-smartphone-picture-id1134002289?b=1&k=6&m=1134002289&s=170x170&h=d6dBRl5lsrgCc5X50sPfEBFV9mgrgNmGK5E0-ndd5nI=' alt='foto estatica' />
                <p className='nome-vendedores-home'>{ vendedores[1].nome }</p>
              </div>
              <div className='card-round'>
                <img className='img-card-r' src='https://media.istockphoto.com/photos/portrait-of-handsome-afro-man-using-his-mobile-picture-id890698790?b=1&k=6&m=890698790&s=170x170&h=DvikSm6ODniIGYcbjcYe5vxJFXZTPm_dNRryFFMIC3Y=' alt='foto estatica' />
                <p className='nome-vendedores-home'>{ vendedores[2].nome }</p>
              </div>
              <div className='card-round'>
                <img className='img-card-r' src='https://images.freeimages.com/images/small-previews/0c6/people-1241171.jpg' alt='foto estatica'/>
                <p className='nome-vendedores-home'>{ vendedores[3].nome }</p>
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    )
  }
}
/* 
const CardRound1111 = (props) => {
  const vendedores = props.vendedores;

  return (
    vendedores && (
      <React.Fragment>
        <div className='card'>
          <p className='escuro'>muita maravilhosidade</p>
          <p className='claro'>nessa famosidade</p>
          <div className='card-4-col'>
            <div className='card-round'>
              <img className='img-card-r' src='https://media.istockphoto.com/photos/nice-beautiful-lady-with-blonde-curly-hair-work-at-the-notebook-sit-picture-id1087959426?b=1&k=6&m=1087959426&s=170x170&h=2W3PfqTV5w53WMi6REF9sDm6Q_1JwH9Sk-UjTOvvOkg=' alt='foto estatica' />
              <p>{vendedores[0].nome}</p>
            </div>
            <div className='card-round'>
              <img className='img-card-r' src='https://media.istockphoto.com/photos/woman-using-smartphone-picture-id1134002289?b=1&k=6&m=1134002289&s=170x170&h=d6dBRl5lsrgCc5X50sPfEBFV9mgrgNmGK5E0-ndd5nI=' alt='foto estatica' />
              <p>vendedores[1].nome</p>
            </div>
            <div className='card-round'>
              <img className='img-card-r' src='https://media.istockphoto.com/photos/portrait-of-handsome-afro-man-using-his-mobile-picture-id890698790?b=1&k=6&m=890698790&s=170x170&h=DvikSm6ODniIGYcbjcYe5vxJFXZTPm_dNRryFFMIC3Y=' alt='foto estatica' />
              <p>vendedores[2].nome</p>
            </div>
            <div className='card-round'>
              <img className='img-card-r' src='https://images.freeimages.com/images/small-previews/0c6/people-1241171.jpg' alt='foto estatica'/>
              <p>vendedores[3].nome</p>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  )

}

 

export default CardRound1111; */