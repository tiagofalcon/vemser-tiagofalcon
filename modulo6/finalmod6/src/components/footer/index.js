import React, { Component } from 'react';

import './footer.css';

export default class Footer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer className='footer'>
        <div className='line'></div>
        <div className='grid-footer'>
          <div>
            <ul>
              <li className='footer-li footer-li-header'>utilidades</li>
              <li className='footer-li footer-li-item'>ajuda</li>
              <li className='footer-li footer-li-item'>como vender</li>
              <li className='footer-li footer-li-item'>como comprar</li>
              <li className='footer-li footer-li-item'>marcas</li>
              <li className='footer-li footer-li-item'>termos de uso</li>
              <li className='footer-li footer-li-item'>política de privacidade</li>
              <li className='footer-li footer-li-item'>trabalhe no enjoei</li>
              <li className='footer-li footer-li-item'>black friday</li>
              <li className='footer-li footer-li-item'>investidores</li>
            </ul>
          </div>
          <div>
            <ul>
              <li className='footer-li footer-li-header'>minha conta</li>
              <li className='footer-li footer-li-item'>minha loja</li>
              <li className='footer-li footer-li-item'>mais vendidas</li>
              <li className='footer-li footer-li-item'>minhas compras</li>
              <li className='footer-li footer-li-item'>enjubank</li>
              <li className='footer-li footer-li-item'>yeyezados</li>
              <li className='footer-li footer-li-item'>configurações</li>
            </ul>
          </div>
          <div>
            <ul>
              <li className='footer-li footer-li-header'>marcas populares</li>
              <div className='li-grid-in'>
                <div>
                  <ul className='no-padding'>
                    <li className='footer-li footer-li-item'>farm</li>
                    <li className='footer-li footer-li-item'>melissa</li>
                    <li className='footer-li footer-li-item'>forever 21</li>
                    <li className='footer-li footer-li-item'>nike</li>
                    <li className='footer-li footer-li-item'>adidas</li>
                    <li className='footer-li footer-li-item'>kipling</li>
                    <li className='footer-li footer-li-item'>ver todas</li>
                  </ul>
                </div>
                <div>
                  <ul className='no-padding'>
                    <li className='footer-li footer-li-item'>zara</li>
                    <li className='footer-li footer-li-item'>arezzo</li>
                    <li className='footer-li footer-li-item'>schultz</li>
                    <li className='footer-li footer-li-item'>tommy hilfiger</li>
                    <li className='footer-li footer-li-item'>antix</li>
                    <li className='footer-li footer-li-item'>apple</li>
                  </ul>
                </div>
              </div>
            </ul>
          </div>
          <div>
            <ul>
              <li className='footer-li footer-li-header'>siga a gente</li>
              <li className='footer-li footer-li-item'>facebook</li>
              <li className='footer-li footer-li-item'>twitter</li>
              <li className='footer-li footer-li-item'>instagram</li>
            </ul>
          </div>
        </div>
      </footer>
    )
  }
}



/* 

utilidades
ajuda
como vender
como comprar
marcas
termos de uso
política de privacidade
trabalhe no enjoei
black friday
investidores
minha conta
minha loja
minhas vendas
minhas compras
enjubank
yeyezados
configurações
marcas populares
farm
zara
melissa
arezzo
forever 21
schutz
nike
tommy hilfiger
adidas
antix
kipling
apple
ver todas
siga a gente
facebook
twitter
instagram

*/