import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

export default class Card141 extends Component {

  render() {
    const { categoria, produtos } = this.props;

    return (
      <React.Fragment>
        <div className='card'>
          <div className='desc-vai-para-la'>
            <div>
              <p className='escuro'>{ categoria.nome }</p>
              <p className='claro'>{ categoria.subTitulo }</p>
            </div>
            <div className='vai-para'>
              <span className='vai'>vai para lá</span><ArrowForwardIosIcon fontSize='small' className='vai' />
            </div>
          </div>
          <div className='card-3-col'>
            <div className='card-1'>
              <Link to={ `/produto/${ produtos[0].id }` } ><img className='img-card' src={produtos[0].imagem} alt={ `Imagem de ${ produtos[0].nome }` } /></Link>
              <p className='preco'>R$ {produtos[0].preco}</p>
            </div>
            <div className='card-4'>
              <div className='fix-card-4'>
                <div>
                  <Link to={ `/produto/${ produtos[1].id }` } ><img className='img-card' src={produtos[1].imagem} alt={ `Imagem de ${ produtos[1].nome }` } /></Link>
                  <p className='preco'>R$ {produtos[1].preco}</p>
                </div>
                <div>
                  <Link to={ `/produto/${ produtos[2].id }` } ><img className='img-card' src={produtos[2].imagem} alt={ `Imagem de ${ produtos[2].nome }` } /></Link>
                  <p className='preco'>R$ {produtos[2].preco}</p>
                </div>
              </div>
              <div className='fix-card-4'>
                <div >
                  <Link to={ `/produto/${ produtos[3].id }` } ><img className='img-card' src={produtos[3].imagem} alt={ `Imagem de ${ produtos[3].nome }` } /></Link>
                  <p className='preco'>R$ {produtos[3].preco}</p>
                </div>
                <div className='fix-rate'>
                  <Link to={ `/produto/${ produtos[4].id }` } ><img className='img-card' src={produtos[4].imagem} alt={ `Imagem de ${ produtos[4].nome }` } /></Link>
                  <p className='preco'>R$ {produtos[4].preco}</p>
                </div>
              </div>
            </div>
            <div className='card-1'>
              <Link to={ `/produto/${ produtos[5].id }` } ><img className='img-card' src={produtos[5].imagem} alt={ `Imagem de ${ produtos[5].nome }` } /></Link>
              <p className='preco'>R$ {produtos[5].preco}</p>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }

}
 

