import React, { Component } from 'react';
import Rating from '@material-ui/lab/Rating';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';
import Box from '@material-ui/core/Box';import { Link } from '@material-ui/core';
import LojaApi from '../../models/lojaApi';
import Comentario from '../comentario';

export default class CardInterno3 extends Component {

  constructor(props) {
    super(props);
    this.lojaApi = new LojaApi();
    this.state = {
      detalhes: null,
      comentarios: [],
      vendedor: null
    } 
  }

  componentDidMount() {
    const { produto } = this.props;

    const requisicoes = [
      this.lojaApi.buscarDetalhes(produto.id),
      this.lojaApi.buscarTodosComentariosdeUmProduto(produto.id),
      this.lojaApi.buscarVendedor(produto.idVendedor)
    ];

    Promise.all(requisicoes).then( respostas => {
      respostas[1].sort((a,b) => b.id - a.id)
      this.setState( {
        detalhes: respostas[0],
        comentarios: respostas[1],
        vendedor: respostas[2]
      } )
    })
  }



  render() {
    const { produto } = this.props;
    const { detalhes, comentarios, vendedor } = this.state;

    const monthNames = ["jan", "fev", "mar", "abr", "mai", "jun",
      "jul", "ago", "set", "out", "nov", "dez"
    ];

    return (
      detalhes && (
        <React.Fragment>
          <div className='cardIn'>
          <div className='card-in-3-col'>
              <div className='card-in-3-maior'>
                <div className='card-in-1'>
                  <img className='img-card-in' src={ produto.imagem } alt={ `Imagem de ${ produto.nome }` } />
                  <img className='img-card-in-mini' src={ produto.imagem } alt={ `Imagem de ${ produto.nome }` } />
                  <Comentario produto={ produto } detalhes={ detalhes } />
                  <div className='perguntas'>
                    <p className='perguntas-texto'>últimas perguntas</p>
                    { comentarios.map( comentario => <p className='small-text' key={ comentario.id }>{comentario.comentario}</p>) }
                  </div>
                </div>
              </div>
              <div className='card-in-3-menor'>
                <div className='icons-middle'><InsertEmoticonIcon fontSize='small' /></div>
                <div className='icons-middle'><ChatOutlinedIcon fontSize='small' /></div>
              </div>
              <div className='card-in-3-maior'>
                <p className='small-header'>moças / acessorios / cintos</p>
                <p className='titulo-produto'>{ produto.nome }</p>
                <p className='marca-header'>{ detalhes.marca } | <span className='span-pink'>seguir marca</span></p>
                <span className='span-pink'>R$ { produto.preco } </span><span className='span-gray'>R$ 150</span>
                <Link to='#' ><button className='button-sd button-pink'>eu quero</button></Link>
                <Link to='#' ><button className='button-sd button-white'>adicionar à sacolinha</button></Link>
                <Link to='#' ><button className='button-sd button-white'>fazer oferta</button></Link>

                <div className='especificacoes'>
                  <div className='spec-tam'>
                    <p className='small-header'>tamanho</p>
                    <p className='small-text'>{detalhes.tamanho ? detalhes.tamanho : '-' }</p>
                  </div>
                  <div className='grid-spec'>
                    <div className='espec-item espec-item-1'>
                      <p className='small-header'>marca</p>
                      <p className='small-text'>{ detalhes.marca }</p>
                    </div>
                    <div className='espec-item'>
                      <p className='small-header'>condição</p>
                      <p className='small-text'>{ detalhes.condicao }</p>
                    </div>
                    <div className='espec-item'>
                      <p className='small-header'>código</p>
                      <p className='small-text'>{ detalhes.codigo }</p>
                    </div>
                  </div>
                  <div className='descricao-prod'>
                    <p className='small-header'>descricao</p>
                    <p className='small-text'>{ detalhes.descricao }</p>
                  </div>
                  
                  <div className='spec-tam spec-tam-n'>
                    <div className='left'>
                      <p className='small-text small-text-bot'>{ vendedor.nome }</p>
                      <p className='small-text small-text-2'>{ vendedor.cidade }</p>
                    </div>
                    <div className='right'>
                      <button className='button-sm button-white btn40'>seguir</button>
                    </div>
                  </div>
                  <div className='grid-spec'>
                    <div className='espec-item espec-item-1'>
                      <p className='small-header'>avaliação</p>
                      <Box component="fieldset" mb={3} borderColor="transparent">
                        <Rating name="read-only" 
                                size={'small'}
                                value={ 5 } 
                                readOnly 
                                precision={0.5} 
                        />
                      </Box>
                    </div>
                    <div className='espec-item'>
                      <p className='small-header'>últimas entregas</p>
                      <Box component="fieldset" mb={3} borderColor="transparent">
                        <Rating name="read-only" 
                                size={'small'}
                                icon={<FiberManualRecordIcon className='radio-sm' fontSize={'small'}/>}
                                value={ 5 } 
                                readOnly 
                                precision={0.5} 
                        />
                      </Box>
                    </div>
                    <div className='espec-item'>
                      <p className='small-header'>tempo médio de envio</p>
                      <p className='small-text'>{ vendedor.tempoMedio }</p>
                    </div>
                  </div>
                  <div className='grid-spec'>
                    <div className='espec-item espec-item-1'>
                      <p className='small-header'>à venda</p>
                      <p className='small-text'>41</p>
                    </div>
                    <div className='espec-item'>
                      <p className='small-header'>vendidos</p>
                      <p className='small-text'>{ vendedor.qtdItensVendidos }</p>
                    </div>
                    <div className='espec-item'>
                      <p className='small-header'>no enjoei desde</p>
                      <p className='small-text'>
                        { monthNames[ new Date( vendedor.criacao ).getMonth() ] }/
                        { new Date( vendedor.criacao ).getFullYear() }
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    )
  }
}
