import React from 'react';
import PropTypes from 'prop-types';
import LocalShippingOutlinedIcon from '@material-ui/icons/LocalShippingOutlined';
import LocalAirportIcon from '@material-ui/icons/LocalAirport';

const TarjaInterna = ( { ehInterna } ) =>
 
    ehInterna ? (
      <React.Fragment>
        <div className='nav-part nav-tarja'>
          <div className='tarja-in'>
            <LocalShippingOutlinedIcon />
            <span className='nav-span-bold'>frete grátis na primeira compra</span>
          </div>
          <div className='tarja-in'>
          <span> - promo válida no frete padrão até R$ 20</span>
          </div>
        </div>
      </React.Fragment>
    ) : (
      <div className='nav-part nav-tarja'>
          <div className='tarja-in'>
            <span className='nav-span-bold'>voa, frete, voa</span>
            <LocalAirportIcon />
            <span> só R$ 3,99</span>
          </div>
          <div className='tarja-in tarja-in2'>
            <span> - por tempo limitado nesse <span className='nav-span-underline'>baaando de produtos</span></span>      
          </div>
      </div>
    )


  
TarjaInterna.propTypes = {
    ehInterna: PropTypes.bool
}

TarjaInterna.defaultProps = {
  ehInterna: false
}

export default TarjaInterna;