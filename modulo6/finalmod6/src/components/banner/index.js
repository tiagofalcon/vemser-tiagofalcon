import React, { Component } from 'react';

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import LojaApi from '../../models/lojaApi';

export default class Banner extends Component {
  
  constructor(props) {
    super(props);
    this.lojaApi = new LojaApi();
    this.state = {
      banners: [],
    } 
  }

  componentDidMount() {
    this.lojaApi.buscarBanners()
    .then( resposta => {
      this.setState( {
        banners: resposta,
      } )
    })
  }

  render() {

    const { banners } = this.state;
    
    return (
      banners.length > 0 && (
        <div className='banner'>
          <Carousel autoPlay={500} infiniteLoop showThumbs={false} showArrows >
            <div>
                <img src={ banners[0].imagem } className='banner' alt={ `Banner ${ banners[0].id }` } />
            </div>
            <div>
                <img src={ banners[1].imagem } className='banner' alt={ `Banner ${ banners[1].id }` } />
            </div>
            <div>
                <img src={ banners[2].imagem } className='banner' alt={ `Banner ${ banners[2].id }` } />
            </div>
            <div>
                <img src={ banners[3].imagem } className='banner' alt={ `Banner ${ banners[3].id }` } />
            </div>
          </Carousel>
        </div>
      )
    )
  }
}