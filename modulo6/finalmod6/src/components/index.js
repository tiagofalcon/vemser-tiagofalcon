import Banner from './banner';
import Card141 from './card141';
import CardInterno3 from './cardInterno3';
import CardInterno51 from './cardInterno5-1';
import CardRound1111 from './cardRound1111';
import Comentario from './comentario';
import Footer from './footer';
import NavigationUi from './navigationUi';

export { Banner, Card141, CardInterno3, CardInterno51, CardRound1111, Comentario, NavigationUi, Footer };