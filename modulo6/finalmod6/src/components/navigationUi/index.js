import React from 'react';
import { Link } from 'react-router-dom';
import { Input, InputAdornment } from '@material-ui/core';

import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import ContactSupportOutlinedIcon from '@material-ui/icons/ContactSupportOutlined';

import Logo from '../../assets/img/enjoicon.png';

import './navigationUi.css';
import TarjaInterna from '../tarjaInterna';
import LojaApi from '../../models/lojaApi';

const removeUser = (e) => {
  e.preventDefault();
  localStorage.removeItem('user');
  window.location.reload();
}

const buscarPorTermoRetornaPrimeiro = async (e) => {
  e.preventDefault();
  const lojaApi = new LojaApi();

  const produtos = await lojaApi.buscar().then( resultado => resultado )

  lojaApi.filtrarPorTermo( e.target.value ).then( ( resultados ) => {
    let lista = [];
    resultados.forEach( ( element ) => {
      produtos.forEach((ep) => {
        if (element.id === ep.id) {
          lista.push(ep);
        }
      });
    });
    if ( lista.length > 0 ) {
      window.location.replace( `http://localhost:3000/produto/${ lista[0].id }` )
    }
  });
}

const NavigationUi = (props) =>
  <div className='nav'>
    <TarjaInterna ehInterna={props.ehInterna} />
    <div className='nav-part1 nav-option'>
      <div className='nav-p1'>

        <Link to='/'><img className='logo' src={Logo} alt='logo' /></Link>
        <Input id="input-with-icon-adornment"
          placeholder={`busque`}
          endAdornment={
            <InputAdornment position="end">
              <SearchOutlinedIcon />
            </InputAdornment>
          }
          onBlur={ e => buscarPorTermoRetornaPrimeiro( e ) }
        />
      </div>
      <div className='nav-p1 nav-p2'>
        <div>
          <Link to='/' className='nav-link'>moças</Link>
          <Link to='/' className='nav-link'>rapazes</Link>
          <Link to='/' className='nav-link'>kids</Link>
          <Link to='/' className='nav-link'>casal&amp;tal</Link>
        </div>
        <div className='nav-p2-in'>
          <Link to='/' className='nav-link'><ContactSupportOutlinedIcon /></Link>
          {
            localStorage.getItem('user') ? 
              <Link to='/login' className='nav-link' onClick={e => removeUser(e) }>sair</Link> :
              <Link to='/login' className='nav-link'>entrar</Link>
          }
          <Link to='/' className='nav-link'>
            <button className='nav-button'>quero vender</button>
          </Link>
        </div>
      </div>
    </div>
  </div>

export default NavigationUi;