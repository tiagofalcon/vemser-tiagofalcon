import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LojaApi from '../../models/lojaApi';


export default class CardInterno51 extends Component {

  constructor(props) {
    super(props);
    this.lojaApi = new LojaApi();
    this.state = {
      listaProdutos: [],
      listaDetalhes:[],
      produtosVendedor:[]
    } 
  }

  componentDidMount() {
    let prodVend = []
    this.lojaApi.buscar()
    .then( resposta => {
      resposta.forEach(element => {
        if( element.idVendedor === this.props.produto.idVendedor ) {
          if(element.id !== this.props.produto.id){
            prodVend.push( element );
          }
        }
      });
      this.setState( {
        listaProdutos: resposta,
        produtosVendedor: prodVend
      } )
    })

  }

  gerarMultiplosAleatorios( qtd, arrayProdutos) {
    let retorno = [];
    for (let i = 0; i < qtd; i++ ) {
      retorno.push( this.elementoAleatorio( arrayProdutos ))
    }

    return retorno;
  }

  elementoAleatorio( arrayProdutos ) {
    return arrayProdutos[Math.floor(Math.random() * arrayProdutos.length)];
  }


  render() {
    const { detalhe } = this.props;
    const { listaProdutos, produtosVendedor } = this.state;
    let aleatorios = this.gerarMultiplosAleatorios( 10, listaProdutos );
    let aleatoriosVendedor = this.gerarMultiplosAleatorios( 5, produtosVendedor );    

    return (
      listaProdutos.length > 0 && (
        detalhe ? (
          <React.Fragment>
            <div className='cardIn'>
              <p className='titulo-card-5'>você também vai curtir</p>
              <div className='card-in-5-col'>
                <div className='card-5-sd'>
                  <div className='img-desc'>
                    <Link to={ `/produto/${ aleatorios[0].id }` } ><img className='img-card-5-in' src={ aleatorios[0].imagem } alt={ `Imagem de ${ aleatorios[0].nome }` }  /></Link>
                    <div className='desc-pink'>20%</div>
                  </div>
                  <span className='span-pink span-pink-sm'>R$ { aleatorios[0].preco } </span>
                  <p className='small-header'>{aleatorios[0].nome}</p>
                </div>
                <div className='card-5-sd'>
                  <div className='img-desc'>
                    <Link to={ `/produto/${ aleatorios[1].id }` } ><img className='img-card-5-in' src={ aleatorios[1].imagem } alt={ `Imagem de ${ aleatorios[1].nome }` } /></Link>
                    <div className='desc-pink'>20%</div>
                  </div>
                  <span className='span-pink span-pink-sm'>R$ { aleatorios[1].preco } </span>
                  <p className='small-header'>{aleatorios[1].nome}</p>
                </div>
                <div className='card-5-sd'>
                  <div className='img-desc'>
                    <Link to={ `/produto/${ aleatorios[2].id }` } ><img className='img-card-5-in' src={ aleatorios[2].imagem } alt={ `Imagem de ${ aleatorios[2].nome }` } /></Link>
                    <div className='desc-pink'>20%</div>
                  </div>
                  <span className='span-pink span-pink-sm'>R$ { aleatorios[2].preco } </span>
                  <p className='small-header'>{aleatorios[2].nome}</p>
                </div>
                <div className='card-5-sd'>
                  <div className='img-desc'>
                    <Link to={ `/produto/${ aleatorios[3].id }` } ><img className='img-card-5-in' src={ aleatorios[3].imagem } alt={ `Imagem de ${ aleatorios[3].nome }` } /></Link>
                    <div className='desc-pink'>20%</div>
                  </div>
                  <span className='span-pink span-pink-sm'>R$ { aleatorios[3].preco } </span>
                  <p className='small-header'>{aleatorios[3].nome}</p>
                </div>
                <div className='card-5-sd'>
                  <div className='img-desc'>
                    <Link to={ `/produto/${ aleatorios[4].id }` } ><img className='img-card-5-in' src={ aleatorios[4].imagem } alt={ `Imagem de ${ aleatorios[4].nome }` } /></Link>
                    <div className='desc-pink'>20%</div>
                  </div>
                  <span className='span-pink span-pink-sm'>R$ { aleatorios[4].preco } </span>
                  <p className='small-header'>{aleatorios[4].nome}</p>
                </div>
              </div>
            </div>
          </React.Fragment>
        ) : 
        (
          <React.Fragment>
            <div className='cardIn'>
              <p className='titulo-card-5'>Esta loja oferece até <span className='titulo-card-5-pink'>30% de desconto + frete único</span> nas compras de sacolinha</p>
              <div className='card-in-5-col'>
                
                {
                  aleatoriosVendedor.map( prod => {
                    return (
                      <div className='card-5-sd' key={ prod.id }>
                        <div className='img-desc'>
                          <Link to={ `/produto/${ prod.id }` } ><img className='img-card-5-in' src={ prod.imagem } alt={ `Imagem de ${ prod.nome }` } /></Link>
                          <div className='desc-pink'>20%</div>
                        </div>
                        <span className='span-pink span-pink-sm'>R$ { prod.preco } </span>
                        <p className='small-header'>{prod.nome}</p>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </React.Fragment>
        )
      )
    )
  }
}
