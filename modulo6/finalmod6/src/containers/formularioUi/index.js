import React, { useState } from 'react';
import { NavigationUi } from '../../components';
import LojaApi from '../../models/lojaApi';

import './formulario.css';

function Formulario(props) {
  const [ user, setUser ] = useState('');
  const [ login, setLogin ] = useState('');
  const [ senha, setSenha ] = useState('');

  const handler = (e) => {
    e.preventDefault();
    const lojaApi = new LojaApi();
    lojaApi.cadastrarUsuario( { nome:user, login: login, senha: senha, logado:false } )
  }

  return (
    <React.Fragment>
      <NavigationUi />
      <form className='formulario' onSubmit={ handler }>
        <div className='formulario-container'>
          <div className='itens'>
            <label>Nome</label>
            <input type='text' name='nome' onChange={ ev => setUser(ev.target.value) } />
          </div>
          <div className='itens'>
            <label>Login</label>
            <input type='email' name='login' onChange={ ev => setLogin(ev.target.value) } />
          </div>
          <div className='itens'>
            <label>Senha</label>
            <input type='password' name='senha' onChange={ ev => setSenha(ev.target.value) } />
            <button type='submit'className='button-form btn-submit' >Salvar</button>
          </div>
        </div>
      </form>
    </React.Fragment>
  )
}

export default Formulario;