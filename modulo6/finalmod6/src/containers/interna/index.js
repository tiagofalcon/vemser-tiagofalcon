import React, { Component } from 'react';

import { CardInterno3, CardInterno51, Footer, NavigationUi } from '../../components';

import LojaApi from '../../models/lojaApi.js';


import './interna.css';

export default class Interna extends Component {
  constructor(props) {
    super(props);
    this.lojaApi = new LojaApi();
    this.state = {
      idProduto: this.props.match.params.id,
      produto: null,
      listaPodutos: [],
    } 
  }

  componentDidMount() {
    const produtosId = this.state.idProduto;
    const requisicoes = [
      this.lojaApi.buscarProduto( produtosId ),
      this.lojaApi.buscar(),
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        
        this.setState( {
          produtosId: produtosId,
          produto: respostas[0],
          listaPodutos: respostas[1],
        } )
        
      })
    

  }

  render() {
    const { produto, listaPodutos } = this.state;
    return produto && listaPodutos && (
        <React.Fragment>
          <NavigationUi ehInterna={ true } />
          <CardInterno3 produto={ produto } />
          <CardInterno51 produto={ produto } detalhe={ false } />
          <CardInterno51 produto={ produto } detalhe={ true } />
          <Footer />
          {produto.id == this.props.match.params.id ? null : window.location.reload()}
        </React.Fragment>
    )
      
    
  }
}