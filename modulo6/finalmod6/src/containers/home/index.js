import React, { Component } from 'react';

import { Banner, Card141, CardRound1111, Footer, NavigationUi } from '../../components';
import LojaApi from '../../models/lojaApi.js';

import './home.css';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.lojaApi = new LojaApi();
    this.state = {
      listaCategorias: [],
      listaPodutos: [],
    } 
  }

  componentDidMount() {
    let categorias = [];
    let produtos = [];

    const requisicoes = [
      this.lojaApi.buscarTodosCategorias(),
      this.lojaApi.buscar(),
    ];

    Promise.all(requisicoes).then((respostas) => {
      categorias = respostas[0];
      produtos = respostas[1];
      
      this.setState((state) => {
        return {
          ...state,
          listaCategorias: categorias,
          listaPodutos: produtos,
        };
      });
    });


  }

  render() {

    const { listaCategorias, listaPodutos } = this.state;

    return (
      <React.Fragment>
        <NavigationUi ehInterna={false} />  
        <Banner />
        
        { 
        listaCategorias.map( ( element ) => {
          let produtos=[];
          listaPodutos.forEach( prod => {
            if(prod.idCategoria === element.id) {
              produtos.push( prod );
            }
          })
          return (
            <Card141 
            key={ element.id }
            categoria={ element }
            produtos={ produtos }
          />
          )
        })
        }
        <CardRound1111 />

        { 
        listaCategorias.map( ( element ) => {
          let produtos=[];
          listaPodutos.forEach( prod => {
            if(prod.idCategoria === element.id) {
              produtos.push( prod );
            }
          })
          return (
            <Card141 
            key={ element.id }
            categoria={ element }
            produtos={ produtos }
          />
          )
        })
        }
        <Footer />
      </React.Fragment>
    );
  }
}