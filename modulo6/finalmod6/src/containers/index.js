import Formulario from './formularioUi';
import Home from './home';
import Interna from './interna';
import Login from './loginUi';

export { Formulario, Home, Interna, Login };