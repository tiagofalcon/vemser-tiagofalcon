import React, { useEffect, useState } from 'react';
import { Link } from  'react-router-dom';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import Logo from '../../assets/img/enjoicon.png';
import LojaApi from '../../models/lojaApi';

import './login.css';

function Login(props) {
  const [ user, setUser ] = useState();
  const [ login, setLogin ] = useState('');
  const [ senha, setSenha ] = useState('');

  const handler = ( e ) => {
    e.preventDefault();
    const lojaApi = new LojaApi();
    lojaApi.buscarUsuarioEspecifico(login, senha)
      .then( resposta => {
        setUser(resposta)
      })
    
    if( user ) {
      localStorage.setItem('user', 'logado')
      props.history.push('/');
    }

  }

  useEffect( () => {
    const lojaApi = new LojaApi();

    async function fetchData() {
      await lojaApi.buscarUsuarioEspecifico(login, senha)
        .then( resposta => {
          setUser(resposta)
        })
    }
    fetchData();
  },[]);

  return (
    <React.Fragment>
      <div className='nav'>
        <div className='nav-logo'>
          <div className='arrow-back'>
          <Link to='/'><ArrowBackIcon className='bckicon'/></Link>
          </div>
          <div className='so-logo'>
            <Link to='/'><img className='logo' src={Logo} alt='logo' /></Link>
          </div>
          <div></div>
        </div>
      </div>
      <form className='formulario' onSubmit={ handler }>
        <div className='formulario-container'>
          <span className='login-bold'>faça login no enjoei</span>

          <div className='itens'>
            <Link to='#' ><button className='button-facebook'>entre usando o facebook</button></Link>
          </div>
          <div className='itens'>
            ou
          </div>

          <div className='itens'>
            <div className='container-label'>
              <span className='label'>email</span>
            </div>
            <input className='form-input' type='text' name='login' onBlur={ ev => setLogin(ev.target.value) } />
          </div>
          <div className='itens'>
            <div className='container-label'>
              <span className='label'>senha supersecreta</span>
            </div>
            <input className='form-input' type='password' name='senha' onBlur={ ev => setSenha(ev.target.value) } />
            <div className='check-forget'>
              <div className='container-label'>
                <input className='input-check' type='checkbox' /> <span className='span-check'>continuar conectado</span>
                <div className='span-forget-container'>
                  <span className='span-forget'>esqueci a senha</span>
                </div>
              </div>
            </div>
          </div>
          <div className='itens'>
            <button className='button-form button-entrar' type='submit'  >entrar</button>
          </div>
          <div>
            <Link className='link-login' to='/formulario'>não tenho conta</Link>
          </div>
        </div>
      </form>
      <footer>
        <div className='login-footer'>
          <span className='footer-text'>Protegido por reCAPTCHA - <span className='footer-pink'>Privacidade - Condições</span></span>
        </div>
      </footer>
    </React.Fragment>
  )
}

export default Login;