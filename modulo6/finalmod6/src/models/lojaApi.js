import axios from 'axios';

const url = 'http://localhost:9000/api/';
const _get = url => new Promise( (resolve, reject ) => axios.get( url ).then( response => resolve(response.data) ) );
const _post = ( url, dados ) => new Promise( (resolve, reject ) => axios.post( url, dados ).then( response => resolve(response.data) ).catch( reject => console.log( reject ) ) );

export default class LojaApi {
  
  async buscar() {
    return await _get( `${ url }produtos` );
  }

  async buscarBanners() {
    return await _get( `${ url }banners` );
  }
  
  async buscarTodosDetalhes() {
    return await _get( `${ url }detalhes` );
  }

  async buscarTodosComentarios() {
    return await _get( `${ url }comentarios` );
  }

  async buscarTodosComentariosdeUmProduto( id ) {
    return await _get( `${ url }comentarios?idProduto=${ id }` );
  }

  async buscarTodosCategorias() {
    return await _get( `${ url }categorias` );
  }

  async buscarTodosVendedores() {
    return await _get( `${ url }vendedor` );
  }
  
  async buscarProduto( id ) {
    const response = await _get( `${ url }produtos?id=${ id }` );
    return response[ 0 ];
  }

  async buscarVendedor( id ) {
    const response = await _get( `${ url }vendedor?id=${ id }` );
    return response[ 0 ];
  }

  async buscarDetalhes( id ) {
    const response = await _get( `${ url }detalhes?idProduto=${ id }` );
    return response[ 0 ];
  }

  async registrarComentario( { idProduto, nomeProduto, codigoProduto, nomeUser, idVendedor, comentario  } ) {
    const response = await _post( `${ url }comentarios`, { idProduto, nomeProduto, codigoProduto, nomeUser, idVendedor, comentario  } );
    return response[ 0 ];
  }

  async filtrarPorCategoria ( idCategoria ) {
    const response =  _get( `${ url }produtos?idCategoria=${ idCategoria }`);
    return response[ 0 ];
  }
  
  async buscarUsuarioEspecifico( login, senha ) {
    return await _post( `${ url }cadastro`, {}, {
      auth: {
        login: login,
        senha: senha
      }
    } );
  }

  async cadastrarUsuario( { nome, login, senha  } ) {
    if( (nome == '') || (login.legth == '' ) || (senha == '') ) {
      return;
    }
    const response = await _post( `${ url }cadastro`, { nome, login, senha  } );
    return response[ 0 ];
  }

  async filtrarPorTermo ( termo ) {
    return await _get( `${ url }detalhes?q=${ termo }`);
  }

}