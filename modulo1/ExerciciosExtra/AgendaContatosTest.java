import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest {
    
    @Test
    public void adicionaItemNaAgenda() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        assertEquals(1, agenda.getContatos().size());
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getContatos().size());
    }
    
    @Test
    public void retornaConsultaPesquisaPorTelefone() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getContatos().size());
        String esperado = agenda.consultar("Marcos");
        String esperado2 = agenda.consultar("Mithrandir");
        assertTrue("555555".equals(esperado));
        assertTrue("444444".equals(esperado2));
    }
    
    @Test
    public void retornaContatoPorNome(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getContatos().size());
        String esperado = agenda.pesquisar("555555");
        String esperado2 = agenda.pesquisar("444444");
        assertTrue("Marcos".equals(esperado));
        assertTrue("Mithrandir".equals(esperado2));
    }
    
    
    
    @Test
    public void adicionarDoisContatosEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        String separador = System.lineSeparator();
        String resultado = String.format("Marcos,555555%sMithrandir,444444%s", separador,separador);
        assertEquals(resultado, agenda.csv());
    }
    
    @Test
    public void adicionarContatoEPesquisa(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        String resultado = agenda.consultar("Marcos");
        assertEquals("555555", resultado);
    }
    
    @Test
    public void adicionarContatoEPesquisaPeloTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        String resultado = agenda.pesquisar("555555");
        assertEquals("Marcos", resultado);
    }
    
    /*
     * 
     * 
     * @Test
    public void adicionarDoisContatosEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        String separador = "System.lineseparator();
        String resultado = agenda.consultar("Marcos");
        assertEquals("555555", resultado);
    }
     * 
     * 
     * 
     * 
    @Test
    public void adicionaItemNaAgenda() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        assertEquals(1, agenda.getContatos().size());
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getContatos().size());
    }
    
    @Test
    public void adicionaContatoEPesquisa() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        assertEquals(1, agenda.getContatos().size());
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getContatos().size());
    }
    
    @Test
    public void retornaConsultaPesquisaPorTelefone() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getContatos().size());
        String esperado = agenda.consultar("Marcos");
        String esperado2 = agenda.consultar("Mithrandir");
        assertTrue("555555".equals(esperado));
        assertTrue("444444".equals(esperado2));
    }
    
    @Test
    public void retornaContatoPorNome(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(2, agenda.getAgenda().size());
        String esperado = agenda.pesquisar("555555");
        String esperado2 = agenda.pesquisar("444444");
        assertTrue("Marcos,555555".equals(esperado));
        assertTrue("Mithrandir,444444".equals(esperado2));
    }
    
    @Test
    public void deveRetornarCSVOrdenadaAlfabeticamente(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Tiago", "000000");
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        assertEquals(3, agenda.getAgenda().size());
        String esperado = "Marcos,555555\nMithrandir,444444\nTiago,000000\n";
        assertTrue(esperado.equals(agenda.csv()));
    }*/
}

