import java.util.*;
public class EstrategiaResistente extends Estrategias {
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> elfos = elfosVivos(atacantes);
        elfos = verificaTemFlechas(elfos);
        elfos = verificaQtdElfosNoturnos(elfos);
        elfos = organizaPorFlechasDescendente(elfos);
        return elfos;
    }
    
    private ArrayList<Elfo> verificaTemFlechas(ArrayList<Elfo> elfos) {
        ArrayList<Elfo> retorno = new ArrayList<> ();
        for(Elfo elfo : elfos) {
            if(elfo.getQtdFlechas() > 0){
            retorno.add(elfo);
            }
        }
        return retorno;
    }
    
    private ArrayList<Elfo> verificaQtdElfosNoturnos(ArrayList<Elfo> elfos) {
        int qtdElfoNoturno = 0;
        int valorDeCorte = (int) Math.floor(0.3 * elfos.size());
        ArrayList<Elfo> retorno = new ArrayList<Elfo>();
        for(Elfo elfo : elfos) {
            qtdElfoNoturno = elfo instanceof ElfoNoturno ? qtdElfoNoturno + 1 : qtdElfoNoturno;
        }
        if(qtdElfoNoturno > valorDeCorte){
            int diferenca = qtdElfoNoturno - valorDeCorte;
            diferenca *= -1;
            retorno = removeDiferenca(diferenca, elfos);
            retorno = verificaQtdElfosNoturnos(retorno);
        } else {
            retorno = elfos;
        }
        return retorno;
    }
    
    private ArrayList<Elfo> organizaPorFlechasDescendente(ArrayList<Elfo> elfos) {
        Elfo ajudante;
        for(int i = 1; i < elfos.size(); i++){
            /*for(int j = 0; j < elfos.size() - 1; j++){
                if(elfos.get(i).getQtdFlechas() < elfos.get(j + 1).getQtdFlechas()){
                    ajudante = elfos.get(i);
                    elfos.set(i, elfos.get(j + 1));
                    elfos.set(j + 1, ajudante);
                }
            }*/
            ajudante = elfos.get(i);
            int j = i - 1;
            while(j >= 0 && ajudante.getQtdFlechas() > elfos.get(j).getQtdFlechas()){
                elfos.set(j+1, elfos.get(j));
                j--;
            }
            elfos.set(j + 1, ajudante); 
        }
        return elfos;
    }
}
