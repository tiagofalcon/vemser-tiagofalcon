import java.util.*;

public class EstatisticasInventario {
    
    Inventario inventario;
    int tamanhoDoInventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
        this.tamanhoDoInventario = this.inventario.getItens().size();
    }
    
    public double calcularMedia() {
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        double total = 0;
        for(Item item : this.inventario.getItens()) {
            total += item.getQuantidade();
        }
        return (double) total/this.tamanhoDoInventario;
    }
    
    public double calcularMediana() {
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        double mediana = 0;
        ArrayList<Integer> valores = this.ordenaValores();
        if(valores.size() % 2 == 0){
            int index1 = (valores.size() / 2) - 1;
            int index2 = (valores.size() / 2);
            int soma = valores.get(index1) + valores.get(index2);
            mediana = (double) soma / 2;
        } else {
            int index = (valores.size() / 2);
            mediana = valores.get(index);
        }
        
        return valores.size() > 0 ? (double) mediana : 0;
    }
    
    private ArrayList<Integer> ordenaValores() {
        int i, j, aux;
        ArrayList<Integer> valores = new ArrayList<>();
        for(Item item : this.inventario.getItens()) {
            valores.add(item.getQuantidade());
        }
        for (i = valores.size() - 1; i >= 1; i--) {
            for(j = 0; j < i; j++) {
                if(valores.get(j) > valores.get(j + 1)) {
                    aux = valores.get(j);
                    valores.set(j, valores.get(j + 1));
                    valores.set(j + 1, aux);
                }
            }
        }
        return valores;
    }
    
    
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int intensAcimaDaMedia = 0;
        for(Item item : this.inventario.getItens()) {
            if(item.getQuantidade() > media) {
                intensAcimaDaMedia++;
            }
        }
        return intensAcimaDaMedia;
    }
    
}
