import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    
    @Test
    public void dwarfDevePerderVida66porcento() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(4);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.sofrerDano();
        assertEquals(100.0, balin.getVida(), 1e-1);
    }
    
    
    @Test
    public void dwarfDevePerderVida33porcento() {
        DadoFalso dadoFalso = new DadoFalso();
        dadoFalso.simularValor(5);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.sofrerDano();
        assertEquals(110.0, balin.getVida(), 1e-1);
    }
    
    @Test
    public void anaoNaoMorreComUltimaVida() {
        boolean viveu = false;
        for(int i = 0; i < 1000; i++) {
            Dwarf anao = new DwarfBarbaLonga("Gimli");
            for(int j = 0; j < 1000; j++){
                anao.sofrerDano();
                if (j >= 11 && anao.getStatus() == Status.SOFREU_DANO) {
                    viveu = true;
                    break;
                }
            }
        } 
        assertTrue(viveu);
    }
    
    @Test
    public void anaoMorreComUltimaVida() {
        boolean morreu = false;
        for(int i = 0; i < 1000; i++) {
            Dwarf anao = new DwarfBarbaLonga("Gimli");
            for(int j = 0; j < 12; j++){
                anao.sofrerDano();
                if (j == 11  && anao.getStatus() == Status.MORTO) {
                    morreu = true;
                    break;
                }
            }
        }
        assertTrue(morreu);
    }
    
}
