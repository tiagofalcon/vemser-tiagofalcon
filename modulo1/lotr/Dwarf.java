public class Dwarf extends Personagem {
    private boolean itemEquipado;
    
    {
        this.itemEquipado = false;
        this.qtdDano = 10.0;
    }
    
    public Dwarf(String nome) {
        super(nome);
        this.vida =110.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
    }
    
    protected double calcularDano() {
        return this.itemEquipado ? 5.0 : this.qtdDano;
    }
    
    public boolean getItemEquipado() {
        return this.itemEquipado;
    }
    
    private void setVida(double vida) {
        
        this.vida += vida;
       
    }
    
    public void equiparEscudo() {
        this.itemEquipado = true;
    }
    
    public String imprimirNomeDaClasse() {
        return "Dwarf";
    }
}
