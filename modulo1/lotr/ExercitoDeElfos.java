import java.util.*;

public class ExercitoDeElfos {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    private ArrayList<Elfo> elfos;
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();

    public ExercitoDeElfos() {
        this.elfos = new ArrayList<>();
    }
    
    public ArrayList<Elfo> getElfos () {
        return this.elfos;
    }
    
    public void alistar(Elfo elfo) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            elfos.add(elfo);
            ArrayList<Elfo> elfosDeUmStatus = porStatus.get(elfo.getStatus());
            if(elfosDeUmStatus == null){
                elfosDeUmStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDeUmStatus);
            }
            elfosDeUmStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> buscar(Status status) {
        ArrayList<Elfo> auxiliar = porStatus.get(status);
        return auxiliar.size() > 0 ? auxiliar : null;
    }
    
}
