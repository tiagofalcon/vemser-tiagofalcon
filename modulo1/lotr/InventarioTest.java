import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class InventarioTest{
    @Test
    public void inventarioDeveIniciarVazio() {
        Inventario inventario = new Inventario();
        
        assertEquals(0, inventario.getItens().size());
        
    }
    
    @Test
    public void adicionaItemNaPrimeiraPosicaoVaziaEmOrdemERetornaItem() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        assertEquals(faca, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(escudo, inventario.obter(2));
        
    }
    
    @Test
    public void removeItemNaPosicaoIndicada() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        inventario.remover(2);
        
        assertEquals(faca, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(2, inventario.getItens().size());
    }
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        
        assertEquals(faca, inventario.getItens().get(0));
        assertEquals(espada, inventario.getItens().get(1));
    }
    
    @Test
    public void retornaDescricaoDosItensSeparadosPorVirgula() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        assertEquals("faca,espada,escudo", inventario.getDescricoesItens());
    }
    
    @Test
    public void retornaItenDeMaiorQuantidade() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item corda = new Item(5,"corda");
        Item capa = new Item(1,"capa");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(corda);
        inventario.adicionar(capa);
        
        Item itemMaiorQuantidade = inventario.retornaItemDeMaiorQuantidade();
        
        assertEquals(5, itemMaiorQuantidade.getQuantidade());
    }
    
    @Test
    public void retornaItenDeMaiorQuantidadeMesmaQuantidade() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item corda = new Item(5,"corda");
        Item lanca = new Item(5,"lanca");
        Item capa = new Item(1,"capa");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(corda);
        inventario.adicionar(lanca);
        inventario.adicionar(capa);
        
        Item itemMaiorQuantidade = inventario.retornaItemDeMaiorQuantidade();
        
        assertEquals(5, itemMaiorQuantidade.getQuantidade());
    }
    
    @Test
    public void retornaItemPorBuscarNome() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        assertEquals("espada", inventario.buscar("espada").getDescricao());
        assertEquals(espada, inventario.buscar("espada"));
    }
    
    @Test
    public void naoRemoveFlecha() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item flechas = new Item(1,"Flechas");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(flechas);
        
        assertEquals("Flechas", inventario.buscar("Flechas").getDescricao());
    }
    
    @Test
    public void retornaItensdeListaInvertidaSemAlterarOriginal() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
                
        assertEquals(escudo, inventario.inverter().get(0));
        assertEquals(espada, inventario.inverter().get(1));
        assertEquals(faca, inventario.inverter().get(2));
        assertEquals(3, inventario.inverter().size());
        
        assertEquals(faca, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
        assertEquals(escudo, inventario.obter(2));
        assertEquals(3, inventario.getItens().size());
    }
    
    @Test
    public void ordenaAListaDeItensPorQuantidade() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(5,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        inventario.ordenarItens();
        
        assertEquals("espada,faca,escudo", inventario.getDescricoesItens());
    }
    
    @Test
    public void ordenaAListaDeItensPorQuantidadeRecebendoAtributoEnum() {
        Inventario inventario = new Inventario();
        TipoOrdenacao ascendente = TipoOrdenacao.ASC;
        TipoOrdenacao descendente = TipoOrdenacao.DESC;
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(5,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        inventario.ordenarItens(ascendente);
        
        assertEquals("espada,faca,escudo", inventario.getDescricoesItens());
        
        inventario.ordenarItens(descendente);
        
        assertEquals("escudo,faca,espada", inventario.getDescricoesItens());
    }
    
    @Test
    public void ordenarComApenasUmItemRecebendoAtributoEnumASC() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        TipoOrdenacao ascendente = TipoOrdenacao.ASC;
        inventario.adicionar(espada);
        inventario.ordenarItens(ascendente);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordenarTotalmenteOrdenadoRecebendoAtributoEnumASC() {
        Inventario inventario = new Inventario();
        Item pizza = new Item(3, "Pizza");
        Item xis = new Item(2, "Xis");
        Item bauru = new Item(1, "Bauru");
        inventario.adicionar(bauru);
        inventario.adicionar(xis);
        inventario.adicionar(pizza);
        TipoOrdenacao ascendente = TipoOrdenacao.ASC;
        inventario.ordenarItens(ascendente);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(bauru, xis, pizza)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordenarQuantidadesIguaisRecebendoAtributoEnumASC() {
        Inventario inventario = new Inventario();
        Item pizza = new Item(3, "Pizza");
        Item xis = new Item(3, "Xis");
        Item bauru = new Item(3, "Bauru");
        inventario.adicionar(bauru);
        inventario.adicionar(xis);
        inventario.adicionar(pizza);
        TipoOrdenacao ascendente = TipoOrdenacao.ASC;
        inventario.ordenarItens(ascendente);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(bauru, xis, pizza)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordenarComApenasUmItemRecebendoAtributoEnumDESC() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        TipoOrdenacao descendente = TipoOrdenacao.DESC;
        inventario.adicionar(espada);
        inventario.ordenarItens(descendente);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordenarTotalmenteOrdenadoRecebendoAtributoEnumDESC() {
        Inventario inventario = new Inventario();
        Item pizza = new Item(3, "Pizza");
        Item xis = new Item(2, "Xis");
        Item bauru = new Item(1, "Bauru");
        inventario.adicionar(pizza);
        inventario.adicionar(xis);
        inventario.adicionar(bauru);
        TipoOrdenacao descendente = TipoOrdenacao.DESC;
        inventario.ordenarItens(descendente);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(pizza, xis, bauru)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordenarQuantidadesIguaisRecebendoAtributoEnumDESC() {
        Inventario inventario = new Inventario();
        Item pizza = new Item(3, "Pizza");
        Item xis = new Item(3, "Xis");
        Item bauru = new Item(3, "Bauru");
        inventario.adicionar(bauru);
        inventario.adicionar(xis);
        inventario.adicionar(pizza);
        TipoOrdenacao descendente = TipoOrdenacao.DESC;
        inventario.ordenarItens(descendente);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(bauru, xis, pizza)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void removerItemRecebendoItem() {
        Inventario inventario = new Inventario();
        
        
        Item faca = new Item(2,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(5,"escudo");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        inventario.remover(1);
        
        assertEquals(faca, inventario.obter(0));
        assertEquals(escudo, inventario.obter(1));
        assertEquals(2, inventario.getItens().size());
    }
}
