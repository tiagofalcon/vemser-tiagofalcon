import java.util.*;

public class Inventario extends Object {
    
    private ArrayList<Item> itens;
    
    public Inventario() {
        this.itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    /*adicionar(Item) - recebe uma instância de item e adiciona
     * no primeiro slot que tiver referência null*/
    public void adicionar(Item item) {
        this.itens.add(item);
    }
    
    /*obter(posicao) - retorna o item correspondente àquela posição no array*/
    public Item obter(int posicao) {
        return this.itens.get(posicao);
    }
    
    public Item buscar(String nomeItem) {
        for(Item item : this.itens) {
            if(nomeItem.equals(item.getDescricao())){
                return item;
            }
        }
        return null;
    }
    
    /*remover(posicao) - remove o item correspondente
     * àquela posição no array (setando para null sua referência)*/
    public void remover(int posicao) {
        this.itens.remove(posicao);
    }
    
    public void remover(Item item) {
        this.itens.remove(item);
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        for (int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            if(item != null){
                String descricao = obter(i).getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = i < this.itens.size() - 1;
                if( deveColocarVirgula ) {
                    descricoes.append(",");
                }
            }
        }
        
        return descricoes.toString();
    }
    
    public Item retornaItemDeMaiorQuantidade() {
        int indice = 0, maiorQuantidadeParcial = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            int qtdAtual = this.obter(i).getQuantidade();
            if (qtdAtual > maiorQuantidadeParcial) {
                maiorQuantidadeParcial = qtdAtual;
                indice = i;
            }
        }
        //Logica de comparacao ? se verdadeiro : se falso;
        return this.itens.size() > 0 ? this.obter(indice) : null;
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> auxiliar = new ArrayList<Item>(this.itens.size());
        int i = this.itens.size() - 1;
        while(i >= 0){
            auxiliar.add(this.itens.get(i));
            i--;
        }
        
        return auxiliar.size() > 0 ? auxiliar : null;
    }
    
    public void ordenarItens() {
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao tipoOrdenacao) {
        int i, j;
        Item aux;        
        for (i = this.itens.size() - 1; i >= 1; i--) {
            for(j = 0; j < i; j++) {
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                boolean deveTrocar = tipoOrdenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
                if(deveTrocar) {
                    aux = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, aux);
                }
            }
        }
    }
    
    private void ordenarItensDescendente() {
        ordenarItens(TipoOrdenacao.DESC);
    }
}
