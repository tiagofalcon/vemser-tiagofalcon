import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    @Test
    public void anaoDeveNascerCom110VidaEComEscudoNaoEquipado() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        
        Item esperado = new Item(1, "Escudo");
        Item resultado = anaoQualquer.getInventario().obter(0);
        
        assertEquals(110.0, anaoQualquer.getVida(), 0.00001);
        assertEquals(Status.RECEM_CRIADO, anaoQualquer.getStatus());
        assertEquals(esperado, resultado);
        assertEquals(1, anaoQualquer.getInventario().getItens().size());
        assertTrue(!anaoQualquer.getItemEquipado());
    }
    
    @Test
    public void anaoPerdeVidaComFlechadaSemEquiparEscudo() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        anaoQualquer.sofrerDano();
        assertEquals(100.0, anaoQualquer.getVida(), 0.00001);
        
    }
    
    @Test
    public void anaoNaoTemVidaMenorQueZeroEZeraVida() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        
        for(int i = 0; i < 12; i++) {
            anaoQualquer.sofrerDano();
        }
        
        assertEquals(0.0, anaoQualquer.getVida(), 0.00001);
        
    }
    
    @Test
    public void dwarfPerdeDezDeVidaDuasVezesSemEquiparEscudo() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        anaoQualquer.sofrerDano();
        anaoQualquer.sofrerDano();
        assertEquals(90.0, anaoQualquer.getVida(), .01);
        assertEquals(Status.SOFREU_DANO, anaoQualquer.getStatus());
    }
    
    @Test
    public void dwarfZeraVidaSemEquiparEscudo() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        
        for(int i = 0; i < 12;i++) {
            anaoQualquer.sofrerDano();
        }
        
        assertEquals(0.0, anaoQualquer.getVida(), .01);
        assertEquals(Status.MORTO, anaoQualquer.getStatus());
    }
    
    @Test
    public void anaoEquipaEscudo() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        anaoQualquer.equiparEscudo();
        assertTrue(anaoQualquer.getItemEquipado());
    }
    
    @Test
    public void dwarfTomaMetadeDoDanoAoEquiparEscudo() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        anaoQualquer.equiparEscudo();
        anaoQualquer.sofrerDano();
        anaoQualquer.sofrerDano();
        assertEquals(100.0, anaoQualquer.getVida(), .01);
        assertEquals(Status.SOFREU_DANO, anaoQualquer.getStatus());
        assertTrue(anaoQualquer.getItemEquipado());
    }
    
    @Test
    public void dwarfGanhaUmItem() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        Item faca = new Item(1, "Faca");
        anaoQualquer.ganharItem(faca);
        assertEquals(faca, anaoQualquer.getInventario().obter(1));
        assertEquals(2, anaoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void dwarfPerdeUmItem() {
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        Item faca = new Item(1, "Faca");
        anaoQualquer.ganharItem(faca);
        Item corda = new Item(1, "Corda");
        anaoQualquer.ganharItem(corda);
        anaoQualquer.perderItem(faca);
        assertEquals(corda, anaoQualquer.getInventario().obter(1));
        assertEquals(2, anaoQualquer.getInventario().getItens().size());
    }
}
