import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    @Test
    public void atirarFlechaDevePerderFlechaE15XPAumentarTriploXPESofreDano() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("NaoLegolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        
        elfoNoturno.atirarFlecha(anao);
        assertEquals(1, elfoNoturno.getQtdFlechas());
        assertEquals(3, elfoNoturno.getExperiencia());
        assertEquals(85.0, elfoNoturno.getVida(), 0.00001);
        assertTrue(Status.SOFREU_DANO == elfoNoturno.getStatus());
    }
    
    @Test
    public void elfoNoturnoNaoAtiraMaisQueDuasFlechasSemAdicionar() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("NaoLegolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        
        for(int i = 0; i < 12; i++) {
            elfoNoturno.atirarFlecha(anao);
        }
        
        assertEquals(70, elfoNoturno.getVida(), 0.00001);
        assertTrue(Status.SOFREU_DANO == elfoNoturno.getStatus());
    }
    
    @Test
    public void ganhaVidaENaoAceitaGanharValorNegativo() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("NaoLegolas");
        elfoNoturno.ganharVida(5);
        elfoNoturno.ganharVida(-5);
        
        
        assertEquals(105, elfoNoturno.getVida(), 0.00001);
    }
    
    @Test
    public void morreComVidaIgualAZero() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("NaoLegolas");
        elfoNoturno.ganharVida(5);
        elfoNoturno.getInventario().obter(1).setQuantidade(30);
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        for(int i = 0; i < 12; i++) {
            elfoNoturno.atirarFlecha(anao);
        }
        
        assertEquals(0, elfoNoturno.getVida(), 0.00001);
        assertTrue(Status.MORTO == elfoNoturno.getStatus());
    }
}



