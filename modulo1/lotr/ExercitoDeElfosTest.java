import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest {
    @Test
    public void exercitoNasceVazio() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        assertEquals(0,exercito.getElfos().size());
    }
    
    @Test
    public void adicionaElfoVerde() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo legolas = new ElfoVerde("Legolas");
        exercito.alistar(legolas);
        assertEquals(1,exercito.getElfos().size());
        assertTrue(exercito.getElfos().get(0) instanceof ElfoVerde);
        assertTrue(exercito.getElfos().contains(legolas));
    }
    
    @Test
    public void adicionaElfoNoturno() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo legolasDaNoite = new ElfoNoturno("Parente do Legolas");
        exercito.alistar(legolasDaNoite);
        assertEquals(1,exercito.getElfos().size());
        assertTrue(exercito.getElfos().get(0) instanceof ElfoNoturno);
        assertTrue(exercito.getElfos().contains(legolasDaNoite));
    }
    
    @Test
    public void aceitaApenasElfosVerdesENoturnos() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo legolasDaNoite = new ElfoNoturno("Parente do Legolas");
        Elfo legolas = new ElfoVerde("Legolas");
        Elfo comum = new Elfo("Mais Legolas");
        Elfo iluminado = new ElfoDaLuz("Outro Legolas");
        exercito.alistar(legolasDaNoite);
        exercito.alistar(legolas);
        exercito.alistar(comum);
        exercito.alistar(iluminado);
        
        assertEquals(2,exercito.getElfos().size());
        assertTrue(exercito.getElfos().get(0) instanceof ElfoNoturno);
        assertTrue(exercito.getElfos().get(1) instanceof ElfoVerde);
        assertTrue(exercito.getElfos().contains(legolasDaNoite));
        assertTrue(exercito.getElfos().contains(legolas));
        assertFalse(exercito.getElfos().contains(comum));
        assertFalse(exercito.getElfos().contains(iluminado));
    }
    
    @Test
    public void reotornaElfosRecemCriados() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(new ElfoNoturno("Parente do Legolas"));
        exercito.alistar(new ElfoVerde("Legolas"));
        ArrayList<Elfo> lista = new ArrayList<>();
        lista = exercito.buscar(Status.RECEM_CRIADO);
        assertEquals(2,lista.size());
        assertTrue(lista.get(0) instanceof ElfoNoturno);
        assertTrue(lista.get(1) instanceof ElfoVerde);
    }
    
    @Test
    public void reotornaElfosSofreDano() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        ElfoNoturno noturno = new ElfoNoturno("Parente do Legolas");
        ElfoVerde verde = new ElfoVerde("Legolas");
        Dwarf dwarf = new Dwarf("Gimli");
        noturno.atirarFlecha(dwarf);
        exercito.alistar(noturno);
        exercito.alistar(verde);
        ArrayList<Elfo> lista = new ArrayList<>();
        lista = exercito.buscar(Status.SOFREU_DANO);
        assertEquals(1,lista.size());
        assertTrue(lista.get(0) instanceof ElfoNoturno);
    }
}
