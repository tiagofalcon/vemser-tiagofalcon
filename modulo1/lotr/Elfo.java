public class Elfo extends Personagem {
    /*
     * nome
     * arco
     * flechas
     * experiencia
    */
    private int indiceFlecha;
    private static int contadorDeElfos;
    
    {
        this.indiceFlecha = 1;
    }
    
    public Elfo(String nome) {
        super(nome);
        Elfo.contadorDeElfos++;
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flechas"));
    }
    
    public void finalize() throws Throwable {
        Elfo.contadorDeElfos--;
    }
    
    public static int getContadorDeElfos() {
        return Elfo.contadorDeElfos;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    public void adicionarFlechas(int qtd) {
        if(qtd > 0) {
            Item item = this.inventario.buscar("Flechas");
            item.setQuantidade(item.getQuantidade() + qtd);
        }
    }
    
    public int getQtdFlechas() {
        return this.inventario.obter(indiceFlecha).getQuantidade();
    }
    
    protected boolean podeAtirar(){
        return this.getQtdFlechas() > 0;
    }
    
    public void atirarFlecha(Dwarf anao) {
        int qtdAtual = this.getQtdFlechas();
        if(this.podeAtirar()){
            this.aumentarXP();
            this.sofrerDano();
            anao.sofrerDano();
            this.inventario.obter(1).setQuantidade(qtdAtual - 1);
        }
        //this.experiencia = experiencia + 1;
        //this.experiencia++;
    }
    
    public String imprimirNomeDaClasse() {
        return "Elfo";
    }
    
}
