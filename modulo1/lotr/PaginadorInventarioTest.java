import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest {
    
    
    @Test
    public void pularLimitarComApenasUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
	inventario.adicionar(espada);
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(0);
	ArrayList<Item> primeiraPagina = paginador.limitar(1); // retorna os itens “Espada” e “Escudo de metal”
	
	assertEquals(espada, primeiraPagina.get(0));
	assertEquals(1, primeiraPagina.size());
    }
    

    @Test
    public void testarPaginadorItensAMostrarDentroDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo de metal");
        Item pocao = new Item(3, "Poção de HP");
        Item bracelete = new Item(4, "Bracelete");
	inventario.adicionar(espada);
	inventario.adicionar(escudo);
	inventario.adicionar(pocao);
	inventario.adicionar(bracelete);
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(0);
	ArrayList<Item> limitado1 = paginador.limitar(2); // retorna os itens “Espada” e “Escudo de metal”
	paginador.pular(2);
	ArrayList<Item> limitado2 = paginador.limitar(2); // retorna os itens “Poção de HP” e “Bracelete”
	
	assertEquals(espada, limitado1.get(0));
	assertEquals(escudo, limitado1.get(1));
	assertEquals(2, limitado1.size());
	assertEquals(pocao, limitado2.get(0));
	assertEquals(bracelete, limitado2.get(1));
	assertEquals(2, limitado2.size());
    }
    
    @Test
    public void testarPaginadorItensAMostrarForaDosLimites() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo de metal");
        Item pocao = new Item(3, "Poção de HP");
        Item bracelete = new Item(4, "Bracelete");
	inventario.adicionar(espada);
	inventario.adicionar(escudo);
	inventario.adicionar(pocao);
	inventario.adicionar(bracelete);
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(0);
	ArrayList<Item> limitado1 = paginador.limitar(2); // retorna os itens “Espada” e “Escudo de metal”
	paginador.pular(2);
	ArrayList<Item> limitado2 = paginador.limitar(10); // retorna os itens “Poção de HP” e “Bracelete”
	
	assertEquals(espada, limitado1.get(0));
	assertEquals(escudo, limitado1.get(1));
	assertEquals(2, limitado1.size());
	assertEquals(pocao, limitado2.get(0));
	assertEquals(bracelete, limitado2.get(1));
	assertEquals(2, limitado2.size());

    }
    
}
