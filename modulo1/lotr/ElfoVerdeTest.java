

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest {
    @Test
    public void atirarFlechaDevePerderFlechaAumentarDobroXP() {
        ElfoVerde elfoVerde = new ElfoVerde("Legolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        
        elfoVerde.atirarFlecha(anao);
        assertEquals(1, elfoVerde.getQtdFlechas());
        assertEquals(2, elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeGanhaUmItemComDescricaoPermitida() {
        ElfoVerde elfoQualquer = new ElfoVerde("Legolas");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        elfoQualquer.ganharItem(arcoVidro);
        assertEquals(new Item(1, "Arco"), elfoQualquer.getInventario().obter(0));
        assertEquals(new Item(2, "Flechas"), elfoQualquer.getInventario().obter(1));
        assertEquals(arcoVidro, elfoQualquer.getInventario().obter(2));
        assertEquals(3, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void elfoVerdePerdeUmItemComDescricaoPermitida() {
        ElfoVerde elfoQualquer = new ElfoVerde("Legolas");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        elfoQualquer.ganharItem(arcoVidro);
        elfoQualquer.perderItem(arcoVidro);
        assertNull(elfoQualquer.getInventario().buscar("Arco de Vidro"));
        assertEquals(new Item(1, "Arco"), elfoQualquer.getInventario().obter(0));
        assertEquals(new Item(2, "Flechas"), elfoQualquer.getInventario().obter(1));
        assertEquals(2, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void elfoVerdeNaoGanhaUmItemComDescricaoNaoPermitido() {
        ElfoVerde elfoQualquer = new ElfoVerde("Legolas");
        Item faca = new Item(1, "Faca");
        elfoQualquer.ganharItem(faca);
        assertEquals(new Item(1, "Arco"), elfoQualquer.getInventario().obter(0));
        assertEquals(new Item(2, "Flechas"), elfoQualquer.getInventario().obter(1));
        assertEquals(2, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void elfoVerdeNaoPerdeUmItemComDescricaoNaoPermitido() {
        ElfoVerde elfoQualquer = new ElfoVerde("Legolas");
        Item flecha = new Item(2, "Flechas");
        elfoQualquer.perderItem(flecha);
        assertEquals(new Item(1, "Arco"), elfoQualquer.getInventario().obter(0));
        assertEquals(new Item(2, "Flechas"), elfoQualquer.getInventario().obter(1));
        assertEquals(2, elfoQualquer.getInventario().getItens().size());
    }
    
}
