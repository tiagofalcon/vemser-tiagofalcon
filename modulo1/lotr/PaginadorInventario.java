import java.util.*;

public class PaginadorInventario {
    
    private Inventario inventario;
    private int marcadorInicial;
    
    public PaginadorInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public void pular(int marcador) {
        this.marcadorInicial = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int limitador) {
        int marcadorFinal = this.marcadorInicial + limitador;
        ArrayList<Item> retorno = new ArrayList<>();
        for (int i = marcadorInicial; i < marcadorFinal && i < this.inventario.getItens().size(); i++) {
            retorno.add(this.inventario.getItens().get(i));
        }
        return retorno;
    }
}
