import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @After 
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas1Arco2ItensNoInventario() {
        Elfo elfoQualquer = new Elfo("Legolas");
        assertEquals(2, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getInventario().obter(0).getQuantidade());
        assertEquals(2, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXP() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        
        elfoQualquer.atirarFlecha(anao);
        assertEquals(1, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getExperiencia());
    }
    
    @Test
    public void atirarTresFlechasTendoDuasFlechaDevePerderFlechaAumentarXPAnaoPerdeXP() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        elfoQualquer.atirarFlecha(anaoQualquer);
        elfoQualquer.atirarFlecha(anaoQualquer);
        elfoQualquer.atirarFlecha(anaoQualquer);
        assertEquals(0, elfoQualquer.getQtdFlechas());
        assertEquals(2, elfoQualquer.getExperiencia());
        assertEquals(90.0, anaoQualquer.getVida(), 0.00001);
    }
    
    @Test
    public void atirarTresFlechasTendoDuasENaoMaisQueTresFlechaDevePerderFlechaAumentarXP() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anaoQualquer = new Dwarf("Balin, son of Gloin");
        Dwarf anao2 = new Dwarf("Gloin");
        Dwarf anao3 = new Dwarf("Torin");
        elfoQualquer.atirarFlecha(anaoQualquer);
        elfoQualquer.atirarFlecha(anao2);
        elfoQualquer.atirarFlecha(anao3);
        assertEquals(0, elfoQualquer.getQtdFlechas());
        assertEquals(2, elfoQualquer.getExperiencia());
        assertEquals(100.0, anaoQualquer.getVida(), 0.00001);
        assertEquals(100.0, anao2.getVida(), 0.00001);
        assertEquals(110.0, anao3.getVida(), 0.00001);
    }
    
    @Test
    public void elfoGanhaUmItem() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Item faca = new Item(1, "Faca");
        elfoQualquer.ganharItem(faca);
        assertEquals(faca, elfoQualquer.getInventario().obter(2));
        assertEquals(3, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void elfoPerdeUmItem() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Item faca = new Item(1, "Faca");
        elfoQualquer.ganharItem(faca);
        Item corda = new Item(1, "Corda");
        elfoQualquer.ganharItem(corda);
        elfoQualquer.perderItem(faca);
        assertEquals(corda, elfoQualquer.getInventario().obter(2));
        assertEquals(3, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void criarUmElfoIncerementaContadorUmaVez() {
        new Elfo("Legolas");
        assertEquals(1, Elfo.getContadorDeElfos());
    }
    
    @Test
    public void aumentaQtdFlechas() {
        Elfo elfo = new Elfo("Legolas");
        elfo.adicionarFlechas(10);
        assertEquals(12, elfo.getQtdFlechas());
    }
}
