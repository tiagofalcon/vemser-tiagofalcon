import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {
    
    @Test
    public void calculaAMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        boolean ehNaN = Double.isNaN(estatisticas.calcularMedia());
        
        assertTrue(ehNaN); 
    }
    
    @Test
    public void calculaAMediaDeUmItem() {
        Inventario inventario = new Inventario();
        
        
        Item faca = new Item(3,"faca");
        
        inventario.adicionar(faca);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(3, estatisticas.calcularMedia(), 0.1); 
    }
    
    @Test
    public void calculaAMediaDeQuantidadesIguais() {
        Inventario inventario = new Inventario();
        
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(3,"espada");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(3, estatisticas.calcularMedia(), 0.1); 
    }
    
    @Test
    public void calculaAMediaDaQuantidadeDeItens() {
        Inventario inventario = new Inventario();
        
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item corda = new Item(5,"corda");
        Item lanca = new Item(5,"lanca");
        Item capa = new Item(1,"capa");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(corda);
        inventario.adicionar(lanca);
        inventario.adicionar(capa);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(2.6, estatisticas.calcularMedia(), 0.1); 
    }
    
    @Test
    public void calculaAMedianaAPenasUmItem() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(3,"faca");
        
        inventario.adicionar(faca);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(3, estatisticas.calcularMediana(), 0.1); 
    }
    
    
    @Test
    public void calculaAMedianaDaQuantidadeParDeItens() {
        Inventario inventario = new Inventario();
        
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(11,"escudo");
        Item corda = new Item(5,"corda");
        Item lanca = new Item(5,"lanca");
        Item capa = new Item(1,"capa");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(corda);
        inventario.adicionar(lanca);
        inventario.adicionar(capa);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(4, estatisticas.calcularMediana(), 0.1); 
    }
    
    @Test
    public void calculaAMedianaDaQuantidadeImparDeItens() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(1,"espada");
        Item corda = new Item(5,"corda");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(corda);
        
        assertEquals(3, estatisticas.calcularMediana(), 0.1); 
    }
    
    
    @Test
    public void retornaQuantidadeDeItensAcimaDaMediaComApenasUmItem() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(3,"faca");
        
        inventario.adicionar(faca);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }
    
    
    @Test
    public void retornaQuantidadeDeItensAcimaDaMediaComVariosItens() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(3,"faca");
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item corda = new Item(5,"corda");
        Item lanca = new Item(5,"lanca");
        Item capa = new Item(1,"capa");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(corda);
        inventario.adicionar(lanca);
        inventario.adicionar(capa);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(2.6, estatisticas.calcularMedia(), 0.1);
        assertEquals(3, estatisticas.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void retornaQuantidadeDeItensAcimaDaMediaComItensIgualMedia() {
        Inventario inventario = new Inventario();
        
        Item faca = new Item(1,"faca");
        Item espada = new Item(2,"espada");
        Item escudo = new Item(3,"escudo");
        Item corda = new Item(4,"corda");
        Item lanca = new Item(5,"lanca");
        
        inventario.adicionar(faca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(corda);
        inventario.adicionar(lanca);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
}


