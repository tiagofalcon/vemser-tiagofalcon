import java.util.*;
public abstract class Estrategias implements EstrategiaDeAtaque {
    protected ArrayList<Elfo> elfosVivos(ArrayList<Elfo> elfos) {
        ArrayList<Elfo> auxiliar = new ArrayList<>();
        for(Elfo elfo : elfos) {
            boolean adicionarElfo = elfo.getStatus() != Status.MORTO;
            if(adicionarElfo){
                auxiliar.add(elfo);
            }
        }
        return auxiliar;
    }
    
    protected ArrayList<Elfo> elfosPorTipo(String nomeDaClasse, ArrayList<Elfo> elfos) {
        ArrayList<Elfo> elfosDeUmaClasse = new ArrayList<>();
        for(Elfo elfo : elfos) {
            if(elfo.getClass().getName().equals(nomeDaClasse)) {
                elfosDeUmaClasse.add(elfo);
            }
        }
        return elfosDeUmaClasse;
    }
    
    protected ArrayList<Elfo> removeDiferenca(int diferenca, ArrayList<Elfo> elfos) {
        ArrayList<Elfo> retorno = new ArrayList<Elfo>();
        if(diferenca > 0) {
            retorno = elfosPorTipo("ElfoVerde",elfos);
            for(int i = 0; i < diferenca; i++) {
                retorno.remove(i);
            }
            retorno.addAll(elfosPorTipo("ElfoNoturno",elfos));
        }
        if(diferenca < 0) {
            retorno = elfosPorTipo("ElfoNoturno",elfos);
            diferenca = Math.abs(diferenca);
            for(int i = diferenca - 1; i >= 0; i--) {
                retorno.remove(i);
            }
            retorno.addAll(elfosPorTipo("ElfoVerde",elfos));
        }
        if(diferenca == 0){
            retorno.addAll(elfosPorTipo("ElfoVerde",elfos));
            retorno.addAll(elfosPorTipo("ElfoNoturno",elfos));
        }
        return retorno;
    }
}
