

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DadoD6Test {
    @Test
    public void geraNumerosEntre1e6(){
        boolean resposta = false;
        DadoD6 dado = new DadoD6();
        
        boolean[] respostas = new boolean[1000];
        
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            resposta = resultado >= 1 && resultado <= 6; 
            respostas[i] = resposta;
        }
        
        for(int i = 0; i < 1000; i++) {
            if(respostas[i] == false){
                resposta = false;
            } 
        }
        assertTrue(resposta);
    }
    
    @Test
    public void dadoDeraNumero1(){
        DadoD6 dado = new DadoD6();
        boolean resposta = false;
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            if(resultado == 1){
                resposta = true;
                break;
            }
        }
        assertTrue(resposta);
    }
    
    @Test
    public void dadoDeraNumero2(){
        DadoD6 dado = new DadoD6();
        boolean resposta = false;
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            if(resultado == 2){
                resposta = true;
                break;
            }
        }
        assertTrue(resposta);
    }
    
    @Test
    public void dadoDeraNumero3(){
        DadoD6 dado = new DadoD6();
        boolean resposta = false;
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            if(resultado == 3){
                resposta = true;
                break;
            }
        }
        assertTrue(resposta);
    }
    
    @Test
    public void dadoDeraNumero4(){
        DadoD6 dado = new DadoD6();
        boolean resposta = false;
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            if(resultado == 4){
                resposta = true;
                break;
            }
        }
        assertTrue(resposta);
    }
    
    @Test
    public void dadoDeraNumero5(){
        DadoD6 dado = new DadoD6();
        boolean resposta = false;
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            if(resultado == 5){
                resposta = true;
                break;
            }
        }
        assertTrue(resposta);
    }
    
    @Test
    public void dadoDeraNumero6(){
        DadoD6 dado = new DadoD6();
        boolean resposta = false;
        for(int i = 0; i < 1000; i++) {
            int resultado = dado.sortear();
            if(resultado == 6){
                resposta = true;
                break;
            }
        }
        assertTrue(resposta);
    }
}
