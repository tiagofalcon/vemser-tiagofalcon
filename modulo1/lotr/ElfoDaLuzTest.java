
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoDaLuzTest {
    @Test
    public void nasceComEspada () {
        ElfoDaLuz elfo = new ElfoDaLuz("maisLegolas");
        Item item = new Item(1, "Espada de Galvorn");
        assertEquals(2, elfo.getQtdFlechas());
        assertEquals(1, elfo.getInventario().obter(0).getQuantidade());
        assertEquals(item, elfo.getInventario().obter(2));
        assertEquals(3, elfo.getInventario().getItens().size());
    }
    
    @Test 
    public void perde21XPAtaqueImpar() {
        ElfoDaLuz elfo = new ElfoDaLuz("maisLegolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        elfo.atacarComEspada(anao);
        assertEquals(79, elfo.getVida(), 0.001);
    }
    
    @Test 
    public void ganha10XPAtaquePar() {
        ElfoDaLuz elfo = new ElfoDaLuz("maisLegolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        elfo.atacarComEspada(anao);
        assertEquals(79, elfo.getVida(), 0.001);
        elfo.atacarComEspada(anao);
        assertEquals(89, elfo.getVida(), 0.001);
    }
    
    @Test 
    public void naoRemoveEspadaDeGalvorn() {
        ElfoDaLuz elfo = new ElfoDaLuz("maisLegolas");
        Dwarf anao = new Dwarf("Balin, son of Gloin");
        elfo.perderItem(new Item(1, "Espada de Galvorn"));
        assertEquals(new Item(1, "Espada de Galvorn"), elfo.getInventario().obter(2));
        
    }
    
    
}
