import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstrategiaResistenteTest {
    
    @Test
    public void retornaListaElfosComFlechaE30PorcentoNoturno() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Dwarf dwarf = new Dwarf("Gimli");
        Elfo elfoverde1 = new ElfoVerde("Legolas1");
        elfoverde1.atirarFlecha(dwarf);
        elfoverde1.atirarFlecha(dwarf);
        Elfo elfoverde2 = new ElfoVerde("Legolas2");
        Elfo elfoverde3 = new ElfoVerde("Legolas2");
        Elfo elfoverde4 = new ElfoVerde("Legolas2");
        Elfo elfoverde5 = new ElfoVerde("Legolas2");
        Elfo elfonoturno1 = new ElfoNoturno("LegolasNoturno1");
        Elfo elfonoturno2 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno3 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno4 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno5 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno6 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno7 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno8 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno9 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno10 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno11 = new ElfoNoturno("LegolasNoturno2");
        exercito.alistar(elfoverde1);
        exercito.alistar(elfoverde2);
        exercito.alistar(elfoverde3);
        exercito.alistar(elfoverde4);
        exercito.alistar(elfoverde5);
        exercito.alistar(elfonoturno1);
        exercito.alistar(elfonoturno2);
        exercito.alistar(elfonoturno3);
        exercito.alistar(elfonoturno4);
        exercito.alistar(elfonoturno5);
        exercito.alistar(elfonoturno6);
        exercito.alistar(elfonoturno7);
        exercito.alistar(elfonoturno8);
        exercito.alistar(elfonoturno9);
        exercito.alistar(elfonoturno10);
        exercito.alistar(elfonoturno11);
        EstrategiaResistente estrategia = new EstrategiaResistente();
        
        assertTrue(elfoverde2.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(1)));
        assertTrue(elfoverde3.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(2)));
        assertTrue(elfoverde4.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(3)));
        assertTrue(elfoverde5.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(4)));
        assertTrue(elfonoturno11.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(0)));
        assertEquals(5,estrategia.getOrdemDeAtaque(exercito.getElfos()).size());
    }
    
    @Test
    public void retornaListaElfosComFlechaE30PorcentoNoturnoQtdFlechas() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Dwarf dwarf = new Dwarf("Gimli");
        Elfo elfoverde1 = new ElfoVerde("Legolas1");
        elfoverde1.atirarFlecha(dwarf);
        elfoverde1.atirarFlecha(dwarf);
        Elfo elfoverde2 = new ElfoVerde("Legolas2");
        Elfo elfoverde3 = new ElfoVerde("Legolas2");
        Elfo elfoverde4 = new ElfoVerde("Legolas2");
        Elfo elfoverde5 = new ElfoVerde("Legolas2");
        Elfo elfonoturno1 = new ElfoNoturno("LegolasNoturno1");
        Elfo elfonoturno2 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno3 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno4 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno5 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno6 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno7 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno8 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno9 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno10 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfonoturno11 = new ElfoNoturno("LegolasNoturno2");
        elfoverde2.adicionarFlechas(10);
        elfoverde3.adicionarFlechas(8);
        elfoverde4.adicionarFlechas(5);
        elfoverde5.adicionarFlechas(3);
        elfonoturno11.adicionarFlechas(1);
        
        exercito.alistar(elfoverde1);
        exercito.alistar(elfoverde2);
        exercito.alistar(elfoverde3);
        exercito.alistar(elfoverde4);
        exercito.alistar(elfoverde5);
        exercito.alistar(elfonoturno1);
        exercito.alistar(elfonoturno2);
        exercito.alistar(elfonoturno3);
        exercito.alistar(elfonoturno4);
        exercito.alistar(elfonoturno5);
        exercito.alistar(elfonoturno6);
        exercito.alistar(elfonoturno7);
        exercito.alistar(elfonoturno8);
        exercito.alistar(elfonoturno9);
        exercito.alistar(elfonoturno10);
        exercito.alistar(elfonoturno11);
        EstrategiaResistente estrategia = new EstrategiaResistente();
        
        assertTrue(elfoverde2.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(0)));
        assertTrue(elfoverde3.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(1)));
        assertTrue(elfoverde4.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(2)));
        assertTrue(elfoverde5.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(3)));
        assertTrue(elfonoturno11.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(4)));
        assertEquals(5,estrategia.getOrdemDeAtaque(exercito.getElfos()).size());
    }
}
