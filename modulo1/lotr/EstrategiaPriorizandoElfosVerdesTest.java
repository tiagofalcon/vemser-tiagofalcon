import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstrategiaPriorizandoElfosVerdesTest {
    
    @Test
    public void retornaListaOrdenadaDeAtaqueSomenteElfosVerdes() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfoverde1 = new ElfoVerde("Legolas1");
        Elfo elfoverde2 = new ElfoVerde("Legolas2");
        exercito.alistar(elfoverde1);
        exercito.alistar(elfoverde2);
        
        EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        assertTrue(elfoverde1.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(0)));
        assertTrue(elfoverde2.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(1)));
        assertEquals(2,estrategia.getOrdemDeAtaque(exercito.getElfos()).size());
    }
    
    @Test
    public void retornaListaOrdenadaDeAtaqueSomenteElfosNoturnos() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfonoturno1 = new ElfoNoturno("LegolasNoturno1");
        Elfo elfonoturno2 = new ElfoNoturno("LegolasNoturno2");
        exercito.alistar(elfonoturno1);
        exercito.alistar(elfonoturno2);
        
        EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        assertTrue(elfonoturno1.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(0)));
        assertTrue(elfonoturno2.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(1)));
        assertEquals(2,estrategia.getOrdemDeAtaque(exercito.getElfos()).size());
    }
    
    @Test
    public void retornaListaOrdenadaDeAtaque() {
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        Elfo elfoverde1 = new ElfoVerde("Legolas1");
        Elfo elfonoturno1 = new ElfoNoturno("LegolasNoturno1");
        Elfo elfoverde2 = new ElfoVerde("Legolas2");
        Elfo elfonoturno2 = new ElfoNoturno("LegolasNoturno2");
        exercito.alistar(elfoverde1);
        exercito.alistar(elfonoturno1);
        exercito.alistar(elfoverde2);
        exercito.alistar(elfonoturno2);
        
        EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        assertTrue(elfoverde1.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(0)));
        assertTrue(elfoverde2.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(1)));
        assertTrue(elfonoturno1.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(2)));
        assertTrue(elfonoturno2.equals(estrategia.getOrdemDeAtaque(exercito.getElfos()).get(3)));
        assertEquals(4,estrategia.getOrdemDeAtaque(exercito.getElfos()).size());
    }
    
}
