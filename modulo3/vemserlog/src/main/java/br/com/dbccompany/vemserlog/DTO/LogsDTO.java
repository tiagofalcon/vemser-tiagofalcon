package br.com.dbccompany.vemserlog.DTO;

import br.com.dbccompany.vemserlog.Entity.LogsEntity;

public class LogsDTO {

    private String id;
    private String codigo;
    private String mensagem;

    public LogsDTO() {}

    public LogsDTO(LogsEntity logsEntity) {
        this.id = logsEntity.getId();
        this.codigo = logsEntity.getCodigo();
        this.mensagem = logsEntity.getMensagem();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    @Override
    public String toString() {
        return String.format(
                "Log[id=%s, cogigo='%s', mensagem='%s']",
                id, codigo, mensagem);
    }

    public LogsEntity convert() {
        LogsEntity logsEntity = new LogsEntity();
        logsEntity.setId(this.id);
        logsEntity.setCodigo(this.codigo);
        logsEntity.setMensagem(this.mensagem);
        return logsEntity;
    }
}
