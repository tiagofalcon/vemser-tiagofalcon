package br.com.dbccompany.vemserlog.Service;

import br.com.dbccompany.vemserlog.DTO.LogsDTO;
import br.com.dbccompany.vemserlog.Entity.LogsEntity;
import br.com.dbccompany.vemserlog.Repository.LogsRepository;
import br.com.dbccompany.vemserlog.VemserlogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogsService {

    @Autowired
    LogsRepository logsRepository;

    private Logger logger = LoggerFactory.getLogger(VemserlogApplication.class);

    @Transactional
    public LogsDTO salvarLog(LogsDTO logsDTO) {
        try {
            LogsEntity logFinal = logsDTO.convert();
            LogsDTO newDTO = new LogsDTO(logsRepository.save(logFinal));
            return newDTO;
        } catch (Exception e) {
            logger.error("Erro ao salvar log: " + e.getMessage());
            throw new RuntimeException();
        }
    }

    @Transactional
    public List<LogsDTO> todos() {
        try {
            List<LogsDTO> logsDTOS = new ArrayList<>();
            for (LogsEntity logEntity: logsRepository.findAll()) {
                logsDTOS.add(new LogsDTO(logEntity));
            }
            return logsDTOS;
        } catch (Exception e) {
            logger.error("Erro ao listar logs: " + e.getMessage());
            throw new RuntimeException();
        }

    }

    public LogsDTO porId(String id) {
        try {
            return new LogsDTO(logsRepository.findById(id).get());
        } catch (Exception e) {
            logger.error("Não foi possível encontrar o log de id: " + id +". Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
