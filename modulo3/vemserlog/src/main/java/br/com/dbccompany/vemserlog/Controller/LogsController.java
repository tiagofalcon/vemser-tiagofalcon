package br.com.dbccompany.vemserlog.Controller;

import br.com.dbccompany.vemserlog.DTO.LogsDTO;
import br.com.dbccompany.vemserlog.Service.LogsService;
import br.com.dbccompany.vemserlog.VemserlogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/apiLogs")
public class LogsController {

    @Autowired
    LogsService logsService;

    private Logger logger = LoggerFactory.getLogger(VemserlogApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<LogsDTO> todosLogs() {
        logger.info("Buscando os logs.");
        logger.warn("A lista pode retornar vazia caso não haja logs cadastrados!");

        return logsService.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public LogsDTO salvar(@RequestBody LogsDTO logsDTO){
        logger.info("Adicionando novo log.");
        return logsService.salvarLog(logsDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public LogsDTO espacoEspecifico(@PathVariable String id) {
        logger.info("Buscando o log de id: " + id);
        return logsService.porId(id);
    }
}
