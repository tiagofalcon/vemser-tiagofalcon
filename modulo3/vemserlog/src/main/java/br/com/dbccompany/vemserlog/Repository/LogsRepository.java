package br.com.dbccompany.vemserlog.Repository;

import br.com.dbccompany.vemserlog.Entity.LogsEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogsRepository extends MongoRepository<LogsEntity, String> {
    List<LogsEntity> findByCodigo(String codigo);
    List<LogsEntity> findByMensagem(String mensagem);
    List<LogsEntity> findByCodigoAndMensagem(String codigo, String mensagem);
}
