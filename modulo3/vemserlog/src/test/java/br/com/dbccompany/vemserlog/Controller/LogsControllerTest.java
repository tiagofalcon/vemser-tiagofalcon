package br.com.dbccompany.vemserlog.Controller;

import br.com.dbccompany.vemserlog.Entity.LogsEntity;
import br.com.dbccompany.vemserlog.Repository.LogsRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class LogsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LogsRepository logsRepository;

    @AfterEach
    public void limpar() {
        logsRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoLogs () throws  Exception {
        URI uri = new URI("/apiLogs/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmLogsEntity() throws Exception {
        URI uri = new URI("/apiLogs/novo");
        String json = "{\"id\": \"5484srs154fs4\"," +
                "    \"codigo\": \"01lff5\"," +
                "    \"mensagem\": \"dessa vez foi\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo").value("01lff5")
        );
    }

    @Test
    public void deveRetornarUmLogsEntity() throws Exception {
        LogsEntity logsEntity = new LogsEntity();
        logsEntity.setId("bbbqq");
        logsEntity.setCodigo("65po");
        logsEntity.setMensagem("Nova Mensagem");
        LogsEntity logsEntity1 = logsRepository.save(logsEntity);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/apiLogs/ver/{id}", logsEntity1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo")
                .value("65po")
        );
    }

}
