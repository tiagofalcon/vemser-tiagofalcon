package br.com.dbccompany.vemserlog.Repository;

import br.com.dbccompany.vemserlog.Entity.LogsEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
public class LogsRepositoryTest {

    @Autowired
    private LogsRepository logsRepository;

    @AfterEach
    public void limpar() {
        logsRepository.deleteAll();
    }

    @Test
    public void salvaLogPorIdECodigoEMensagem() {
        LogsEntity logsEntity = new LogsEntity();
        logsEntity.setId("aaa111");
        logsEntity.setCodigo("3040aa");
        logsEntity.setMensagem("Nova Mensagem");
        logsRepository.save(logsEntity);

        assertEquals("aaa111", logsRepository.findByMensagem("Nova Mensagem").get(0).getId());
        assertEquals("aaa111", logsRepository.findByMensagem("Nova Mensagem").get(0).getId());
        assertEquals("aaa111", logsRepository.findByCodigo("3040aa").get(0).getId());
        assertEquals("aaa111", logsRepository.findByCodigoAndMensagem("3040aa","Nova Mensagem").get(0).getId());
        assertEquals("3040aa", logsRepository.findById("aaa111").get().getCodigo());
    }

    @Test
    public void retornaOptionalEmptySeLogForCriado() {

        assertEquals(Optional.empty(),logsRepository.findById("aaa111"));
    }

    @Test
    public void salvaDoisLogsERetornaTodos() {
        LogsEntity logsEntity = new LogsEntity();
        logsEntity.setId("aaa111");
        logsEntity.setCodigo("3040aa");
        logsEntity.setMensagem("Nova Mensagem");
        logsRepository.save(logsEntity);

        LogsEntity logsEntity2 = new LogsEntity();
        logsEntity2.setId("222aaa111");
        logsEntity2.setCodigo("2223040aa");
        logsEntity2.setMensagem("Novissima Mensagem");
        logsRepository.save(logsEntity2);

        List<LogsEntity> logsEntities = logsRepository.findAll();

        assertEquals("aaa111", logsEntities.get(0).getId());
        assertEquals("222aaa111", logsEntities.get(1).getId());
    }
}
