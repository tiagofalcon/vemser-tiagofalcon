package br.com.dbccompany.vemser.Exception;

public class ExceptionBanco extends Exception {

    private String mensagem;

    //metodos com novas excecoes
    //usa essa classe para extender novas excecoes

    public ExceptionBanco(String mensagem) {
        super(mensagem);
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
