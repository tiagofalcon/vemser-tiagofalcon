package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.MovimentacaoEntity;
import br.com.dbccompany.vemser.Entity.TipoMovimentacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovimentacaoRepository extends CrudRepository<MovimentacaoEntity, Integer> {

    List<MovimentacaoEntity> findByValor(double valor);
    List<MovimentacaoEntity> findByTipoMovimentacao(TipoMovimentacaoEnum tipoMovimentacao);
    List<MovimentacaoEntity> findByConta(ContaEntity conta);

}
