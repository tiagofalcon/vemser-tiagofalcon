package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;

@Entity
public class EnderecoEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
    @GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String logradouro;
    private int numero;
    private String complemento;
    @Column(length = 8)
    private String cep;
    private String bairro;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn (name = "ID_CIDADE")
    private CidadeEntity cidade;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "ID_AGENCIA")
    //@OneToOne(mappedBy = "endereco", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private AgenciaEntity agencia;

    @OneToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "ID_USUARIO")
    private UsuarioEntity usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCEP() {
        return cep;
    }

    public void setCEP(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public CidadeEntity getCidade() {
        return cidade;
    }

    public void setCidade(CidadeEntity cidade) {
        this.cidade = cidade;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }
}
