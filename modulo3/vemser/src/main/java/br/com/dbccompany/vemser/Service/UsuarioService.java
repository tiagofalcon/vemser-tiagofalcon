package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.*;
import br.com.dbccompany.vemser.Repository.UsuarioRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@Primary
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity editarPeloEndereco(UsuarioEntity usuario, EnderecoEntity endereco) {
        usuario.setEndereco(endereco);
        return repository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity editarPeloCpf(UsuarioEntity usuario, String cpf) {
        usuario.setCpf(cpf);
        return repository.save(usuario);
    }

    public List<UsuarioEntity> usuariosPeloNome(String nome) {
        return repository.findByNome(nome);
    }

    public List<UsuarioEntity> usuariosPeloEstadoCivil(EstadoCivilEnum estadoCivil) {
        return repository.findByEstadoCivil(estadoCivil);
    }

    public List<UsuarioEntity> usuariosPelaDataNascimento(Date dataNascimento) {
        return repository.findBydataNascimento(dataNascimento);
    }

    public List<UsuarioEntity> usuariosPeloBanco(BancoEntity banco) {
        List<ContaClienteEntity> contasClientes = banco.getContasClientes();
        return retornaUsuariosPorContasClientes(contasClientes);
    }

    public List<UsuarioEntity> usuarioPelaConta(ContaEntity conta) {
        List<ContaClienteEntity> contasClientes = conta.getContasClientes();
        return retornaUsuariosPorContasClientes(contasClientes);
    }
    private List<UsuarioEntity> retornaUsuariosPorContasClientes(List<ContaClienteEntity> contasClientes) {
        List<UsuarioEntity> auxiliar = new ArrayList<>();
        for (ContaClienteEntity contaCliente : contasClientes ) {
            auxiliar.add(contaCliente.getUsuario());
        }
        return auxiliar;
    }

    public UsuarioEntity usuarioPeloEndereco(EnderecoEntity endereco) {
        return repository.findByEndereco(endereco);
    }

    public UsuarioEntity usuarioPeloCpf(String cpf) {
        return repository.findByCpf(cpf);
    }
}
