package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsolidacaoRepository extends CrudRepository<ConsolidacaoEntity, Integer> {
    ConsolidacaoEntity findByAgencia(AgenciaEntity agencia);
    List<ConsolidacaoEntity> findBySaldoAtual(double saldoAtual);
    List<ConsolidacaoEntity> findBySaques(double saques);
    List<ConsolidacaoEntity> findByDepositos(double depositos);
    List<ConsolidacaoEntity> findByQtdCorrentistas(int qtdCorrentistas);
}
