package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.Service.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class BaseAbstractController<T extends EntityAbstract, S extends ServiceAbstract, ID> {

    @Autowired
    protected S service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<T> todosEntidade() {
        return service.todos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public T salvar(@RequestBody T entidade) {
        return (T) service.salvar(entidade);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public T entidadeEspecifica(@PathVariable ID id) {
        return (T) service.porId(id);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public T editarEntidade(@PathVariable ID id, @RequestBody T entidade) {
        return (T) service.editar(entidade, id);
    }
}
