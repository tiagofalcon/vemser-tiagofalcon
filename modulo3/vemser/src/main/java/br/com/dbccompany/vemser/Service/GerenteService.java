package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Entity.TipoGerenteEnum;
import br.com.dbccompany.vemser.Repository.GerenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GerenteService extends UsuarioService {

    @Autowired
    private GerenteRepository repository;

    public List<GerenteEntity> gerentesPeloCodigo(int codigo) {
        return this.repository.findByCodigo(codigo);
    }

    public List<GerenteEntity> gerentesPeloTipoGerente(TipoGerenteEnum tipoGerente) {
        return this.repository.findByTipoGerente(tipoGerente);
    }

    public List<GerenteEntity> gerentesPelaConta(ContaEntity conta) {
        return conta.getGerentes();
    }

}
