package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Repository.TipoContaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoContaService extends ServiceAbstract<TipoContaRepository, TipoContaEntity, Integer> {

    public TipoContaEntity tipoContaPeloNome(String nome) {
        return repository.findByNome(nome);
    }

    public TipoContaEntity tipoContaPelaConta(ContaEntity conta) {
        List<TipoContaEntity> tiposConta = this.todos();
        for (TipoContaEntity tipoConta : tiposConta) {
            if(tipoConta.getContas().contains(conta)){
                return tipoConta;
            }
        }
        return null;
    }


}
