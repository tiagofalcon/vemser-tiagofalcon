package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {
    List<EnderecoEntity> findByCidade(CidadeEntity cidade);
    EnderecoEntity findByAgencia(AgenciaEntity agencia);
    EnderecoEntity findByUsuario(UsuarioEntity usuario);
    List<EnderecoEntity> findByLogradouro(String logradouro);
    List<EnderecoEntity> findByNumero(int numero);
    List<EnderecoEntity> findByComplemento(String complemento);
    List<EnderecoEntity> findByCep(String cep);
    List<EnderecoEntity> findByBairro(String bairro);
    EnderecoEntity findByLogradouroAndNumeroAndComplementoAndCepAndBairroAndCidade(String logradouro, int numero, String complemento, String cep, String bairro, CidadeEntity cidade);
}
