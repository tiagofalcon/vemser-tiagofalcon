package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Service.EstadoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/estado")
public class EstadoController extends BaseAbstractController<EstadoEntity, EstadoService, Integer> {
}
