package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Service.CidadeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController extends BaseAbstractController<CidadeEntity, CidadeService, Integer>{
}
