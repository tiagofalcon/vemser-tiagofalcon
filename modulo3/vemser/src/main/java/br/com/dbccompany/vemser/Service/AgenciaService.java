package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.*;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AgenciaService extends ServiceAbstract<AgenciaRepository, AgenciaEntity, Integer> {

    @Autowired
    private AgenciaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public AgenciaEntity editarPeloEndereco(AgenciaEntity agencia, EnderecoEntity endereco) {
        agencia.setEndereco(endereco);
        return repository.save(agencia);
    }

    @Transactional(rollbackFor = Exception.class)
    public AgenciaEntity editarPelaConsolidacao(AgenciaEntity agencia, ConsolidacaoEntity consolidacao) {
        agencia.setConsolidacao(consolidacao);
        return repository.save(agencia);
    }

    public List<AgenciaEntity> agenciasPeloCodigo(int codigo) {
        return repository.findByCodigo(codigo);
    }

    public List<AgenciaEntity> agenciasPeloBanco(BancoEntity banco) {
        return repository.findByBanco(banco);
    }

    public List<AgenciaEntity> agenciasPeloNome(String nome) {
        return repository.findByNome(nome);
    }

    public List<AgenciaEntity> agenciasPelaConta(ContaEntity conta) {
        List<AgenciaEntity> agencias = this.todos();
        List<AgenciaEntity> auxiliar = new ArrayList<>();
        for (AgenciaEntity agencia : agencias ) {
            if(agencia.getContas().contains(conta)){
                auxiliar.add(agencia);
            }
        }
        return auxiliar;
    }

    AgenciaEntity agenciaPeloEndereco(EnderecoEntity endereco) {
        return repository.findByEndereco(endereco);
    }

    AgenciaEntity agenciaPelaConsolidacao(ConsolidacaoEntity consolidacao) {
        return repository.findByConsolidacao(consolidacao);
    }

}
