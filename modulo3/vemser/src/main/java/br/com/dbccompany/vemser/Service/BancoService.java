package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.*;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import br.com.dbccompany.vemser.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BancoService extends ServiceAbstract<BancoRepository, BancoEntity, Integer> {



    @Transactional(rollbackFor = Exception.class)
    public BancoEntity editarComCodigo(BancoEntity banco, int codigo) {
        banco.setCodigo(codigo);
        return repository.save(banco);
    }

    public BancoEntity bancoPorCodigo(int codigo) {
        return repository.findByCodigo(codigo);
    }


    public List<BancoEntity> bancosPorNome(String nome) {
        return repository.findByNome(nome);
    }

    public List<BancoEntity> bancosPorAgencia(AgenciaEntity agencia) {
        List<BancoEntity> bancos = this.todos();
        List<BancoEntity> auxiliar = new ArrayList<>();
        for (BancoEntity banco : bancos ) {
            if(banco.getAgencias().contains(agencia)){
                auxiliar.add(banco);
            }
        }
        return auxiliar;
    }

    public List<BancoEntity> bancosPorContasCliente(ContaClienteEntity contaCliente) {
        List<BancoEntity> bancos = this.todos();
        List<BancoEntity> auxiliar = new ArrayList<>();
        for (BancoEntity banco : bancos ) {
            if(banco.getContasClientes().contains(contaCliente)){
                auxiliar.add(banco);
            }
        }
        return auxiliar;
    }

    public List<BancoEntity> bancosPorConta(ContaEntity conta) {
        List<ContaClienteEntity> contasClientes = conta.getContasClientes();
        return retornaBancosPorContasClientes(contasClientes);
    }

    public List<BancoEntity> bancosPorUsuario(UsuarioEntity usuario) {
        List<ContaClienteEntity> contasClientes = usuario.getContasClientes();
        return retornaBancosPorContasClientes(contasClientes);
    }
    private List<BancoEntity> retornaBancosPorContasClientes(List<ContaClienteEntity> contasClientes) {
        List<BancoEntity> auxiliar = new ArrayList<>();
        for (ContaClienteEntity contaCliente : contasClientes ) {
            auxiliar.add(contaCliente.getBanco());
        }
        return auxiliar;
    }
}
