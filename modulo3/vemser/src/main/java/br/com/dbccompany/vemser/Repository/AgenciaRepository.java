package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciaRepository extends CrudRepository<AgenciaEntity,Integer> {

    AgenciaEntity findByConsolidacao(ConsolidacaoEntity consolidacao);
    AgenciaEntity findByEndereco(EnderecoEntity endereco);
    List<AgenciaEntity> findByCodigo(int codigo);
    List<AgenciaEntity> findByNome(String nome);
    List<AgenciaEntity> findByBanco(BancoEntity banco);

}
