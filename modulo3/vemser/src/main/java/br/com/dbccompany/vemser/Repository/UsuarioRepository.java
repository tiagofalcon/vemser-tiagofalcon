package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.EstadoCivilEnum;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

    UsuarioEntity findByEndereco(EnderecoEntity endereco);
    UsuarioEntity findByCpf(String cpf);
    List<UsuarioEntity> findByNome(String nome);
    List<UsuarioEntity> findByEstadoCivil(EstadoCivilEnum estadoCivil);
    List<UsuarioEntity> findBydataNascimento(Date dataNascimento);
    Optional<UsuarioEntity> findByLogin(String login);
}
