package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class MovimentacaoEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "MOVIMENTO_SEQ", sequenceName = "MOVIMENTO_SEQ")
    @GeneratedValue(generator = "MOVIMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private double valor;

    @Enumerated(EnumType.STRING)
    private TipoMovimentacaoEnum tipoMovimentacao;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name="ID_CONTA"),
            @JoinColumn(name = "ID_TIPO_CONTA")
    })
    private ContaEntity conta;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public TipoMovimentacaoEnum getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacaoEnum tipoMovimentaco) {
        this.tipoMovimentacao = tipoMovimentaco;
    }

    public ContaEntity getConta() {
        return conta;
    }

    public void setConta(ContaEntity conta) {
        this.conta = conta;
    }
}
