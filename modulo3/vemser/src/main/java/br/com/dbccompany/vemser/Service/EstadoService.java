package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EstadoService extends ServiceAbstract<EstadoRepository, EstadoEntity, Integer> {

    public List<EstadoEntity> todosOsEstadosDeUmPais(PaisEntity pais) {
        return repository.findByPais(pais);
    }

}
