package br.com.dbccompany.vemser.DTO;

import br.com.dbccompany.vemser.Entity.*;

public class GeralDTO {

    private PaisEntity pais;
    private EstadoEntity estado;
    private CidadeEntity cidade;
    private EnderecoEntity entereco;
    private BancoEntity banco;
    private AgenciaEntity agencia;
    private ContaEntity conta;

    public PaisEntity getPais() {
        return pais;
    }

    public void setPais(PaisEntity pais) {
        this.pais = pais;
    }

    public EstadoEntity getEstado() {
        return estado;
    }

    public void setEstado(EstadoEntity estado) {
        this.estado = estado;
    }

    public CidadeEntity getCidade() {
        return cidade;
    }

    public void setCidade(CidadeEntity cidade) {
        this.cidade = cidade;
    }

    public EnderecoEntity getEntereco() {
        return entereco;
    }

    public void setEntereco(EnderecoEntity entereco) {
        this.entereco = entereco;
    }

    public BancoEntity getBanco() {
        return banco;
    }

    public void setBanco(BancoEntity banco) {
        this.banco = banco;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public ContaEntity getConta() {
        return conta;
    }

    public void setConta(ContaEntity conta) {
        this.conta = conta;
    }
}
