package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.*;
import br.com.dbccompany.vemser.Repository.ContaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContaService  extends ServiceAbstract<ContaRepository, ContaEntity, ContaEntityId> {

    @Transactional(rollbackFor = Exception.class)
    public ContaEntity editar(ContaEntity entidade, Integer id, Integer idTipoConta) {
        ContaEntityId newId = new ContaEntityId(id, idTipoConta);
        entidade.setId(newId);

        return repository.save(entidade);
    }

    public List<ContaEntity> contasPeloCodigo(int codigo) {
        return repository.findByCodigo(codigo);
    }

    public List<ContaEntity> contasPeloSaldo(double saldo) {
        return repository.findBySaldo(saldo);
    }

    public List<ContaEntity> contasPelaAgencia(AgenciaEntity agencia) {
        return repository.findByAgencia(agencia);
    }

    public List<ContaEntity> contasPeloTipoConta(TipoContaEntity tipoConta) {
        return repository.findByTipoConta(tipoConta);
    }

    public List<ContaEntity> contasPorBanco(BancoEntity banco) {
        List<ContaClienteEntity> contasClientes = banco.getContasClientes();
        return retornaContasPorContasClientes(contasClientes);
    }

    public List<ContaEntity> contasPorUsuario(UsuarioEntity usuario) {
        List<ContaClienteEntity> contasClientes = usuario.getContasClientes();
        return retornaContasPorContasClientes(contasClientes);
    }
    private List<ContaEntity> retornaContasPorContasClientes(List<ContaClienteEntity> contasClientes) {
        List<ContaEntity> auxiliar = new ArrayList<>();
        for (ContaClienteEntity contaCliente : contasClientes ) {
            auxiliar.add(contaCliente.getConta());
        }
        return auxiliar;
    }

    public List<ContaEntity> contasPeloGerente(GerenteEntity gerente) {
        List<ContaEntity> contas = this.todos();
        List<ContaEntity> auxiliar = new ArrayList<>();
        for (ContaEntity conta : contas ) {
            for (GerenteEntity gerenteEntity : conta.getGerentes()) {
                boolean comparacao = gerenteEntity.getCpf().equals(gerente.getCpf());
                if (comparacao) {
                    auxiliar.add(conta);
                }
            }
        }
        return auxiliar;
    }

    public ContaEntity contasPelaMovimentacao(MovimentacaoEntity movimentacao) {
        List<ContaEntity> contas = this.todos();
        for (ContaEntity conta : contas ) {
            if(conta.getMovimentacoes().contains(movimentacao)) {
                return conta;
            }
        }
        return null;
    }
}
