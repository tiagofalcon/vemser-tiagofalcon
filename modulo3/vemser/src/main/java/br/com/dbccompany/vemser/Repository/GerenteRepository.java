package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Entity.TipoGerenteEnum;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GerenteRepository extends CrudRepository<GerenteEntity, Integer> {

    List<GerenteEntity> findByCodigo(int codigo);
    List<GerenteEntity> findByTipoGerente(TipoGerenteEnum tipogerente);

}
