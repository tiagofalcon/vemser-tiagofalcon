package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EnderecoService extends ServiceAbstract<EnderecoRepository, EnderecoEntity, Integer> {

    @Transactional(rollbackFor = Exception.class)
    public EnderecoEntity editarComUsuario(EnderecoEntity endereco, UsuarioEntity usuario) {
        endereco.setUsuario(usuario);
        return repository.save(endereco);
    }

    @Transactional(rollbackFor = Exception.class)
    public EnderecoEntity editarComAgencia(EnderecoEntity endereco, AgenciaEntity agencia) {
        endereco.setAgencia(agencia);
        return repository.save(endereco);
    }

    public List<EnderecoEntity> todosOsEnderecosDeUmaCidade(CidadeEntity cidade) {
        return repository.findByCidade(cidade);
    }

    public List<EnderecoEntity> todosOsEnderecosDeUmLogradouro(String logradouro) {
        return repository.findByLogradouro(logradouro);
    }

    public List<EnderecoEntity> todosOsEnderecosDeUmNumero(int numero) {
        return repository.findByNumero(numero);
    }

    public List<EnderecoEntity> todosOsEnderecosDeUmComplemento(String complemento) {
        return repository.findByComplemento(complemento);
    }

    public List<EnderecoEntity> todosOsEnderecosDeUmCep(String cep) {
        return repository.findByCep(cep);
    }

    public List<EnderecoEntity> todosOsEnderecosDeUmBairro(String bairro) {
        return repository.findByBairro(bairro);
    }

    public EnderecoEntity enderecoPelaAgencia(AgenciaEntity agencia) {
        return repository.findByAgencia(agencia);
    }

    public EnderecoEntity enderecoPeloUsuario(UsuarioEntity usuario) {
        return repository.findByUsuario(usuario);
    }

    public EnderecoEntity enderecoPeloEnderecoCompleto(String logradouro, int numero, String complemento, String cep, String bairro, CidadeEntity cidade) {
        return repository.findByLogradouroAndNumeroAndComplementoAndCepAndBairroAndCidade(logradouro, numero, complemento, cep, bairro, cidade);
    }
}
