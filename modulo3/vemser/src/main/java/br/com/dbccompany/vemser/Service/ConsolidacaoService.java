package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Repository.ConsolidacaoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConsolidacaoService  extends ServiceAbstract<ConsolidacaoRepository, ConsolidacaoEntity, Integer>  {


    @Transactional(rollbackFor = Exception.class)
    public ConsolidacaoEntity editarPorAgencia(ConsolidacaoEntity consolidacao, AgenciaEntity agencia) {
        consolidacao.setAgencia(agencia);
        return repository.save(consolidacao);
    }

    List<ConsolidacaoEntity> consolidacoesPeloSaldoAtual(double saldoAtual) {
        return repository.findBySaldoAtual(saldoAtual);
    }

    List<ConsolidacaoEntity> consolidacoesPelosSaques(double saques) {
        return repository.findBySaques(saques);
    }

    List<ConsolidacaoEntity> consolidacoesPelosDepositos(double depositos) {
        return repository.findByDepositos(depositos);
    }

    List<ConsolidacaoEntity> consolidacoesPelaQtdCorrentistas(int qtdCorrentistas) {
        return repository.findByQtdCorrentistas(qtdCorrentistas);
    }

    ConsolidacaoEntity consolidacaoPelaAgencia(AgenciaEntity agencia) {
        return repository.findByAgencia(agencia);
    }
}
