package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoRepository extends CrudRepository<EstadoEntity, Integer> {
    EstadoEntity findByNome(String nome);
    List<EstadoEntity> findByPais(PaisEntity pais);
    List<EstadoEntity> findAllByNome(String nome);
}
