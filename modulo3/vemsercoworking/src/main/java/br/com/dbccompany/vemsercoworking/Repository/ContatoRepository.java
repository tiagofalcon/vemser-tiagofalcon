package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    ContatoEntity findByValor(String nome);
    List<ContatoEntity> findByTipoContato(TipoContatoEntity tipoContato);
    List<ContatoEntity> findByCliente(ClienteEntity cliente);
    List<ContatoEntity> findByTipoContatoAndCliente(TipoContatoEntity tipoContato, ClienteEntity cliente);

}
