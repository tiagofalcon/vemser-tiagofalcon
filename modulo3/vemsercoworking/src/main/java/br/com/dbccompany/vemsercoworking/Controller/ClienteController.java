package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.ClienteDTO;
import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Service.ClienteService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
    @Autowired
    ClienteService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClienteDTO> todosCliente() {
        logger.info("Buscando os usuarios.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<ClienteDTO> listaDTO = new ArrayList<>();
        for (ClienteEntity cliente : service.todos()) {
            listaDTO.add(new ClienteDTO(cliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteDTO salvar(@RequestBody ClienteDTO clienteDTO) {
        logger.info("Adicionando novo cliente.");
        ClienteEntity clienteEntity = clienteDTO.convert();
        ClienteDTO newDto = new ClienteDTO(service.salvar(clienteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClienteDTO clienteEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o cliente de id: " + id);
        ClienteDTO clienteDTO = new ClienteDTO(service.porId(id));
        return clienteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteDTO editarCliente(@PathVariable Integer id, @RequestBody ClienteDTO clienteDTO) {
        logger.info("Editando o cliente de id: " + id);
        ClienteEntity cliente = clienteDTO.convert();
        ClienteDTO newDTO = new ClienteDTO(service.editar(cliente, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarCliente(@PathVariable Integer id) {
        logger.info("Deletando cliente de id: " + id);
        service.deletarPorId(id);
        return "Cliente removido";
    }
}
