package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.UsuarioDTO;
import br.com.dbccompany.vemsercoworking.Entity.UsuarioEntity;
import br.com.dbccompany.vemsercoworking.Service.UsuarioService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioDTO> todosUsuario() {
        logger.info("Buscando os usuarios.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        return service.retornarListaUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public UsuarioDTO salvar(@RequestBody UsuarioDTO usuarioDTO){
        logger.info("Adicionando novo usuario.");
        UsuarioDTO newDto = service.salvarComValidacaoECriptografiaDeSenha(usuarioDTO);
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public UsuarioDTO usuarioEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o usuario de id: " + id);
        UsuarioDTO usuarioDTO = new UsuarioDTO(service.porId(id));
        return usuarioDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioDTO editarUsuario(@PathVariable Integer id, @RequestBody UsuarioDTO usuarioDTO) {
        logger.info("Editando o usuario de id: " + id);
        UsuarioEntity usuario = usuarioDTO.convert();
        UsuarioDTO newDTO = new UsuarioDTO(service.editar(usuario, id));
        return newDTO;
    }

    @PostMapping(value = "/login")
    @ResponseBody
    public boolean fazerLogin(@RequestBody UsuarioDTO login) {
        logger.info(login.getNome() + "Logando no sistema");
        return service.login(login.getLogin(), login.getSenha());
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarUsuario(@PathVariable Integer id) {
        logger.info("Deletando usuário de id: " + id);
        service.deletarPorId(id);
        return "Usuário removido";
    }
}
