package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    ClienteEntity findByCpf(String cpf);
    List<ClienteEntity> findByNome(String nome);
    List<ClienteEntity> findByDataNascimento(Date dataNascimento);
}
