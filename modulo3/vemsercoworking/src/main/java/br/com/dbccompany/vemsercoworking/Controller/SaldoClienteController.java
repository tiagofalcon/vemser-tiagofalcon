package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemsercoworking.Service.SaldoClienteService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<SaldoClienteDTO> todosSaldoCliente() {
        logger.info("Buscando saldos de clientes.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<SaldoClienteDTO> listaDTO = new ArrayList<>();
        for (SaldoClienteEntity saldoCliente : service.todos()) {
            listaDTO.add(new SaldoClienteDTO(saldoCliente));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoClienteDTO salvar(@RequestBody SaldoClienteDTO saldoClienteDTO){
        logger.info("Adicionando novo saldo de cliente.");
        SaldoClienteEntity saldoClienteEntity = saldoClienteDTO.convert();
        SaldoClienteDTO newDto = new SaldoClienteDTO(service.salvar(saldoClienteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoClienteDTO saldoClienteEspecifico(@PathVariable Integer idCliente, @PathVariable Integer idEspaco) {
        logger.info("Buscando saldo do cliente:" + idCliente + ". No espaço de id: " + idEspaco);
        Optional<SaldoClienteEntity> optionalEntity = service.porId(idCliente, idEspaco);
        SaldoClienteEntity saldoClienteEntity = optionalEntity.isPresent() ? optionalEntity.get() : null;
        SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO(saldoClienteEntity);
        return saldoClienteDTO;
    }

    @PutMapping(value = "/editar/{idCliente}/{idEspaco}")
    @ResponseBody
    public SaldoClienteDTO editarSaldoCliente(@PathVariable Integer idCliente, @PathVariable Integer idEspaco, @RequestBody SaldoClienteDTO saldoClienteDTO) {
        logger.info("Editando saldo do cliente: " + idCliente + ". No espaço de id: " + idEspaco);
        SaldoClienteEntity saldoCliente = saldoClienteDTO.convert();
        SaldoClienteDTO newDTO = new SaldoClienteDTO(service.editar(saldoCliente, idCliente, idEspaco));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{idCliente}/{idEspaco}")
    @ResponseBody
    public String deletarTipoContato(@PathVariable Integer idCliente, @PathVariable Integer idEspaco) {
        logger.info("Deletando saldo do cliente de id: " + idCliente + ". No espaço de id: " + idEspaco);
        service.deletarSaldoPorId(idCliente, idEspaco);
        return "Saldo do cliente removido";
    }
}
