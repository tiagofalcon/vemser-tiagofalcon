package br.com.dbccompany.vemsercoworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;
    @Column(nullable = false)
    private int qtdPessoas;
    @Column(nullable = false)
    private double valor;

    @OneToMany(mappedBy = "espaco")
    private List<SaldoClienteEntity> saldosClientes;

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacoteEntity> espacosPacotes;

    @OneToMany(mappedBy = "espaco")
    private List<ContratacaoEntity> contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
