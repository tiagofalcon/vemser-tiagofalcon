package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.ContatoDTO;
import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Service.ContatoService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    ContatoService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContatoDTO> todosContato() {
        logger.info("Buscando os contatos.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<ContatoDTO> listaDTO = new ArrayList<>();
        for (ContatoEntity contato : service.todos()) {
            listaDTO.add(new ContatoDTO(contato));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContatoDTO salvar(@RequestBody ContatoDTO contatoDTO){
        logger.info("Adicionando novo contato.");
        ContatoEntity contatoEntity = contatoDTO.convert();
        ContatoDTO newDto = new ContatoDTO(service.salvar(contatoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContatoDTO contatoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o contato de id: " + id);
        ContatoDTO contatoDTO = new ContatoDTO(service.porId(id));
        return contatoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContatoDTO editarContato(@PathVariable Integer id, @RequestBody ContatoDTO contatoDTO) {
        logger.info("Editando o contato de id: " + id);
        ContatoEntity contato = contatoDTO.convert();
        ContatoDTO newDTO = new ContatoDTO(service.editar(contato, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarContato(@PathVariable Integer id) {
        logger.info("Deletando contato de id: " + id);
        service.deletarPorId(id);
        return "Contato removido";
    }
}
