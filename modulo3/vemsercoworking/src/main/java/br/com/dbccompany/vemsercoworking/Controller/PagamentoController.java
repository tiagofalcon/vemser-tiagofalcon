package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.PagamentoDTO;
import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Service.PagamentoService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PagamentoDTO> todosPagamento() {
        logger.info("Buscando pagamentos.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<PagamentoDTO> listaDTO = new ArrayList<>();
        for (PagamentoEntity pagamento : service.todos()) {
            listaDTO.add(new PagamentoDTO(pagamento));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PagamentoDTO salvar(@RequestBody PagamentoDTO pagamentoDTO){
        logger.info("Adicionando novo pagamento.");
        return service.salvarAceitandoClientePacoteOuContratacaoOpcional(pagamentoDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PagamentoDTO pagamentoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando pagamento de id: " + id);
        PagamentoDTO pagamentoDTO = new PagamentoDTO(service.porId(id));
        return pagamentoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PagamentoDTO editarPagamento(@PathVariable Integer id, @RequestBody PagamentoDTO pagamentoDTO) {
        logger.info("Editando pagamento de id: " + id);
        PagamentoEntity pagamento = pagamentoDTO.convert();
        PagamentoDTO newDTO = new PagamentoDTO(service.editar(pagamento, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarPagamento(@PathVariable Integer id) {
        logger.info("Deletando pagamento de id: " + id);
        service.deletarPagamentoPorIdERemoveSaldoAssociado(id);
        return "Pagamento removido";
    }
}
