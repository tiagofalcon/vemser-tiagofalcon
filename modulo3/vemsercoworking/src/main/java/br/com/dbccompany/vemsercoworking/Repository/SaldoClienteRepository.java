package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteId;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {

    List<SaldoClienteEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    List<SaldoClienteEntity> findByQuantidade(int quantidade);
    List<SaldoClienteEntity> findByVencimento(LocalDate vencimento);

}
