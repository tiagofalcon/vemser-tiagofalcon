package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
    List<ClientePacoteEntity> findByCliente(ClienteEntity cliente);
    List<ClientePacoteEntity> findByPacote(PacoteEntity pacote);
    List<ClientePacoteEntity> findByQuantidade(int quantidade);
}
