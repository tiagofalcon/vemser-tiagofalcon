package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.AcessoDTO;
import br.com.dbccompany.vemsercoworking.DTO.TipoContatoDTO;
import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import br.com.dbccompany.vemsercoworking.Service.TipoContatoService;
import br.com.dbccompany.vemsercoworking.Service.TipoContatoService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoDTO> todosTipoContato() {
        logger.info("Buscando tipos de contato.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<TipoContatoDTO> listaDTO = new ArrayList<>();
        for (TipoContatoEntity tipoContato : service.todos()) {
            listaDTO.add(new TipoContatoDTO(tipoContato));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContatoDTO salvar(@RequestBody TipoContatoDTO tipoContatoDTO){
        logger.info("Adicionando novo tipo de contato.");
        TipoContatoEntity tipoContatoEntity = tipoContatoDTO.convert();
        TipoContatoDTO newDto = new TipoContatoDTO(service.salvar(tipoContatoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public TipoContatoDTO tipoContatoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando tipo de contato de id: " + id);
        TipoContatoDTO tipoContatoDTO = new TipoContatoDTO(service.porId(id));
        return tipoContatoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContatoDTO editarTipoContato(@PathVariable Integer id, @RequestBody TipoContatoDTO tipoContatoDTO) {
        logger.info("Editando tipo de contato de id: " + id);
        TipoContatoEntity tipoContato = tipoContatoDTO.convert();
        TipoContatoDTO newDTO = new TipoContatoDTO(service.editar(tipoContato, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarTipoContato(@PathVariable Integer id) {
        logger.info("Deletando tipo de contato de id: " + id);
        service.deletarPorId(id);
        return "Tipo de contato removido";
    }
}
