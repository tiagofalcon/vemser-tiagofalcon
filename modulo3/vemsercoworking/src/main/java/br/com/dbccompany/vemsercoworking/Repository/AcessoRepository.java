package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer>{
    List<AcessoEntity> findBySaldoCliente(SaldoClienteEntity saldoCliente);
    List<AcessoEntity> findByIsEntradaTrue();
    List<AcessoEntity> findByIsEntradaFalse();
    List<AcessoEntity> findByData(LocalDate data);
    List<AcessoEntity> findByIsExcecaoTrue();
    List<AcessoEntity> findByIsExcecaoFalse();
}
