package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    EspacoEntity findByNome(String nome);
    List<EspacoEntity> findByValor(double valor);
    List<EspacoEntity> findByQtdPessoas(int  QtdPessoas);
}
