package br.com.dbccompany.vemsercoworking.Util;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;

public class CalculaValorEspacoQuantidadeUsadaUtil {

    public double calculaValorTotal(TipoContratacaoEnum tipoContratacaoEnum, int quantidade, EspacoEntity espacoEntity) {
        double valor = 0;
        switch (tipoContratacaoEnum) {
            case MES:
                valor = quantidade * 30 * espacoEntity.getValor();
                break;
            case SEMANA:
                valor = quantidade * 7 * espacoEntity.getValor();
                break;
            case DIARIA:
                valor = quantidade * espacoEntity.getValor();
                break;
            case HORA:
                valor = Math.round(Math.ceil(quantidade/24.0)) * espacoEntity.getValor();
                break;
            case MINUTO:
                valor = Math.round(Math.ceil(quantidade/24.0/60.0)) * espacoEntity.getValor();
                break;
        }
        return valor;
    }
}
