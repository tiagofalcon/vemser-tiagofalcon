package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    List<ContratacaoEntity> findByEspaco(EspacoEntity espaco);
    List<ContratacaoEntity> findByCliente(ClienteEntity cliente);
    List<ContratacaoEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    List<ContratacaoEntity> findByQuantidade(int quantidade);
    List<ContratacaoEntity> findByDesconto(double desconto);
    List<ContratacaoEntity> findByPrazo(int prazo);
}
