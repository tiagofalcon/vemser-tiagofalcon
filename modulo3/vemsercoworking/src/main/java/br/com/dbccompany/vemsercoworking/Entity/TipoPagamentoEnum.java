package br.com.dbccompany.vemsercoworking.Entity;

public enum TipoPagamentoEnum {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
