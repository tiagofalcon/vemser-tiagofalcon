package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.DTO.ContratacaoDTO;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.vemsercoworking.Repository.ContratacaoRepository;
import br.com.dbccompany.vemsercoworking.Util.ConvertePrazoHorasTurno;
import org.springframework.stereotype.Service;

@Service
public class ContratacaoService extends ServiceAbstract<ContratacaoRepository, ContratacaoEntity, Integer> {
    public ContratacaoDTO salvarContratacaoComQuantidadeTurno(ContratacaoDTO contratacaoDTO) {
        try {
            int quantidade = contratacaoDTO.getQuantidade();
            TipoContratacaoEnum tipoContratacaoEnum = contratacaoDTO.getTipoContratacao();
            contratacaoDTO.setQuantidade(ConvertePrazoHorasTurno.converteTurnoParaHoras(quantidade, tipoContratacaoEnum));

            ContratacaoEntity contratacaoEntity = contratacaoDTO.convert();
            ContratacaoDTO newDto = new ContratacaoDTO((super.salvar(contratacaoEntity)));
            return newDto;
        } catch (Exception e) {
            logger.error("Erro ao salvar contratacao com conversao de quantidade de horas: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
