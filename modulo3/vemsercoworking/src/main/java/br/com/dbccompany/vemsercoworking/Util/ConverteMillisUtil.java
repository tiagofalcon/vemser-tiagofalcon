package br.com.dbccompany.vemsercoworking.Util;

import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;

public class ConverteMillisUtil {
    public static int converteTimeMillisParaMinutoOuHora(long tempo, TipoContratacaoEnum tipoContratacaoEnum) {
        int tempoConvertido = 0;
        int minutos = (int) Math.round(Math.ceil((tempo / 1000) / 60));
        switch (tipoContratacaoEnum){
            case MINUTO:
                tempoConvertido = minutos;
                break;
            case HORA:
                tempoConvertido = (int) Math.round(Math.ceil((minutos / 60)));
                break;
            case TURNO:
                tempoConvertido = (int) Math.round(Math.ceil((minutos / 60) / 5));
                break;
        }
        return tempoConvertido;
    }
}
