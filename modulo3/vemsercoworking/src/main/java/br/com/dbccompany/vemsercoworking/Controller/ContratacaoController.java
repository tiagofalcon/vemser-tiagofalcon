package br.com.dbccompany.vemsercoworking.Controller;


import br.com.dbccompany.vemsercoworking.DTO.ContratacaoDTO;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Service.ContratacaoService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContratacaoDTO> todosContratacao() {
        logger.info("Buscando as contratações.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        for (ContratacaoEntity contratacao : service.todos()) {
            listaDTO.add(new ContratacaoDTO(contratacao));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContratacaoDTO salvar(@RequestBody ContratacaoDTO contratacaoDTO){
        logger.info("Adicionando nova contratação.");
        return service.salvarContratacaoComQuantidadeTurno(contratacaoDTO);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContratacaoDTO contratacaoEspecifico(@PathVariable Integer id) {
        logger.info("Buscando o contratação de id: " + id);
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.porId(id));
        return contratacaoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContratacaoDTO editarContratacao(@PathVariable Integer id, @RequestBody ContratacaoDTO contratacaoDTO) {
        logger.info("Editando o contratação de id: " + id);
        ContratacaoEntity contratacao = contratacaoDTO.convert();
        ContratacaoDTO newDTO = new ContratacaoDTO(service.editar(contratacao, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarContratacao(@PathVariable Integer id) {
        logger.info("Deletando contratação de id: " + id);
        service.deletarPorId(id);
        return "Contratação removida";
    }
}
