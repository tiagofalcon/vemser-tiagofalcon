package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.DTO.EspacoPacoteDTO;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.vemsercoworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.vemsercoworking.Util.ConvertePrazoHorasTurno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EspacoPacoteService extends ServiceAbstract<EspacoPacoteRepository, EspacoPacoteEntity, Integer> {

    @Autowired
    EspacoPacoteRepository espacoPacoteRepository;


    public EspacoPacoteDTO salvarEspacoComQuantidadeTurno(EspacoPacoteDTO espacoPacoteDTO) {
        try {
            int quantidade = espacoPacoteDTO.getQuantidade();
            TipoContratacaoEnum tipoContratacaoEnum = espacoPacoteDTO.getTipoContratacao();
            espacoPacoteDTO.setQuantidade(ConvertePrazoHorasTurno.converteTurnoParaHoras(quantidade, tipoContratacaoEnum));

            EspacoPacoteEntity espacoPacoteEntity = espacoPacoteDTO.convert();
            EspacoPacoteDTO newDto = new EspacoPacoteDTO((super.salvar(espacoPacoteEntity)));
            return newDto;
        } catch (Exception e) {
            logger.error("Erro ao salvar espaço com conversao de quantidade de horas: " + e.getMessage());
            throw new RuntimeException();
        }
    }
}
