package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.DTO.PacoteDTO;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Service.PacoteService;
import br.com.dbccompany.vemsercoworking.VemsercoworkingApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {

    @Autowired
    PacoteService service;

    private Logger logger = LoggerFactory.getLogger(VemsercoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<PacoteDTO> todosPacote() {
        logger.info("Buscando pacotes.");
        logger.warn("A lista pode retornar vazia caso não haja itens cadastrados!");
        List<PacoteDTO> listaDTO = new ArrayList<>();
        for (PacoteEntity pacote : service.todos()) {
            listaDTO.add(new PacoteDTO(pacote));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public PacoteDTO salvar(@RequestBody PacoteDTO pacoteDTO) {
        logger.info("Adicionando novo pacote.");
        PacoteEntity pacoteEntity = pacoteDTO.convert();
        PacoteDTO newDto = new PacoteDTO(service.salvar(pacoteEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PacoteDTO pacoteEspecifico(@PathVariable Integer id) {
        logger.info("Buscando pacote de id: " + id);
        PacoteDTO pacoteDTO = new PacoteDTO(service.porId(id));
        return pacoteDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public PacoteDTO editarPacote(@PathVariable Integer id, @RequestBody PacoteDTO pacoteDTO) {
        logger.info("Editando pacote de id: " + id);
        PacoteEntity pacote = pacoteDTO.convert();
        PacoteDTO newDTO = new PacoteDTO(service.editar(pacote, id));
        return newDTO;
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public String deletarPacote(@PathVariable Integer id) {
        logger.info("Deletando pacote de id: " + id);
        service.deletarPorId(id);
        return "Pacote removido";
    }
}
