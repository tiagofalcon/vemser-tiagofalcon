package br.com.dbccompany.vemsercoworking.Util;

public class ValidaSenhaUtil {
    public static boolean validaSenha(String senha) {
        if(senha.length() < 6){
            return false;
        }
        for (int i = 0; i < senha.length(); i++) {
            boolean letraOuNumero = Character.isDigit(senha.charAt(i)) || Character.isLetter(senha.charAt(i));
            if (letraOuNumero) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }
}
