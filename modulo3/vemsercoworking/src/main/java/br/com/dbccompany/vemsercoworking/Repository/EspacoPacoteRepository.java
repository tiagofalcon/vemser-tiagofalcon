package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    List<EspacoPacoteEntity> findByTipoContratacao(TipoContratacaoEnum tipoContratacaoEnum);
    List<EspacoPacoteEntity> findByQuantidade(int quantidade);
    List<EspacoPacoteEntity> findByPrazo(int prazo);
    List<EspacoPacoteEntity> findByPacote(PacoteEntity pacote);
    List<EspacoPacoteEntity> findByEspaco(EspacoEntity espacoEntity);
}
