package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.DTO.PagamentoDTO;
import br.com.dbccompany.vemsercoworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteId;
import br.com.dbccompany.vemsercoworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.vemsercoworking.Repository.PagamentoRepository;
import br.com.dbccompany.vemsercoworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
public class PagamentoService extends ServiceAbstract<PagamentoRepository, PagamentoEntity, Integer> {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    public PagamentoDTO salvarAceitandoClientePacoteOuContratacaoOpcional(PagamentoDTO pagamentoDTO) {
        PagamentoDTO newDto = new PagamentoDTO();
        try {
            if (pagamentoDTO.getContratacao() != null && pagamentoDTO.getClientePacote() == null) {
                this.salvaSaldoClientePorContratacao(pagamentoDTO);
            }
            if (pagamentoDTO.getClientePacote() != null && pagamentoDTO.getContratacao() == null) {
                this.salvaSaldoClientePorClientePacote(pagamentoDTO);
            }
            PagamentoEntity pagamentoEntity = pagamentoDTO.convert();
            newDto = new PagamentoDTO(repository.save(pagamentoEntity));
        } catch (Exception e) {
            logger.warn("Verifique se cliente-pacote e contratação são nulos: " + e.getMessage());
            logger.error("Erro ao salvar pagamento aceitando cliente-pacote ou contratação: " + e.getMessage());
            throw new RuntimeException();
        } finally {
            return newDto;
        }
    }

    private void salvaSaldoClientePorContratacao(PagamentoDTO pagamentoDTO) {
        try {
            SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO();
            SaldoClienteId saldoClienteId = new SaldoClienteId();
            saldoClienteId.setIdCliente(pagamentoDTO.getContratacao().getCliente().getId());
            saldoClienteId.setIdEspaco(pagamentoDTO.getContratacao().getEspaco().getId());
            saldoClienteDTO.setId(saldoClienteId);
            saldoClienteDTO.setQuantidade(pagamentoDTO.getContratacao().getQuantidade());
            saldoClienteDTO.setTipoContratacao(pagamentoDTO.getContratacao().getTipoContratacao());
            saldoClienteDTO.setVencimento(LocalDate.now().plusDays(pagamentoDTO.getContratacao().getPrazo()));
            SaldoClienteEntity saldoClienteEntity = saldoClienteDTO.convert();
            SaldoClienteDTO dto = new SaldoClienteDTO(saldoClienteRepository.save(saldoClienteEntity));
        } catch (Exception e) {
            logger.error("Erro ao salvar saldo cliente por contratação no pagamento: " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    private void salvaSaldoClientePorClientePacote(PagamentoDTO pagamentoDTO) {
        try {
            SaldoClienteDTO saldoClienteDTO = new SaldoClienteDTO();
            SaldoClienteId saldoClienteId = new SaldoClienteId();
            saldoClienteId.setIdCliente(pagamentoDTO.getClientePacote().getCliente().getId());
            List<EspacoPacoteEntity> espacoPacoteEntities = espacoPacoteRepository.findByPacote(pagamentoDTO.getClientePacote().getPacote());
            for (EspacoPacoteEntity espacoPacoteEntity : espacoPacoteEntities) {
                saldoClienteId.setIdEspaco(espacoPacoteEntity.getEspaco().getId());
                saldoClienteDTO.setId(saldoClienteId);
                saldoClienteDTO.setQuantidade(espacoPacoteEntity.getQuantidade() * pagamentoDTO.getClientePacote().getQuantidade());
                saldoClienteDTO.setTipoContratacao(espacoPacoteEntity.getTipoContratacao());
                saldoClienteDTO.setVencimento(LocalDate.now().plusDays(espacoPacoteEntity.getPrazo()));
                SaldoClienteEntity saldoClienteEntity = saldoClienteDTO.convert();
                SaldoClienteDTO dto = new SaldoClienteDTO(saldoClienteRepository.save(saldoClienteEntity));
            }

        } catch (Exception e) {
            logger.error("Erro ao salvar saldo cliente por cliente-pacote no pagamento: " + e.getMessage());
            throw new RuntimeException();
        }
    }



    @Transactional
    public void deletarPagamentoPorIdERemoveSaldoAssociado(Integer id) {
        try {
            PagamentoEntity entity = repository.findById(id).get();
            if(entity.getContratacao() != null) {
                removeSaldoPorContratacao(entity);
            }
            if(entity.getClientePacote() != null) {
                removeSaldoPorClientePacote(entity);
            }
            repository.delete(entity);
        } catch (Exception e) {
            logger.error("Não foi possível remover a entidade de id: " + id +". Mensagem: " + e.getMessage());
            throw new RuntimeException();
        }
    }
    
    private void removeSaldoPorContratacao (PagamentoEntity entity) {
        Integer idCliente = entity.getContratacao().getCliente().getId();
        Integer idEspaco = entity.getContratacao().getEspaco().getId();
        SaldoClienteId saldoClienteId = new SaldoClienteId(idCliente, idEspaco);
        SaldoClienteEntity saldoClienteEntity = saldoClienteRepository.findById(saldoClienteId).get();
        saldoClienteRepository.delete(saldoClienteEntity);
    }

    private void removeSaldoPorClientePacote (PagamentoEntity entity) {
        SaldoClienteId saldoClienteId = new SaldoClienteId();
        Integer idCliente = entity.getClientePacote().getCliente().getId();
        saldoClienteId.setIdCliente(idCliente);
        List<EspacoPacoteEntity> espacoPacoteEntities = espacoPacoteRepository.findByPacote(entity.getClientePacote().getPacote());
        for (EspacoPacoteEntity espacoPacoteEntity : espacoPacoteEntities) {
            saldoClienteId.setIdEspaco(espacoPacoteEntity.getEspaco().getId());
            SaldoClienteEntity saldoClienteEntity = saldoClienteRepository.findById(saldoClienteId).get();
            saldoClienteRepository.delete(saldoClienteEntity);
        }
    }
}
