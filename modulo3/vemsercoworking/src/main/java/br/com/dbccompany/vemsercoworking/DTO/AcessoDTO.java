package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.AcessoEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.LocalDate;

public class AcessoDTO {
    private Integer id;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;
    private boolean isEntrada;
    private LocalDate data;
    private boolean isExcecao;
    private String mensagem;
    private long timeEntrada;
    private long timeSaida;

    {
        this.isEntrada = false;
        this.isExcecao = false;
    }

    public AcessoDTO() {}

    public AcessoDTO(AcessoEntity acesso) {
        this.id = acesso.getId();
        this.saldoCliente = acesso.getSaldoCliente();
        this.isEntrada = acesso.isEntrada();
        this.data = acesso.getData();
        this.isExcecao = acesso.isExcecao();
        this.timeEntrada = acesso.getTimeEntrada();
        this.timeSaida = acesso.getTimeSaida();
    }

    public AcessoEntity convert() {
        AcessoEntity acesso = new AcessoEntity();
        acesso.setId(this.id);
        acesso.setSaldoCliente(this.saldoCliente);
        acesso.setEntrada(this.isEntrada);
        acesso.setData(this.data);
        acesso.setExcecao(this.isExcecao);
        acesso.setTimeEntrada(this.timeEntrada);
        acesso.setTimeSaida(this.timeSaida);
        return acesso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public long getTimeEntrada() {
        return timeEntrada;
    }

    public void setTimeEntrada(long timeEntrada) {
        this.timeEntrada = timeEntrada;
    }

    public long getTimeSaida() {
        return timeSaida;
    }

    public void setTimeSaida(long timeSaida) {
        this.timeSaida = timeSaida;
    }
}
