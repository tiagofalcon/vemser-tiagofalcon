package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Entity.PagamentoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    List<PagamentoEntity> findByClientePacote(ClientePacoteEntity clientePacote);
    List<PagamentoEntity> findByContratacao(ContratacaoEntity contratacao);
    List<PagamentoEntity> findByTipoPagamento(TipoPagamentoEnum tipoPagamentoEnum);

}
