package br.com.dbccompany.vemsercoworking.Util;

import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;

public class ConvertePrazoHorasTurno {
    public static int converteTurnoParaHoras(int quantidade, TipoContratacaoEnum tipoContratacaoEnum)  {
        int qtd = quantidade;
        if (tipoContratacaoEnum == TipoContratacaoEnum.TURNO) {
            qtd *= 5;
        }
        return qtd;
    }
}
