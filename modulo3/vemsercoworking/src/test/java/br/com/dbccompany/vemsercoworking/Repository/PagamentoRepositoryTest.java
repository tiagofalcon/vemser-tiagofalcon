package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class PagamentoRepositoryTest {

    @Autowired
    PagamentoRepository repository;

    @Autowired
    ClientePacoteRepository clientePacoteRepository;

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void salvaContratacaoERetornaPorClienteEspacoPrazoDescontoQuantidadeTipoContratacao() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteRepository.findByCpf("12345678910"));
        contratacao.setEspaco(espacoRepository.findByNome("Area 1"));
        contratacao.setPrazo(30);
        contratacao.setDesconto(60.00);
        contratacao.setQuantidade(30);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacaoRepository.save(contratacao);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacotes.get(0));
        clientePacote.setCliente(clienteRepository.findByCpf("12345678910"));
        clientePacote.setQuantidade(30);
        clientePacoteRepository.save(clientePacote);


        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setClientePacote(clientePacoteRepository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0));
        pagamento.setContratacao(contratacaoRepository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0));
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);

        assertEquals(TipoPagamentoEnum.DEBITO, repository.findByClientePacote(clientePacoteRepository
                .findByCliente(clienteRepository.findByCpf("12345678910")).get(0)).get(0).getTipoPagamento());
        assertEquals(TipoPagamentoEnum.DEBITO, repository.findByContratacao(contratacaoRepository
                .findByCliente(clienteRepository.findByCpf("12345678910")).get(0)).get(0).getTipoPagamento());
        assertEquals("12345678910", repository.findByTipoPagamento(TipoPagamentoEnum.DEBITO)
                .get(0).getClientePacote().getCliente().getCpf());

    }


    @Test
    public void retornaOptionalEmptySePagamentoForCriado() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteRepository.findByCpf("12345678910"));
        contratacao.setEspaco(espacoRepository.findByNome("Area 1"));
        contratacao.setPrazo(30);
        contratacao.setDesconto(60.00);
        contratacao.setQuantidade(30);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacaoRepository.save(contratacao);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacotes.get(0));
        clientePacote.setCliente(clienteRepository.findByCpf("12345678910"));
        clientePacote.setQuantidade(30);
        clientePacoteRepository.save(clientePacote);

        TipoPagamentoEnum tipoPagamentoEnum = TipoPagamentoEnum.DEBITO;

        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisPagamentoERetornaTodos() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteRepository.findByCpf("12345678910"));
        contratacao.setEspaco(espacoRepository.findByNome("Area 1"));
        contratacao.setPrazo(30);
        contratacao.setDesconto(60.00);
        contratacao.setQuantidade(30);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        contratacaoRepository.save(contratacao);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        ClientePacoteEntity clientePacote = new ClientePacoteEntity();
        clientePacote.setPacote(pacotes.get(0));
        clientePacote.setCliente(clienteRepository.findByCpf("12345678910"));
        clientePacote.setQuantidade(30);
        clientePacoteRepository.save(clientePacote);


        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setClientePacote(clientePacoteRepository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0));
        pagamento.setContratacao(contratacaoRepository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0));
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);

        PagamentoEntity pagamento2 = new PagamentoEntity();
        pagamento2.setClientePacote(clientePacoteRepository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0));
        pagamento2.setContratacao(contratacaoRepository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0));
        pagamento2.setTipoPagamento(TipoPagamentoEnum.CREDITO);
        repository.save(pagamento2);

        List<PagamentoEntity> pagamentos = (List<PagamentoEntity>) repository.findAll();

        assertEquals(TipoPagamentoEnum.DEBITO, pagamentos.get(0).getTipoPagamento());
        assertEquals(TipoPagamentoEnum.CREDITO, pagamentos.get(1).getTipoPagamento());
    }

}
