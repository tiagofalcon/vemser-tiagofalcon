package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClienteRepositoryTest {
    @Autowired
    ClienteRepository repository;

    @Test
    public void salvaClienteERetornaPorDataNscimentoNomeECpf() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        repository.save(cliente);

        assertEquals("12345678910", repository.findByNome("joao").get(0).getCpf());
        assertEquals("joao", repository.findByDataNascimento(new Date(1995, 5, 25)).get(0).getNome());
    }

    @Test
    public void retornaOptionalEmptySePacoteForCriado() {
        String nome = "joao";
        String cpf = "12345678910";
        Date dataNascimentto = new Date(1995, 5, 25);
        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisClientesERetornaTodos() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        repository.save(cliente);

        ClienteEntity clienteNovo = new ClienteEntity();
        clienteNovo.setNome("ze");
        clienteNovo.setCpf("11145678910");
        clienteNovo.setDataNascimento(new Date(1996, 5, 25));
        repository.save(clienteNovo);

        List<ClienteEntity> clientes = (List<ClienteEntity>) repository.findAll();

        assertEquals("12345678910", clientes.get(0).getCpf());
        assertEquals("11145678910", clientes.get(1).getCpf());
    }
}
