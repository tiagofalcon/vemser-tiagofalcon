package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    TipoContatoRepository repository;

    @Test
    public void salvaTipoContato() {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Telefone");
        repository.save(tipoContato);

        List<TipoContatoEntity> tiposContato = (List<TipoContatoEntity>) repository.findAll();

        assertEquals(tipoContato.getNome(), tiposContato.get(0).getNome());
    }

    @Test
    public void retornaOptionalEmptySeTipoContatoForCriado() {
        String nome = "telefone";
        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisTiposContatoERetornaTodos() {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        repository.save(tipoContato);

        TipoContatoEntity tipoContatoNovo = new TipoContatoEntity();
        tipoContatoNovo.setNome("WhatsApp");
        repository.save(tipoContatoNovo);

        List<TipoContatoEntity> tiposContato = (List<TipoContatoEntity>) repository.findAll();

        assertEquals("Email", tiposContato.get(0).getNome());
        assertEquals("WhatsApp", tiposContato.get(1).getNome());
    }

}
