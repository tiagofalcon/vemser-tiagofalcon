package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import br.com.dbccompany.vemsercoworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TipoContatoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoTipoContatoEntity () throws  Exception {
        URI uri = new URI("/api/tipoContato/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmTipoContatoEntity() throws Exception {
        URI uri = new URI("/api/tipoContato/novo");
        String json = "{\"nome\": \"email\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("email")
        );
    }

    @Test
    public void deveRetornarUmTipoContatoEntity() throws Exception {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("telefone");

        TipoContatoEntity newTipocontato = repository.save(tipoContato);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/tipoContato/ver/{id}", newTipocontato.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("telefone")
        );
    }

    @Test
    public void deveEditarERetornarUmTipoContato() throws Exception {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("whatsapp");

        TipoContatoEntity newTipoContato = repository.save(tipoContato);
        String json = "{\"nome\": \"fax\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/tipoContato/editar/{id}", newTipoContato.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("fax")
        );
    }
}
