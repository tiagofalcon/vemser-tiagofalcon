package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EspacoPacoteRepositoryTest {

    @Autowired
    EspacoPacoteRepository repository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void salvaEspacoPacoteEReotrnaPorEspacoPacotePrazoQuantidadeTipoContratacao() {

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setPacote(pacotes.get(0));
        espacoPacote.setEspaco(espacoRepository.findByNome("Area 1"));
        espacoPacote.setPrazo(30);
        espacoPacote.setQuantidade(30);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        repository.save(espacoPacote);

        List<EspacoPacoteEntity> espacoPacotes = (List<EspacoPacoteEntity>) repository.findAll();

        assertEquals(TipoContratacaoEnum.DIARIA, repository.findByEspaco(espacoRepository.findByNome("Area 1")).get(0).getTipoContratacao());
        assertEquals(TipoContratacaoEnum.DIARIA, repository.findByPacote(pacotes.get(0)).get(0).getTipoContratacao());
        assertEquals(TipoContratacaoEnum.DIARIA, repository.findByPrazo(30).get(0).getTipoContratacao());
        assertEquals(TipoContratacaoEnum.DIARIA, repository.findByQuantidade(30).get(0).getTipoContratacao());
        assertEquals(30, repository.findByTipoContratacao(TipoContratacaoEnum.DIARIA).get(0).getQuantidade());
    }

    @Test
    public void retornaOptionalEmptySeEspacoPacoteForCriado() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);

        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisEspacoPacotesERetornaTodos() {

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Area 2");
        espaco2.setQtdPessoas(30);
        espaco2.setValor(800.00);
        espacoRepository.save(espaco2);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(800.00);
        pacoteRepository.save(pacote);
        List<PacoteEntity> pacotes = (List<PacoteEntity>) pacoteRepository.findAll();

        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setPacote(pacotes.get(0));
        espacoPacote.setEspaco(espacoRepository.findByNome("Area 1"));
        repository.save(espacoPacote);

        EspacoPacoteEntity espacoPacote2 = new EspacoPacoteEntity();
        espacoPacote2.setPacote(pacotes.get(0));
        espacoPacote2.setEspaco(espacoRepository.findByNome("Area 2"));
        repository.save(espacoPacote2);

        List<EspacoPacoteEntity> espacoPacotes = (List<EspacoPacoteEntity>) repository.findAll();

        assertEquals("Area 1", espacoPacotes.get(0).getEspaco().getNome());
        assertEquals("Area 2", espacoPacotes.get(1).getEspaco().getNome());
    }
}
