package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ContratacaoRepositoryTest {

    @Autowired
    ContratacaoRepository repository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Test
    public void salvaContratacaoERetornaPorClienteEspacoPrazoDescontoQuantidadeTipoContratacao() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteRepository.findByCpf("12345678910"));
        contratacao.setEspaco(espacoRepository.findByNome("Area 1"));
        contratacao.setPrazo(30);
        contratacao.setDesconto(60.00);
        contratacao.setQuantidade(30);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        repository.save(contratacao);

        assertEquals(30, repository.findByDesconto(60.00).get(0).getPrazo());
        assertEquals(30, repository.findByCliente(clienteRepository.findByCpf("12345678910")).get(0).getPrazo());
        assertEquals(30, repository.findByEspaco(espacoRepository.findByNome("Area 1")).get(0).getPrazo());
        assertEquals(30, repository.findByTipoContratacao(TipoContratacaoEnum.DIARIA).get(0).getPrazo());
        assertEquals(TipoContratacaoEnum.DIARIA, repository.findByQuantidade(30).get(0).getTipoContratacao());
        assertEquals(TipoContratacaoEnum.DIARIA, repository.findByPrazo(30).get(0).getTipoContratacao());
    }

    @Test
    public void retornaOptionalEmptySePacoteForCriado() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        int prazo = 30;
        int quantidade = 30;
        double desconto = 60.00;
        TipoContratacaoEnum tipoContratacaoEnum = TipoContratacaoEnum.DIARIA;


        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDuasContratacaoERetornaTodos() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(clienteRepository.findByCpf("12345678910"));
        contratacao.setEspaco(espacoRepository.findByNome("Area 1"));
        contratacao.setPrazo(30);
        contratacao.setDesconto(60.00);
        contratacao.setQuantidade(30);
        contratacao.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        repository.save(contratacao);

        ContratacaoEntity contratacao2 = new ContratacaoEntity();
        contratacao2.setCliente(clienteRepository.findByCpf("12345678910"));
        contratacao2.setEspaco(espacoRepository.findByNome("Area 1"));
        contratacao2.setPrazo(30);
        contratacao2.setDesconto(60.00);
        contratacao2.setQuantidade(30);
        contratacao2.setTipoContratacao(TipoContratacaoEnum.MES);
        repository.save(contratacao2);

        List<ContratacaoEntity> contratacaos = (List<ContratacaoEntity>) repository.findAll();

        assertEquals(TipoContratacaoEnum.DIARIA, contratacaos.get(0).getTipoContratacao());
        assertEquals(TipoContratacaoEnum.MES, contratacaos.get(1).getTipoContratacao());
    }
}
