package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class AccessoRepositoryTest {

    @Autowired
    AcessoRepository repository;

    @Autowired
    SaldoClienteRepository saldoClienteRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Test
    public void salvaAcessoERetornaPorSaldoClienteEEntradaEExcecaoEData() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId.setIdEspaco(espacoRepository.findByNome("Area 1").getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setQuantidade(30);
        LocalDate data = LocalDate.now().plusDays(36l);
        saldoCliente.setVencimento(data);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        saldoClienteRepository.save(saldoCliente);

        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldoClienteRepository.findByQuantidade(30).get(0));
        acesso.setExcecao(false);
        acesso.setEntrada(true);
        LocalDate dataAcesso = LocalDate.now();
        acesso.setData(dataAcesso);
        repository.save(acesso);

        assertTrue(repository.findByData(dataAcesso).get(0).isEntrada());
        assertTrue(repository.findBySaldoCliente(saldoClienteRepository.findByTipoContratacao(TipoContratacaoEnum.DIARIA).get(0)).get(0).isEntrada());
        assertTrue(repository.findByIsExcecaoFalse().get(0).isEntrada());
        assertTrue(repository.findByIsEntradaTrue().get(0).isEntrada());
        assertTrue(repository.findByIsExcecaoTrue().isEmpty());
        assertTrue(repository.findByIsEntradaFalse().isEmpty());
    }

    @Test
    public void retornaOptionalEmptySeAcesso() {

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId.setIdEspaco(espacoRepository.findByNome("Area 1").getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setQuantidade(30);
        LocalDate data = LocalDate.now().plusDays(36l);
        saldoCliente.setVencimento(data);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        saldoClienteRepository.save(saldoCliente);

        boolean isEntrada = true;
        boolean isExcecao = false;
        LocalDate dataAcesso = LocalDate.now();

        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisAcessosERetornaTodos() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao");
        cliente.setCpf("12345678910");
        cliente.setDataNascimento(new Date(1995, 5, 25));
        clienteRepository.save(cliente);

        SaldoClienteId saldoClienteId = new SaldoClienteId();
        saldoClienteId.setIdCliente(clienteRepository.findByCpf("12345678910").getId());
        saldoClienteId.setIdEspaco(espacoRepository.findByNome("Area 1").getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setQuantidade(30);
        LocalDate data = LocalDate.now().plusDays(36l);
        saldoCliente.setVencimento(data);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        saldoClienteRepository.save(saldoCliente);

        AcessoEntity acesso = new AcessoEntity();
        acesso.setSaldoCliente(saldoClienteRepository.findByQuantidade(30).get(0));
        acesso.setExcecao(false);
        acesso.setEntrada(true);
        LocalDate dataAcesso = LocalDate.now();
        acesso.setData(dataAcesso);
        repository.save(acesso);

        AcessoEntity acesso2 = new AcessoEntity();
        acesso2.setSaldoCliente(saldoClienteRepository.findByQuantidade(30).get(0));
        acesso2.setExcecao(false);
        acesso2.setEntrada(false);
        LocalDate dataAcesso2 = LocalDate.now().minusDays(1l);
        acesso.setData(dataAcesso2);
        repository.save(acesso2);

        List<AcessoEntity> acessos = (List<AcessoEntity>) repository.findAll();

        assertTrue(acessos.get(0).isEntrada());
        assertTrue(!acessos.get(1).isEntrada());
    }
}
