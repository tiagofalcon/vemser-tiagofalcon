package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Repository.PacoteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PacoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PacoteRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoPacoteEntity () throws  Exception {
        URI uri = new URI("/api/pacote/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmPacoteEntity() throws Exception {
        URI uri = new URI("/api/pacote/novo");
        String json = "{\"valor\": \"R$ 800,00\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor").value("R$ 800,00")
        );
    }

    @Test
    public void deveRetornarUmPacoteEntity() throws Exception {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(300.00);

        PacoteEntity newTipocontato = repository.save(pacote);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pacote/ver/{id}", newTipocontato.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("R$ 300,00")
        );
    }

    @Test
    public void deveEditarERetornarUmPacote() throws Exception {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200.00);

        PacoteEntity newPacote = repository.save(pacote);
        String json = "{\"valor\": \"R$ 100,00\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/pacote/editar/{id}", newPacote.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("R$ 100,00")
        );
    }
}
