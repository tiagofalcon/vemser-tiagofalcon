package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.ClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.ContatoEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    ContatoRepository repository;

    @Autowired
    TipoContatoRepository tipoContatoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Test
    public void salvaContato() {

        ContatoEntity contato = new ContatoEntity();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        tipoContatoRepository.save(tipoContato);
        List<TipoContatoEntity> tiposContatosEntity = (List<TipoContatoEntity>) tipoContatoRepository.findAll();

        contato.setTipoContato(tiposContatosEntity.get(0));
        contato.setValor("aa@email.com");

        repository.save(contato);

        assertEquals("aa@email.com", repository.findByValor("aa@email.com").getValor());
    }

    @Test
    public void retornaOptionalEmptySeTipoContatoForCriado() {
        String nome = "aa@email.com";
        assertEquals(Optional.empty(), repository.findById(1));
    }

    @Test
    public void salvaDoisContatosERetornaPorTipoContato() {

        ContatoEntity contato = new ContatoEntity();
        ContatoEntity contatoEntity = new ContatoEntity();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        tipoContatoRepository.save(tipoContato);
        List<TipoContatoEntity> tiposContatosEntity = (List<TipoContatoEntity>) tipoContatoRepository.findAll();

        contato.setTipoContato(tiposContatosEntity.get(0));
        contato.setValor("bb@email.com");
        contatoEntity.setTipoContato(tiposContatosEntity.get(0));
        contatoEntity.setValor("cc@email.com");

        repository.save(contato);
        repository.save(contatoEntity);

        assertEquals(2, repository.findByTipoContato(tipoContato).size());
        assertEquals("bb@email.com", repository.findByTipoContato(tipoContato).get(0).getValor());
        assertEquals("cc@email.com", repository.findByTipoContato(tipoContato).get(1).getValor());
    }

    @Test
    public void salvaDoisContatosERetornaPorClienteEClienteTipoContato() {
        ContatoEntity contato = new ContatoEntity();
        ContatoEntity contatoEntity = new ContatoEntity();

        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Email");
        tipoContatoRepository.save(tipoContato);
        List<TipoContatoEntity> tiposContatosEntity = (List<TipoContatoEntity>) tipoContatoRepository.findAll();

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("joao da silva");
        cliente.setDataNascimento(new Date(1995, 6, 25));
        cliente.setCpf("12345678910");
        clienteRepository.save(cliente);
        ClienteEntity newCliente = clienteRepository.findByCpf("12345678910");

        contato.setTipoContato(tiposContatosEntity.get(0));
        contato.setValor("bb@email.com");
        contato.setCliente(newCliente);
        contatoEntity.setTipoContato(tiposContatosEntity.get(0));
        contatoEntity.setValor("cc@email.com");

        repository.save(contato);
        repository.save(contatoEntity);


        assertEquals("bb@email.com", repository.findByCliente(newCliente).get(0).getValor());
        assertEquals("bb@email.com", repository.findByTipoContatoAndCliente(tiposContatosEntity.get(0), newCliente).get(0).getValor() );
    }
}
