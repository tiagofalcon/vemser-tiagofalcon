package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import br.com.dbccompany.vemsercoworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.PacoteEntity;
import br.com.dbccompany.vemsercoworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.vemsercoworking.Repository.EspacoPacoteRepository;
import br.com.dbccompany.vemsercoworking.Repository.EspacoRepository;
import br.com.dbccompany.vemsercoworking.Repository.PacoteRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EspacoPacoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @BeforeEach
    public void gerarDadosDeEntrada() {

        //Espaco
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 2");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);
        EspacoEntity newEspaco = espacoRepository.save(espaco);

        //Pacote
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200.00);
        PacoteEntity pacoteEntity = pacoteRepository.save(pacote);


    }

    @AfterEach
    public void limpar() {
        espacoPacoteRepository.deleteAll();
        espacoRepository.deleteAll();
        pacoteRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoEspacoPacoteEntity () throws  Exception {
        URI uri = new URI("/api/espacoPacote/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmEspacoPacoteEntity() throws Exception {

        PacoteEntity pacote = pacoteRepository.findByValor(200.0).get(0);
        int idPacote = pacote.getId();
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");
        int idEspaco = espaco.getId();

        URI uri = new URI("/api/espacoPacote/novo");
        String json = "{\"espaco\": {\"id\": " + idEspaco + "}," +
                "\"pacote\": {\"id\": " + idPacote + "}," +
                "\"quantidade\":" + 2 + "," +
                "\"prazo\": 2," +
                "\"tipoContratacao\": \"MES\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao").value("MES")
        );
    }

    @Test
    public void deveRetornarUmEspacoPacoteEntity() throws Exception {

        PacoteEntity pacote = pacoteRepository.findByValor(200.0).get(0);
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");

        EspacoPacoteEntity espacoPacoteEntity = new EspacoPacoteEntity();
        espacoPacoteEntity.setEspaco(espaco);
        espacoPacoteEntity.setPacote(pacote);
        espacoPacoteEntity.setQuantidade(5);
        espacoPacoteEntity.setTipoContratacao(TipoContratacaoEnum.MES);
        espacoPacoteEntity.setQuantidade(1);
        EspacoPacoteEntity newEspacoPacoteEntity= espacoPacoteRepository.save(espacoPacoteEntity);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacoPacote/ver/{id}", newEspacoPacoteEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("MES")
        );
    }

    @Test
    public void deveEditarERetornarUmEspacoPacote() throws Exception {

        PacoteEntity pacote = pacoteRepository.findByValor(200.0).get(0);
        EspacoEntity espaco = espacoRepository.findByNome("Area 2");
        
        EspacoPacoteEntity espacoPacote = new EspacoPacoteEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPacote(pacote);
        espacoPacote.setQuantidade(10);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        espacoPacote.setPrazo(30);
        EspacoPacoteEntity newEspacoPacoteEntity = espacoPacoteRepository.save(espacoPacote);

        int idEspaco = espaco.getId();
        int idPacote = pacote.getId();

        String json = "{\"quantidade\": 12," +
                "\"espaco\": {\"id\":" + idEspaco + "}," +
                "\"pacote\": {\"id\":" + idPacote + "}," +
                "\"prazo\": 2," +
                "\"tipoContratacao\": \"SEMANA\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/espacoPacote/editar/{id}", newEspacoPacoteEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("SEMANA")
        );
    }
}
