package br.com.dbccompany.vemsercoworking.Controller;

import br.com.dbccompany.vemsercoworking.Entity.UsuarioEntity;
import br.com.dbccompany.vemsercoworking.Repository.UsuarioRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuarioRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoUsuario () throws  Exception {
        URI uri = new URI("/api/usuario/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmUsuario() throws Exception {
        URI uri = new URI("/api/usuario/novo");
        String json = "{\"nome\": \"Joao da Silva\"," +
                "\"email\": \"joao@dasilva.com\"," +
                "\"login\": \"joaosilva\"," +
                "\"senha\": \"123456\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Joao da Silva")
        );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("voldemort");
        usuario.setLogin("voldemort");
        usuario.setSenha("654321");
        usuario.setEmail("voldemort@senhordastrevas.com");
        UsuarioEntity newUsuario = repository.save(usuario);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/usuario/ver/{id}", newUsuario.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("voldemort")
        );
    }

    @Test
    public void deveEditarERetornarUmUsuario() throws Exception {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setNome("voldemort2");
        usuario.setLogin("voldemort2");
        usuario.setSenha("6543212");
        usuario.setEmail("voldemort2@senhordastrevas.com");
        UsuarioEntity newUsuario = repository.save(usuario);
        newUsuario.setNome("harry");
        String json = "{\"nome\": \"harry\"," +
                "\"email\": \"harry@potter.com\"," +
                "\"login\": \"harrypotter\"," +
                "\"senha\": \"123456934\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/usuario/editar/{id}", newUsuario.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("harry")
        );
    }

}
