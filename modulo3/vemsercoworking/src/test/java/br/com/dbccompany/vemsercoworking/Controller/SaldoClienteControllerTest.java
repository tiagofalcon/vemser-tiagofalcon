package br.com.dbccompany.vemsercoworking.Controller;


import br.com.dbccompany.vemsercoworking.Entity.*;
import br.com.dbccompany.vemsercoworking.Repository.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SaldoClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @BeforeEach
    public void gerarDadosDeEntrada() {
        //Cliente
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("email");
        TipoContatoEntity newTipoContato = tipoContatoRepository.save(tipoContato);
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(newTipoContato);
        contato.setValor("fulano@fulano.com");
        ContatoEntity c1 = contatoRepository.save(contato);

        TipoContatoEntity tipoContato2 = new TipoContatoEntity();
        tipoContato2.setNome("telefone");
        TipoContatoEntity newTipoContato2 = tipoContatoRepository.save(tipoContato2);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(newTipoContato2);
        contato2.setValor("5151515151");
        ContatoEntity c2 = contatoRepository.save(contato2);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("novoFulano");
        cliente.setCpf("01987654321");
        cliente.setDataNascimento(new Date(1990, 05, 30));
        List<ContatoEntity> listContatos = new ArrayList<>();
        listContatos.add(c1);
        listContatos.add(c2);
        cliente.setContatos(listContatos);
        ClienteEntity clienteEntity = clienteRepository.save(cliente);
        int idCliente = clienteEntity.getId();

        //Espaco
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 2");
        espaco.setQtdPessoas(30);
        espaco.setValor(300.00);
        EspacoEntity newEspaco = espacoRepository.save(espaco);
        int idEspaco = newEspaco.getId();

    }

    @AfterEach
    public void limpar() {
        saldoClienteRepository.deleteAll();
        espacoRepository.deleteAll();
        clienteRepository.deleteAll();
        contatoRepository.deleteAll();
        tipoContatoRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoConsultadoSaldoClienteEntity () throws  Exception {
        URI uri = new URI("/api/saldoCliente/todos");

        mockMvc
                .perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status()
                        .is(200)
                );
    }

    @Test
    public void salvarRetornarUmContratacaoEntity() throws Exception {

        ClienteEntity clienteEntity = clienteRepository.findByCpf("01987654321");
        int idCliente = clienteEntity.getId();

        EspacoEntity espacoEntity = espacoRepository.findByNome("Area 2");
        int idEspaco = espacoEntity.getId();

        URI uri = new URI("/api/saldoCliente/novo");
        String json = "{\"id\": {\"idCliente\": " + idCliente + "," +
                "\"idEspaco\":" + idEspaco + "}," +
                "\"quantidade\":" + 2 + "," +
                "\"vencimento\": \"2020-10-30\"," +
                "\"tipoContratacao\": \"MES\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id['idCliente','idEspaco']").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(2)
        );
    }

    @Test
    public void deveRetornarUmSaldoClienteEntity() throws Exception {

        ClienteEntity clienteEntity = clienteRepository.findByCpf("01987654321");
        EspacoEntity espacoEntity = espacoRepository.findByNome("Area 2");
        SaldoClienteId saldoClienteId = new SaldoClienteId(clienteEntity.getId(), espacoEntity.getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setEspaco(espacoEntity);
        saldoCliente.setCliente(clienteEntity);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.MES);
        saldoCliente.setQuantidade(2);
        saldoCliente.setVencimento(LocalDate.now().plusDays(10));

        SaldoClienteEntity saldoClienteEntity = saldoClienteRepository.save(saldoCliente);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/saldoCliente/ver/" + saldoClienteEntity.getId().getIdCliente() + "/" + saldoClienteEntity.getId().getIdEspaco())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(2)
        );
    }

    @Test
    public void deveEditarERetornarUmSaldoCliente() throws Exception {
        ClienteEntity clienteEntity = clienteRepository.findByCpf("01987654321");
        EspacoEntity espacoEntity = espacoRepository.findByNome("Area 2");
        SaldoClienteId saldoClienteId = new SaldoClienteId(clienteEntity.getId(), espacoEntity.getId());

        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setEspaco(espacoEntity);
        saldoCliente.setCliente(clienteEntity);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.MES);
        saldoCliente.setQuantidade(2);
        saldoCliente.setVencimento(LocalDate.now().plusDays(10));

        SaldoClienteEntity saldoClienteEntity = saldoClienteRepository.save(saldoCliente);

        String json = "{\"quantidade\":" + 50 + "," +
                "\"vencimento\": \"2020-10-30\"," +
                "\"tipoContratacao\": \"MES\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/api/saldoCliente/editar/" + saldoClienteEntity.getId().getIdCliente() + "/" + saldoClienteEntity.getId().getIdEspaco())
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(50)
        );
    }
}
