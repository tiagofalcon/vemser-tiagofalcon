package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.EspacoEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EspacoRepositoryTest {

    @Autowired
    EspacoRepository repository;

    @Test
    public void salvaEspacoERecuparaPorQtdPessoasNomeEValor() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(800.00);
        repository.save(espaco);

        assertEquals(800.00, repository.findByNome("Area 1").getValor(), 0.00001);
        assertEquals(30, repository.findByValor(800.00).get(0).getQtdPessoas());
    }

    @Test
    public void retornaOptionalEmptySePacoteForCriado() {
        double valor = 800.00;
        assertEquals(Optional.empty(),repository.findById(1));
    }

    @Test
    public void salvaDoisPacotesERetornaTodos() {
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Area 1");
        espaco.setQtdPessoas(30);
        espaco.setValor(500.00);
        repository.save(espaco);

        EspacoEntity espacoNovo = new EspacoEntity();
        espacoNovo.setNome("Area 2");
        espacoNovo.setQtdPessoas(30);
        espacoNovo.setValor(600.00);
        repository.save(espacoNovo);

        List<EspacoEntity> espacos = (List<EspacoEntity>) repository.findAll();

        assertEquals("Area 1", espacos.get(0).getNome());
        assertEquals("Area 2", espacos.get(1).getNome());
    }
}
